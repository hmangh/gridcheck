#! Demonstrates the usage of simple tests.

#:include 'fytest.fypp'

#:block TEST_SUITE('simple')
  use matmult
  implicit none

#:contains

  #! Testing for various special factorial values

  #:block TEST('1')
  integer  ,parameter                      :: m = 2
  integer  ,parameter                      :: n = 2
  real(kind=4), dimension(m,n)             :: a
  real(kind=4), dimension(m)               :: b
  real(kind=4), dimension(m)               :: c

    a(:,1) = [1.,2.]
    a(:,2) = [3.,4.]
    b = [1,2]
    call mat_vec_mul(a,b,c,m,n)
    @:ASSERT( c(1) == 7.)
    @:ASSERT( c(2) == 10.)
  #:endblock

  ! #:block TEST('5')
  !   @:ASSERT(factorial(5) == 120)
  ! #:endblock

  ! #:block TEST('6')
  !   @:ASSERT(factorial(6) == 720)
  ! #:endblock

  ! #! This one will fail in order to demonstrate test failing

  ! #:block TEST('0_failing')
  !   @:ASSERT(factorial(0) == 0)
  ! #:endblock

  ! #! This should pass again

  ! #:block TEST('7')
  !   @:ASSERT(factorial(7) == 5040)
  ! #:endblock

#:endblock TEST_SUITE


@:TEST_DRIVER()
