module integral_super_structure
    use gto_mod
    implicit none

    private


contains

subroutine integrate_over_atoms(atm_array)

    type(gto_atom_orbital)::atm_array(:)
    integer :: num_atms, indx_1, indx_2
    num_atms = size(atm_array)

    do indx_1 = 1, num_atms

        do indx_2 = indx_1, num_atms


            call integrate_over_orbitals(atm_array(indx_1),atm_array(indx_2))


        end do    
              
    end do    

end subroutine    

subroutine integrate_over_orbitals(atm1,atm2)

type(gto_atom_orbital):: atm1, atm2
integer :: atm1_num_orbitals, atm2_num_orbitals
integer :: atm1_orb_indx, atm2_orb_indx
atm1_num_orbitals = atm1%num
atm2_num_orbitals = atm2%num

do atm1_orb_indx = 1, atm1_num_orbitals
    do atm2_orb_indx = 1, atm2_num_orbitals

        call integrate_over_primitives(atm1%gto(atm1_orb_indx),atm2%gto(atm2_orb_indx))

            ! contraction here

            ! transform to spherical here

    end do
end do

end subroutine    

subroutine integrate_over_primitives(gto_prim1,gto_prim2)
    type(gtos_data):: gto_prim1, gto_prim2
    integer :: indx_prim1, indx_prim2

    do indx_prim1 = 1, gto_prim1%num

        do indx_prim2 = 1, gto_prim2%num

            ! overlap integral
            ! make gaussin gto_prim1%exp(indx_prim1),gto_prim2%exp(indx_prim2))

        end do

    end do


end subroutine    

end module    