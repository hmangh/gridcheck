module get_inp_mod


    implicit none
    private
    public get_input_filename
    character(len=12), parameter :: default_inp_folder = '../../Input/'
    character(len=22) :: default_molden_inp = default_inp_folder//'molden.inp'
    character(len=20) :: default_grid_inp = default_inp_folder//'input.in'
    contains
    !///////////////////////////////////////////////////////
    ! get inline input filename for gridinp and moldeninp
    ! if missing assume the default name for either or both
    !///////////////////////////////////////////////////////
    subroutine get_input_filename(gridinp_file_name, molden_inp_filename)
        character(len=:), allocatable :: gridinp_file_name, molden_inp_filename

        character(len=255) :: dummy1, dummy2
        integer :: ierr1, ierr2, arg_count
        logical :: inputfile_exists

        arg_count = command_argument_count()
        print*, arg_count
        select case(arg_count)

        case(0)
  
           print*, 'No command argument inputs, defaults are being used'
           gridinp_file_name = default_grid_inp
           molden_inp_filename = default_molden_inp
           inquire(file=trim(gridinp_file_name),exist=inputfile_exists)
           if(.not. inputfile_exists) stop 'grid input file is missing!'
           inquire(file=trim(molden_inp_filename),exist=inputfile_exists)
           if(.not. inputfile_exists) stop 'molden input file is missing!'
  
         case(1)
  
           print*, 'Only grid input file is specified, default molden file will be used'
           call get_command_argument(1, dummy1, status=ierr1)
           gridinp_file_name = trim(dummy1)
           if(ierr1 > 0)then
             stop 'no such grid input file name!'
           else if (ierr1 == -1)then
              stop 'command argument was truncated for grid inputfile'
           else
             inquire(file=trim(gridinp_file_name),exist=inputfile_exists)
             print*, 'grid input file name:', gridinp_file_name
             if(.not. inputfile_exists) stop 'no such grid input file name!'
            
             molden_inp_filename = default_molden_inp
             inquire(file=trim(molden_inp_filename),exist=inputfile_exists)
             if(.not. inputfile_exists) stop 'molden input file is missing!'
             
           end if
  
         case(2)
  
           print*, 'Grid input and molden input files are specified, coord will replace input centers'
           print*, 'Default basis is assumed'
           call get_command_argument(1, dummy1, status=ierr1)
  
           if(ierr1 > 0)then
             stop 'no such grid input file name!'
           else if (ierr1 == -1)then
              stop 'command argument was truncated for grid inputfile'
           else
             gridinp_file_name = trim(dummy1)
             print*, 'grid input file name:', gridinp_file_name
             inquire(file=trim(gridinp_file_name),exist=inputfile_exists)
             if(.not. inputfile_exists) stop 'no such grid input file name!'
           end if
  
           call get_command_argument(2, dummy2, status=ierr2)
           if(ierr2 > 0)then
             stop 'no such molden input file name!'
           else if (ierr2 == -1)then
              stop 'command argument was truncated for molden input'
           else
             molden_inp_filename = trim(dummy2)
             print*, 'molden input file name:', molden_inp_filename
           end if
  

  

        case default
  
           stop 'too many arguments on the inline'
  
        end select   

    end subroutine    

  

end module    