module commonparams
  implicit none

  integer, parameter                              :: idp=kind(0.d0)

  !****************************************************************************************************************************************
  ! Iosys functions
  character (len=32) , external           :: chrkey        !character key to read strings through iosys in/out system
  logical            , external           :: logkey         !logical key to read Booleanss through iosys in/out system
  integer            , external           :: intkey         !integer key to read integer variables from iosys
  real(idp)          , external           :: fpkey          !real variables key of iosys
  !*****************************************************************************************************************************************
  ! Lebedev parameters that are usable
!  integer, parameter                      :: leb_max_num = 32 !number of lebedev rules that are coded
!  integer, dimension(:),allocatable       :: ord              !order of the lebedev points for each region

!  integer, dimension(leb_max_num)         :: lrule =                                                   &
!    (/ 6,   14,   26,   38,   50,   74,   86,  110,  146,  170,  &
!    194,  230,  266,  302,  350,        434,              590,  &
!    770,              974,              1202,       &
!    1454,             1730,              2030,             &
!    2354,             2702,             3074,              3470, &
!    3890,              4334,             4802,       &
!    5294,              5810 /)

!  integer, parameter, dimension(leb_max_num) :: lprecision_table =                                     &
!    (/ 3,   5,   7,   9,  11,  13,  15,  17,  19,  21,           &
!    23,  25,  27,  29,  31     ,  35,            41,            &
!    47,            53,            59,                 &
!    65,            71,            77,                      &
!    83,            89,            95,           101,            &
!    107,            113,           119,                &
!    125,            131 /)
  !******************************************************************************************************************************************

  CHARACTER (LEN=128)                             :: grid_filename
  CHARACTER (LEN=128)                             :: orbital_filename
  CHARACTER (LEN=128)                             :: kohndt_filename
  CHARACTER(LEN=4096)                             :: ops
  !character(len=4), external                      :: itoc

  integer                                         :: iout = 9
  integer                                         :: inp  = 8

  real(idp), parameter                            :: zero = 0.0_idp
  real(idp), parameter                            :: one  = 1.0_idp
  real(idp), parameter                            :: pi =  atan(1.d0) * 4.d0
  real(idp), parameter                            :: bohr_angstrom = 5.29177210903d-01

!  contains


end module
