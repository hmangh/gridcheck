module H2_test
use commonparams
use gridparams 
implicit none
private
public test_h2

contains

!**************************************************************************
!Test originally designed for H_2, but could take in more atoms than 2
!On input the type(atom_on_grid) and type(atom) type atom has self-center
!coordinates as well as information about the dvr grid and dvr derivatives
!for a list of properties refer to gridparams.f90
!type(atom_on_grid) has the cartesian coordinates of atomic grid transformed
!to the central grid's coordinate, i.e.:
!atm_grid(i)%xyz(ipt,k) = atm(i)%xyz(ipt,k)+atm(i)%center(k) where k =1,2,3
!the test makes dirct and exchange integrals out of the 1s orbitals of H,
!but any other form of density, for example, made out of different orbitals
!could easily replace the density part
!***************************************************************************
subroutine test_h2(atm_grid, atm)
   type(atom_on_grid)::atm_grid(:)
   type(atom)        :: atm(:)
   type(vec_per_grid),allocatable:: v(:)
   integer           ::num_grids, icent, i
   real(idp), allocatable :: rho_icent(:,:), yk(:,:), rho_k(:,:)
   real(idp), allocatable :: u_k(:,:) 
   real(idp), allocatable :: centers(:,:)
   
   !This is total number of multi-centerd grids
   num_grids= size(atm_grid)
   
   !get coordinates of centers of atoms only
   centers= get_atom_centers(atm)

   !step1# loop over grids
   do icent = 1, num_grids

       !step2# make density_matrix on grid rho_icent
        !\rho_{icent}(r,\Omega) = \rho(r,\Omega) wbecke_{icent}(r,\Omega)
        !where wbecke is becke weights 
        call make_density_mat(atm_grid(icent),rho_icent, centers)
        print('(A4,I0,A12,I0,"x",I0)'), 'rho_',icent,' dimensions:',&
        size(rho_icent,1), size(rho_icent,2)
       
       !step3# make y_lm matrix scaled with angular weight
        !yk(\Omega) = y_{lm}(\Omega) w(\Omega)
        call ylm_on_atmgrid(yk,atm_grid(icent),scaled=atm_grid(icent)%w_omega)
        print('(A3,I0,A12,I0,"x",I0)'), 'yk_',icent,' dimensions:',&
        size(yk,1), size(yk,2)

       !step4# make expansion coefficients of rho in ylm
        ! \rho_{icent}(r,\Omega)= \sum_{k} rho_k(r) Y_k(\Omega)
        ! where k indicates a unique lm pair
        call make_expansion_coeff(rho_k,rho_icent, yk)                           
        print('(A6,I0,A12,I0,"x",I0)'), 'rho_k_',icent,' dimensions:',&
        size(rho_k,1), size(rho_k,2)

       !step5# solve radial poisson equation with expansion in dvrs
        call solve_poisson_rho_mat(rho_k,atm(icent),u_k)
       
       !step6# u_k contains the radial value of potential on icent
       !        find the value on all other points of other centers
        call extend_radial_potential(u_k,icent,atm_grid,v)


   end do
 
end subroutine test_h2

!*************************************************************************
!Here a density is mapped into the multi-centered grid points, but wrt
!the atomic grid centers. r and omega are in the atomic centers, not 
!the overall grid origin. Note that the function density_1s1s can be 
!easily replaced by any other functional form in this routine, or
!could be made into a pointer according to many different cases
! On input: type(atom_on_grid)::atm_grid, properties of one atom on the grid
!           real, allocatable :: rho_icent, dimension(r_icent, omega_icent)
!           real :: cent, dimension(number of atoms, k) , k = 1, 2, 3
!                   corresponding to Cartesian coordinates
! On output: rho_icent is allocated and contains the density*becke_w wrt
!            one of the atomic centers
!**************************************************************************
subroutine make_density_mat(atm_grid,rho_icent, cent)
   
   type(atom_on_grid)::atm_grid
   real(idp),allocatable  :: rho_icent(:,:)
   integer :: r_num, ang_num, ir, iomeg, num_bridges
   integer :: ipt, icent
   real(idp) :: cent(:,:)

   if (allocated(rho_icent))then
       deallocate(rho_icent)
   end if

   num_bridges = size(atm_grid%rad_bound) - 1
   !print*, num_bridges
   
    allocate(rho_icent(atm_grid%r_num, atm_grid%ang_num ))
    
     ipt = 0

     do ir = 1, atm_grid%r_num


       do iomeg = 1, atm_grid%ang_num

           ipt = ipt + 1
           !make the function (4pi)^-1/2 [e^-r_1 + e^-r_2]
           rho_icent(ir, iomeg) = density_1s1s(atm_grid%xyz(ipt,:), cent)
           !sacle with becke norm
           rho_icent(ir, iomeg) = rho_icent(ir, iomeg) * atm_grid%becke_norm(ipt)
        
    
       end do

     end do
    
    
end subroutine make_density_mat
!************************************************************************
! 1s_a and 1s_b density for hydrogen atoms
!************************************************************************
function density_1s1s(xyz, cent) result(density)
   integer            :: ipt, num_cent, ir, iomeg, icent
   integer            :: rnum, angnum
   real(idp)          :: cent(:,:)
   real(idp)          :: f
   real(idp)          :: density
   real(idp)          :: xyz(3)

           num_cent = size(cent, 2)

           do icent=1, num_cent
                f = f + fun_1s(xyz, cent(:,icent))
           end do   
           
           
           density = f*f 
            

end function 


!*****************************************************
!1s, analytical function of H
!*****************************************************
  function fun_1s (xyz,cent) result(s1)
      real(idp) :: xyz(3), bw
      real(idp) :: s1, r
      real(idp) :: cent(3)

      r = sqrt(dot_product(xyz-cent,xyz-cent))
      
      s1 = exp(- r )
      s1 = 0.5d0/sqrt(pi) * s1 
!!      print*,'r:',r,exp(-r),bw, 's1:',s1 
  end function
!******************************************************
!Mapped real spherical harmonics on the grid times a 
! scale (this could be 1) should be made optional
!******************************************************
   subroutine ylm_on_atmgrid(yk,atm_grid,scaled)
       use sphericalharmonics
       type(atom_on_grid) :: atm_grid
       real(idp), allocatable :: yk(:,:)
       integer :: k
       real(idp) :: scaled(:)
       
       if (allocated(yk))then
          deallocate(yk)
       end if

       call real_spherical_harmonics_on_atmgrid(yk, atm_grid, atm_grid%lmax)
       
       if (size(scaled) == size(yk,1)) then
             stop 'ylm_onatmgrid scale is not the right size!'  
       end if

       do k = 1, size(yk,2)
          yk(:,k) = yk(:,k) * scaled(:)
       end do
       

   end subroutine
!********************************************************************
! 
!*******************************************************************
    subroutine make_expansion_coeff(rho, rho_n, yk)

    use matmult,only: mat_mat_mul
      real(idp) :: yk(:,:), rho_n(:,:)
      real(idp), allocatable :: rho(:,:)
      integer :: r_num
      integer :: kmax

       if (allocated(rho))then
          deallocate(rho)
       end if
      kmax = size(yk,2)
      r_num = size(rho_n,1)
      allocate(rho(r_num,kmax))

          
          !    ! initialize rho_k => rho(ir, kappa)           
          !    ! \rho is \rho_k; the coefficient of expansion of \rho_n in Ylm's          

          rho = matmul(rho_n, yk)


    end subroutine
!*****************************************************
!
!*****************************************************
  subroutine solve_poisson_rho_mat(rho, atms, dvr_c)
    use lmindx_map
    use dvrmatrixOperations
!-----------------------------------------------------------------

    type(atom)                      :: atms
    real(idp)                       :: rho(:,:)
    real(idp), allocatable          :: r_array(:), dvr_c(:,:)

    integer                :: l, j 
    integer                :: icent
    integer                :: num_cent
    integer                :: r_num
    integer                :: kmax
    integer                :: knum
    integer                :: lmax
    integer                :: radial_regions
    integer                :: num_cont_pts
    integer                :: k12(2)
    integer, allocatable   :: index(:)
    
    real(idp), allocatable :: w(:), qn(:)
    real(idp), allocatable :: tot_mat_r(:,:), mat_rhs(:,:)
    
    logical                :: fix(2) = [.false.,.true.]
!-----------------------------------------------------------------
       

!       The homogenous solution is:
!            u_{lm}(r) =\frac{ \int_0^R r^{l+2}\rho_{lm}(r) dr }{R^{2l+1}}* r^{l+1}
                
!     
!       uh_l(:)= homogen_sol(r,rho,rmax,l)       
!        
!
       

       if(allocated (r_array)) deallocate(r_array)
       if(allocated (dvr_c)) deallocate(dvr_c)

!      ! This radius is in the atom-center frame:       
         call make_contiguous_r(atms%r,r_array,w,index)
         num_cont_pts = size(r_array)
         kmax = size(rho,2)
         allocate(dvr_c(num_cont_pts, kmax))
        print*, 'number of continuous radial points:',num_cont_pts

!     ! solve linear Poisson equation for each center:
!     ! [d^2/dr^2 - l(l+1)/r^2] u(r,k) = -4pi*r*rho(r,k)
!     ! here rho(r,k) is right-hand matrix, 
!     ! linear solver replaces mat_rhs with u(r,k). We then store u(r,k) back into
!     ! rho(r,k) that we no longer need.
            
            do l = 0, atms%lmax
!            !k12 contains the index of all kappa related to a fixed l number 
!            ! kappa <=> {l,m=-l,l}
                   k12=indx_range(l)
!            !!print*, 'k12:',k12            
                    call make_ddvr_ddvr_matrix(atms%r,l, tot_mat_r,fix_and_drop=fix)
!            !!print*, 'rho'
!            !!call print_matrix(rho(:,k12(1):k12(2)))
                    call make_rhs_matrix(atms%r,rho(:,k12(1):k12(2)),mat_rhs,fix)
                    do j = 1, knum  
                        mat_rhs(:,j) = -mat_rhs(:,j)* r_array(:) * 4.0_idp * pi
                    end do    
!         !
!         !   print*, 'mat before'
!         !   call print_matrix(mat_rhs)
                    call linear_solver_matmat(tot_mat_r, mat_rhs)
!
!            print*, ' mat right after linear solve'
!            call print_matrix(mat_rhs)
!                   
                    knum = k12(2) - k12(1) + 1
                    allocate(qn(knum))
                    !needs fixing!!!
                    !qn(1:knum)= sum(rho(icent)%mat(:,k12(1):k12(2))*atms(icent)%xyz(:)%w)


!            print*, 'mat after linear solve'
!            this is now the particular solution
!            call print_matrix(mat_rhs)
            
            do j = 1, num_cont_pts
!               
               dvr_c(j,k12(1):k12(2)) =   mat_rhs(j,:)
!               
            end do
!
             deallocate(tot_mat_r,mat_rhs,qn)

          end do !l 

end subroutine
!
!
!
    subroutine extend_radial_potential(u_k, icent, atm_grid, v)
       integer :: icent, num_grids
       real(idp) :: u_k(:,:)
       type(atom_on_grid) :: atm_grid(:)
       type(vec_per_grid), allocatable :: v(:)
       integer :: jcent, ipt, kmax
       real(idp) :: x_sph(3)
      
      
       kmax = size(u_k,2)
       num_grids = size(atm_grid)

       allocate(v(num_grids) )

       if(icent == 1) then
            do jcent = 2, num_grids
               
               call v(jcent)%initialize(atm_grid(jcent)%num_pts)
               do ipt = 1, atm_grid(jcent)%num_pts
                    
                 ! x_sph =  get_spherical_coord(atm_grid(jcent))
                 ! u_r(jcent)%mat(ipt,1:kmax) = interp(x_sph(1),atm(jcent))
                 ! y_k(jcent)%mat(ipt,1:kmax) = get_ylm(x_sph(2),x_sph(3),kmax)
                  

               end do

            end do  
          
       else  
            do jcent = 1, icent - 1
                do ipt = 1, atm_grid(jcent)%num_pts
            
           ! call itnitialize_
           ! atm_grid(jcent)%xyz(ipt,1

                 end do  
            end do

            do jcent = icent + 1, num_grids
       
            end do

       end if       


    end subroutine  

    function get_spherical_coord(atm_grid,cent) result(mat)

         type(atom_on_grid) :: atm_grid
         real(idp), optional :: cent(3)
         real(idp), allocatable:: x(:), y(:), z(:)
         !real(idp), allocatable:: r(:), t(:), p(:)
         real(idp), allocatable:: mat(:,:)
         
         allocate(x(atm_grid%num_pts),y(atm_grid%num_pts), z(atm_grid%num_pts))
         !allocate(r(atm_grid%num_pts),t(atm_grid%num_pts), p(atm_grid%num_pts))
         allocate(mat(atm_grid%num_pts,3))

          if (present(cent))then

           x(:) =  atm_grid%xyz(:,1) - cent(1) 
           y(:) =  atm_grid%xyz(:,2) - cent(2)
           z(:) =  atm_grid%xyz(:,3) - cent(3)
          
           mat(:,1) =sqrt( x(:)*x(:) + y(:)*y(:) + z(:) * z(:) )
           mat(:,2) = acos(z(:)/mat(:,1))
           mat(:,3) = atan(y(:)/x(:))

           

         else

         end if  

    end function
    !****************************************************
    !given type atm extracts the atomic centers
    !only for the atoms
    !****************************************************
    function get_atom_centers(atm) result(centers)
      type(atom) :: atm(:)
      integer    :: num_grids, i
      real(idp),allocatable::centers(:,:)
       
       num_grids = size(atm) 
       
        allocate(centers(3, num_grids-1))

        do i = 2, num_grids
            centers(:,i-1) = atm(i)%center
        end do

     end function

end module
