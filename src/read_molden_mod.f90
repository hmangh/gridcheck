module read_molden_mod
    use commonparams, only:idp
    use lmindx_map,only: lookup_l
    use gto_mod
    use mo_data_mod
    use string_array_mod
    implicit none

    private
    public read_molden_file

contains

   subroutine read_molden_file(moldeninp)

        character(len=*), intent(in) :: moldeninp
        
        integer :: iostat, linesum, indx
        integer :: i,j,k, number_atoms, atm_number, num_primitives
        integer, allocatable :: atmnum(:), charge(:)

        real(idp),allocatable :: xyzcoord(:,:)
        real(idp)             :: energy
        real(idp)             :: occupancy
        
        character(len=15)::aline
        character(len=7)::atoms_key
        character(len=5) :: gto_key
        character(len=4) :: mo_key
        character(len=1)::bracket, orb_symb
        character(len=6)::dummy
        character(len=4) :: symkey, enkey
        character(len=5) :: spinkey, spin
        character(len=6) :: occkey, symm
        
        type(gto_atom_orbital), allocatable :: atm(:)
        type(mo_data), allocatable :: molec_data(:)
        type(string_array_type),allocatable::symb(:)

        
        open(unit=18, file=moldeninp, status='old', iostat=iostat)
        if ( iostat /= 0 ) stop 'molden file is not opened'

        read(18,*)
        read(18,'(A7)') atoms_key
        if (atoms_key .ne. '[Atoms]') stop 'molden file [Atoms] section is missing'
        !determine how many lines to the next section
        number_atoms = 0
        do 
           read(18,'(A1)') bracket
           if ( bracket .eq. '[' ) exit
           number_atoms = number_atoms + 1
        end do

        do i = 1, number_atoms+1
            backspace (18)
        end do
        
        allocate(symb(number_atoms))
        allocate(atmnum(number_atoms),charge(number_atoms)) 
        allocate(xyzcoord(3,number_atoms))
        
        ! number_atoms here is really the number of atoms
        do  i = 1, number_atoms   
            read(18,*) dummy, atmnum(i), charge(i), xyzcoord(:,i)
            symb(i)%string = trim(dummy)
            print*, symb(i)%string, atmnum(i), charge(i), xyzcoord(:,i)
        end do
        
        !initialize number of atoms numer type with orbitals
        allocate(atm(number_atoms))

        read(18,*) gto_key
        if (gto_key .ne. '[GTO]') stop 'molden file [GTO] section is missing'
        ! determine how many lines are in GTO section
        linesum = 1
        do i = 1, number_atoms
            k = 0
            read(18,*) atm_number
            if (atm_number /= i) stop 'reading GTOS ran into a problem'
            
            do 
                linesum = linesum + 1
                read(18,'(A)')aline
                if(index(aline,'               ')>0) then 
                    exit
                else
                   backspace(18) 
                   read(18,*) orb_symb, num_primitives
                   print*, orb_symb, num_primitives
                   k = k + 1
                   do j = 1, num_primitives
                      read(18,*)
                   end do
                   linesum = linesum + num_primitives
                
                end if


            end do 
            
            print*, i , k, linesum
            call atm(i)%initialize(k,xyzcoord(:,i))
            print*, atm(i)%center
        end do
        linesum = linesum + 1
        print*, linesum
        do i = 1, linesum
            backspace(18)
        end do
        ! read(18,'(A)')aline
        ! print*, aline    
        do i = 1, number_atoms
            k = 0
            read(18,*) atm_number
            if (atm_number /= i) stop 'reading GTOS ran into a problem'
            
            do 
                read(18,'(A)')aline
                if(index(aline,'               ')>0) then 
                    exit
                else
                   backspace(18) 
                   read(18,*) orb_symb, num_primitives
                   
                   k = k + 1
                   call atm(i)%gto(k)%initialize(num_primitives,trim(orb_symb))
                   do j = 1, num_primitives
                      read(18,*) atm(i)%gto(k)%expon(j), atm(i)%gto(k)%cont_coeff(j)
                   end do
                   
                
                end if


            end do 
            
        end do 

        ! Reach the [MO] section
        do 
            read(18,*)mo_key
            if(index(mo_key,'[MO]')>0) then 
                exit
            endif 

        end do

        read(18,*) symkey, symm
        print*, symkey, symm
        read(18,*) enkey, energy
        print*, enkey, energy
        read(18,*) spinkey, spin
        print*, spinkey, spin
        read(18,*) occkey, occupancy
        print*, occkey, occupancy

        ! count number of mo coefficients
        i = 0 
        do

          read(18,*)  symkey 
          if(index(symkey,'Sym=')>0) exit
          i = i + 1

        end do   
        
        print*, i

        do j = 1, i+ 1
            backspace(18)
        end do
        ! read(18,*) indx,  coeff
        ! print*, indx, coeff   

        allocate(molec_data(i))
        call molec_data(1)%initialize(i,symm,spin,occupancy,energy)
        do j = 1, i
            read(18,*) indx,  molec_data(1)%coeff(i)
            print*, indx, molec_data(1)%coeff(i)
        end do

        ! from here read the rest 
        do j = 2, i
            read(18,*) symkey, symm
            print*, symkey, symm
            read(18,*) enkey, energy
            print*, enkey, energy
            read(18,*) spinkey, spin
            print*, spinkey, spin
            read(18,*) occkey, occupancy
            print*, occkey, occupancy
            call molec_data(j)%initialize(i,symm,spin,occupancy,energy)
            do k = 1, i
                read(18,*) indx, molec_data(j)%coeff(k)
            end do     
        end do

    end subroutine    

    


end module    