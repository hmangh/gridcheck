module dvrmatrixOperations
  use commonparams, only : idp     !* in principle could be taken out .and
                                        !replaced with * or any file output number
  use gridparams, only: radial
  implicit none
  private
  public make_ddvr_ddvr_matrix, make_rhs_vector_dvr_dvr, &
!         make_continuous_rhs_vec_dvrs, &
         linear_solver_matvec, linear_solver_matmat,make_rhs_matrix
  public print_matrix , print_vector      
contains

  !****************************************************************************************
  ! This is the left_hand_side matrix in a radial Poisson equation of the form
  ! $\frac{d^2}{dr^2} - \frac{l(l+1)}{r^2}$ u_l(r) = \rho $
  ! if $u_l(r)$ is expanded in dvr functions:  $u_l(r) = \sum_j \phi_j(r)$
  ! the integro-differential form is solved here by projecting from left into another 
  ! dvr.
  !****************************************************************************************

  subroutine make_ddvr_ddvr_matrix(r, lambda, tot_mat_r, fix_and_drop)

    type(radial), intent(in)   :: r(:)
    integer     , intent(in)   :: lambda
    integer                    :: region_num
    integer                    :: i,j,k, m, n
    integer                    :: tot_rsize
    integer                    :: last
    real(idp) , allocatable      :: tot_mat_r(:,:)
    real(idp) , allocatable      :: mat_r(:,:)
    real(idp)                    :: var, coef
    logical                    :: fix_and_drop(2)

    coef = 1.d0
    var = -1.d0 * (lambda * (lambda + 1))
    region_num = size(r)
    !!tot_rsize = size(tot_mat_r,1)
    tot_rsize = sum(r(1:region_num)%num)
    tot_rsize = tot_rsize - region_num + 1

    write(*,*) 'tot matrix size:', tot_rsize
    allocate(tot_mat_r(tot_rsize ,tot_rsize ))


    last = r(1)%num

    tot_mat_r = 0.d0


    do i= 1, region_num

      allocate(mat_r( r(i)%num , r(i)%num ))


      mat_r = 0.d0



      do j = 1, r(i)%num

        do k = 1, r(i)%num
          !!
          mat_r(j,k) = dvr_integral_maker(r(i), j, k, coef )


        end do !k

        !!     The diagonal element of potential:

        mat_r(j,j) = mat_r(j,j) +  diagonal_elements(r(i),j,var)


      end do !j




      if(i==1)then

        m = 1        
        if(fix_and_drop(1)) m =2            !this is fixing the first point of first 
                                            !radial interval and dropping it from
                                            !matrix calculations
        
        if(region_num == 1 .and. fix_and_drop(2)) last = last -1

        tot_mat_r( :last , m:last  ) = mat_r(m:last, m:last)


      else
      !!     Taking care of the bridge functions
        n = r(i)%num - 1
        
        ! dropping the last point if needed
        if( i == region_num .and. fix_and_drop(2) ) n = n - 1
        do j= 0, n
          do k= 0, n

            tot_mat_r( last+j , last+k  ) = tot_mat_r( last+j , last+k  ) + mat_r (j+1,k+1)

          end do !k

        end do !j

        last = last + n !r(i)%num - 1


      endif ! i==1
      call print_matrix(mat_r)
      write(*,*) '**********************************'
      deallocate(mat_r)

    end do !i

    call Print_Matrix(tot_mat_r)

  end subroutine make_ddvr_ddvr_matrix

!*********************************************************************************************************
! For a dvr function of form \phi_j(r) the integral 
! $\int \phi_i(r) \frac{d^2}{dr^2} \phi_j(r) dr = - \int \frac{d}{dr}\phi_i(r) \frac{d}{dr} \phi_j(r) dr $
!*********************************************************************************************************
  function dvr_integral_maker(r, indx1, indx2, coef) result(mat_elem)
    type(radial)          :: r
    integer               :: indx1, indx2
    real(idp)               :: coef
    real(idp)               :: mat_elem

    mat_elem = - coef * sum(r%dp(:,indx1) * r%dp(:,indx2) * r%w(:))

    mat_elem =  mat_elem * r%norm(indx1) * r%norm(indx2)


  end function dvr_integral_maker

!*****************************************************************
! 
!*****************************************************************

  function diagonal_elements(r,indx,var) result(mat_elem)
    type(radial)          :: r
    integer               :: indx
    real(idp)               :: var
    real(idp)               :: mat_elem


    mat_elem  = var / (r%t(indx) * r%t(indx))
    mat_elem = mat_elem * r%norm(indx) * r%norm(indx) * r%w(indx)

  end function diagonal_elements


  !****************************************************************************************
  !
  !****************************************************************************************
  subroutine diagonalize_dvrmat(mat_r)

    character :: jobz, uplo
    integer   :: n, lwork, info
    real(idp)   :: mat_r(:,:)
    real(idp), allocatable   :: w(:)
    real(idp), allocatable   :: work(:)

    jobz = 'N'
    uplo = 'U'
    n    = size(mat_r, 1)
    write(*,*) 'matrix size=',n
    lwork = max(1, 3 * n - 1)
    allocate (w(n), work(lwork))
    call dsyev(jobz, uplo, n, mat_r, n, w, work, lwork, info)

    if (info /= 0) then

      stop 'problem with dsyev'

    else

      n = min(10, n)
      write(*,*) w(1:n)

    end if


  end subroutine
  !
  !*****************************************************************************************
  !
  !*****************************************************************************************
  subroutine print_matrix(matrix)
    integer :: i
    real(idp), dimension(:,:), intent(in)     :: matrix
    real(idp), dimension(:,:), allocatable    :: t_matrix
    character(len=128) :: fmt
    integer  :: mat_size_row, mat_size_clm

    !get the size of rows
    mat_size_row = size(matrix,1)
    !print*, mat_size_row
    !sieze the size of the columns:
    mat_size_clm = size(matrix,2)
    !print*, mat_size_clm
    write(fmt,'("(",I0,"Es9.1)")') mat_size_clm
    !print*, fmt
    allocate(t_matrix(mat_size_clm, mat_size_row))
    
    t_matrix = transpose(matrix)

   ! do i = 1, mat_size_row
   !   print fmt, matrix(i,:)
   ! end do

    do i = 1, mat_size_row
      print fmt, t_matrix(:,i)
    end do
  end subroutine print_matrix
  !
  !*****************************************************************************************
  !
  !*****************************************************************************************
  subroutine print_vector(vector)
    integer :: i
    real(idp), dimension(:), intent(in) :: vector

    integer  :: vec_size

    !get the size of rows
    vec_size = size(vector)
    !print*, mat_size_row
    !sieze the size of the columns:

    !print*, mat_size_clm

    !print*, fmt
    do i = 1, vec_size
      write(*,*) vector(i)
    end do

  end subroutine print_vector
!************************************************************************************
! right_handside vector made in a discontinuous fashion
!************************************************************************************
  subroutine make_rhs_vector_dvr_dvr(r, lambda, tot_vec_r, fix_and_drop )

    type(radial), intent(in)   :: r(:)
    integer     , intent(in)   :: lambda
    integer                    :: region_num
    integer                    :: i,j,k, m, n
    integer                    :: tot_rsize
    integer                    :: last
    real(idp)                    :: tot_vec_r(:)
    real(idp) , allocatable      :: vec_r(:)
    real(idp)                    :: var
    logical                    :: fix_and_drop(2)

    var = 2.d0 * lambda + 1.d0
    !!    Finding the size of the matrix
    tot_rsize = size(tot_vec_r)
    region_num = size(r)
    write(*,*) 'tot vector size:', tot_rsize

    !!!allocate(tot_vec_r(tot_rsize ))

    last = r(1)%num

    tot_vec_r = 0.d0


    do i= 1, region_num

      allocate(vec_r( r(i)%num ))

      vec_r = 0.d0

      do j = 1, r(i)%num

          !!
          vec_r(j) = dvr_dvr_integral_rhs(r(i), j, var )


      end do !j

      if(i==1)then

        m = 1
        if(fix_and_drop(1)) m = 2

        tot_vec_r( m:last  ) = vec_r( m:last)

      else
      !!     Taking care of the bridge functions
        n = r(i)%num - 1
        
        if( i == region_num .and. fix_and_drop(2)) n = n - 1
        do j= 0, n

            tot_vec_r( last+j  ) = tot_vec_r( last+j  ) + vec_r (j+1)

        end do !j

        last = last + n !r(i)%num - 1


      endif ! i==1
      call print_vector(vec_r)
      write(*,*)"************************************************"
      deallocate(vec_r)

    end do !i
    write(*,*)"total_vector"
    call Print_vector(tot_vec_r)
    write(*,*)"************************************************"

  end subroutine


  function dvr_dvr_integral_rhs(r, indx, var) result(vec_elem)
    type(radial)          :: r
    integer               :: indx
    real(idp)               :: var
    real(idp)               :: vec_elem

    vec_elem = r%norm(indx)*r%norm(indx)*r%norm(indx)*r%w(indx)
    vec_elem = var * vec_elem / r%t(indx)

  end function
!!*****************************************************************************
!! Since the right-hand-side does not include any derivatives it can be handeled
!! in a continuous fashion, making the routine much simpler
!!*****************************************************************************
!
!  subroutine make_continuous_rhs_vec_dvrs(dvr,lambda, vec)
!    use fedvrs
!
!    type(fedvr_fun), intent(in)      :: dvr    ! these are normalized & continuous
!    integer        , intent(in)      :: lambda
!    real(idp)        , intent(out)     :: vec(:)
!
!
!       vec(:) = (2.d0 * lambda + 1.d0) * dvr%f(:) * dvr%f(:) * dvr%f(:)
!       vec(:) = vec(:) * dvr%w(:) / dvr%r(:)
!       call Print_vector(vec)
!  end subroutine
!
!*****************************************************************************
! very general right-hand-side case, where a function of radius is on the
! right-hand-side.
!*****************************************************************************

    subroutine make_rhs_matrix(r,mat,real_mat, fix_and_drop)
!
      type(radial)                     :: r(:)
      real(idp)                          :: mat(:,:)
      real(idp)        , allocatable     :: real_mat(:,:)
      integer                          :: r_regions, ksize, k, i,m, np
      integer                          :: first, last, ir, real_size
      logical                          :: fix_and_drop(2)

!      real(idp),allocatable, intent(out) :: vec(:)
!
         r_regions = size(r)
         ksize=size(mat,2)
         real_size = size(mat,1) - r_regions + 1

         allocate(  real_mat(real_size, ksize) )

         real_mat = 0.d0
!         print*, size(mat,1), size(r(1)%norm); stop          
!         do ir = 1, r_regions
!           do k = 1, ksize 
!             mat(:,k) =  mat(:,k) * r(ir)%norm(:)
!           end do 
!         end do

         if(r_regions == 1) then
           m = 1
           if(fix_and_drop(1)) m = 2
           do k = 1, ksize
              real_mat(:,k) = mat(:,k) * r(1)%w(:) * r(1)%norm(:)
           end do   

         else

           do k = 1, ksize
             
             first = 1
             ! first point of first region
                real_mat(first,k) = mat(first,k) * r(first)%w(first) * &
                                  r(first)%norm(first)
             first = 2                   

             do ir = 1, r_regions-1

               np = r(ir)%num - 1

               last = first + np - 2
              
               ! fill up to one before last matrix elements
               real_mat(first:last,k) = mat(first:last,k) * r(ir)%w(2:np)* &
                                        r(ir)%norm(1:np) 
               !print*, 'last:',last                       
               !now bridge part only
               last = last + 1
               real_mat(last,k) = mat(last,k) * r(ir)%norm(np+1) *&
                                   (r(ir)%w(np+1) + r(ir+1)%w(1))
               !print*, 'point:',last,  mat(last,k), r(ir)%norm(np+1),&
               !                         r(ir)%w(np+1) , r(ir+1)%w(1),&
               !                         real_mat(last,k)
               first = last + 1
               !print*, 'first:', first
             end do
             
             !last region
             np = r(r_regions)%num
             last = first + np - 2
             if(fix_and_drop(2)) last = last - 1
             !print*, 'last:',last
             real_mat(first:last,k) = mat(first:last,k) * r(r_regions)%w(2:np) * &
                                      r(r_regions)%norm(2:np)
                                    
             !  do i = first, last
             !  print*, 'point:',i,  mat(i,k), r(r_regions)%norm(i-first+2),&
             !                           r(r_regions)%w(i-first+2) ,&
             !                           real_mat(i,k)
             !  end do                        
           end do    
         end if

    end subroutine

    subroutine linear_solver_matvec(tot_mat_r, tot_rhs_v )

      integer :: tot_rsize
      real(idp) :: tot_mat_r(:,:), tot_rhs_v(:)
      integer :: info
      integer, allocatable :: ipiv(:)


      tot_rsize = size(tot_rhs_v)

      if(tot_rsize /= size(tot_mat_r,1)) then
!        call lnkerr( 'matrix and vector are not the same dimension (linear solver)' ); stop
        stop 'matrix and vector are not the same dimension (linear solver)' 
      end if
      tot_rsize = tot_rsize - 1
      allocate(ipiv(tot_rsize))
!computes the solution to system of linear equations A * X = B
! subroutine dgesv ( integer  	N, !The number of linear equations, i.e., the order of the matrix A. 
!                                   N >= 0
!integer  NRHS, !The number of right hand sides, i.e., the number of columns of the matrix B.  NRHS >= 0.
!real(idp), dimension( lda, * )  A, On entry, the N-by-N coefficient matrix A.
      !                                 On exit, the factors L and U from the factorization
      !                                 A = P*L*U; the unit diagonal elements of L are not stored.
! 		integer  	 LDA,            The leading dimension of the array A.  LDA >= max(1,N)
! 		integer, dimension( * )	IPIV, The pivot indices that define the permutation matrix P;
!                                       row i of the matrix was interchanged with row IPIV(i).
! 		real(idp), dimension( ldb, * ) B, On entry, the N-by-NRHS matrix of right hand side matrix B.
!                                               On exit, if INFO = 0, the N-by-NRHS solution matrix X.
! 		integer  	LDB,                     The leading dimension of the array B.  LDB >= max(1,N).
! 		integer  	INFO
! 	)

          

  !     call dgesv(N, NRHS, A, lda, IPIV, B, LDB, INFO)
           call dgesv(tot_rsize, 1, tot_mat_r, tot_rsize, IPIV, tot_rhs_v, tot_rsize, info)
      !!   tot_rhs_v is the answer to C_m
           if(INFO/=0)then
              write(*,*)'INFO=', info
              write(*,*)'something went wrong in linear solver fedvr_potential'
              stop
           endif!, 'WORK(1)=', work(1)

    end subroutine

    subroutine linear_solver_matmat(lhs_mat, rhs_mat)

      integer :: tot_rsize
      real(idp) :: rhs_mat(:,:), lhs_mat(:,:)
      integer :: info, nrhs
      integer, allocatable :: ipiv(:)

      tot_rsize = size(lhs_mat,1)

      if(tot_rsize /= size(rhs_mat,1)) then
             stop 'lhs and rhs matrices are not the same dimension (linear solver)' 
!        call lnkerr( 'matrix and vector are not the same dimension (linear solver)' ); stop
      end if
      !presumably dropping the last point, so it is zeroed 
      tot_rsize = tot_rsize - 1
      nrhs = size(rhs_mat,2)
      !computes the solution to system of linear equations A * X = B
      ! subroutine dgesv ( integer  	N, !The number of linear equations, i.e., the order of the matrix A.  N >= 0
      ! 		     integer  	NRHS, !The number of right hand sides, i.e., the number of columns of the matrix B.  NRHS >= 0.
      ! 		real(idp), dimension( lda, * )  A, On entry, the N-by-N coefficient matrix A.
      !                                         On exit, the factors L and U from the factorization
      !                                         A = P*L*U; the unit diagonal elements of L are not stored.
      ! 		integer  	 LDA,                     The leading dimension of the array A.  LDA >= max(1,N)
      ! integer, dimension( * )	IPIV,           The pivot indices that define the permutation matrix P;
      !                                           row i of the matrix was interchanged with row IPIV(i).
      ! real(idp), dimension( ldb, * ) B, On entry, the N-by-NRHS matrix of right hand side matrix B.
      !                                            On exit, if INFO = 0, the N-by-NRHS solution matrix X.
      ! integer  	LDB,                        The leading dimension of the array B.  LDB >= max(1,N).
      ! integer  	INFO
      ! 	)
           
           allocate(ipiv(tot_rsize))
      !     call dgesv(N, NRHS, A, lda, IPIV, B, LDB, INFO)
           call dgesv(tot_rsize,nrhs , lhs_mat, tot_rsize, IPIV, rhs_mat, tot_rsize, info)
      !!   tot_rhs_v is the answer to C_m
           if(INFO/=0)then
              write(*,*)'INFO=', info
              write(*,*)'something went wrong in linear solver fedvr_potential'
              stop
           endif!, 'WORK(1)=', work(1)


    end subroutine

end module
