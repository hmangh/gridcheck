module gridparams
  use commonparams
  use dvr
  use io, only: itoc
  !use fconfig
  use cfgio_mod
  !use lebedev_quadrature
  implicit none
  private
  public read_write_gridparams, atom_on_grid, atom, change_angular_grid_params, &
          change_radial_grid_params
  public radial, make_contiguous_r
  public mat_per_grid, vec_per_grid
  public find_pt_rad_2_cent, find_cen_pts_inside_atms

  ! public read_write_gridparams, read_radial_angular_grid, read_cartesian_grid, &
  !   make_overall_grid, radial, angular, cartesian, polar, azimuthal, atom, &
  !   atom_on_grid

  !*********************************************************************************************************************************************
  !polar type
  !*********************************************************************************************************************************************
  type polar
    integer                                      :: num     !number of points
    integer                                      :: kpts    !quadrature constraint key (0,1,2)
    character (len=32)                           :: ckind   !name of quadrature method
    real(idp), dimension(:),allocatable          :: t       !contains quadrature points
    real(idp), dimension(:),allocatable          :: w       !contains quadrature weights
    real(idp), dimension(:),allocatable          :: b       !an internal scratch array for calculation of quadratures
    real(idp), dimension(:),allocatable          :: d       !contains the differential of the type
    real(idp), dimension(2)                      :: endpts  !end points of the quadrature
    real(idp)                                    :: alpha   !internal parameter related only to Gauss-Jacobi quadrature
    real(idp)                                    :: beta    !internal parameter related only to Gauss-Jacobi quadrature
    logical                                      :: called  !if called=.true. the allocations won't be repeated a second time.
    !Needs to be false to reset the allocations
  end type
  !*************************************************************************************************************************************
  !azimuthal type
  !*************************************************************************************************************************************
  type azimuthal
    integer                                      :: num
    integer                                      :: kpts
    character (len=32)                           :: ckind
    real(idp), dimension(:),allocatable          :: t
    real(idp), dimension(:),allocatable          :: w
    real(idp), dimension(:),allocatable          :: b
    real(idp), dimension(:),allocatable          :: d
    real(idp), dimension(2)                      :: endpts
    real(idp)                                    :: alpha
    real(idp)                                    :: beta
    logical                                      :: called
  end type
  !*************************************************************************************************************************************
  !radial type
  !*************************************************************************************************************************************
  type radial
    integer                                      :: num
    integer, dimension(2)                        :: fixed_end !determines if the boundaries are included in quadrature (1), default is not (0).
    integer                                      :: kpts
    character(len=32)                            :: ckind
    real(idp), dimension(:),allocatable          :: t
    real(idp), dimension(:),allocatable          :: w
    real(idp), dimension(:),allocatable          :: norm       !normalization of lobotto Gauss quadratures in each region dimension(region_num)
    real(idp), dimension(:),allocatable          :: b
    real(idp), dimension(:),allocatable          :: d
    real(idp), dimension(:),allocatable          :: pw         !lagrange polynomial weights (for boundary they are different)
    real(idp), dimension(:,:),allocatable        :: p          !lagrange polynomials of FEDVRS, has dimension (num,num) followed by:
    real(idp), dimension(:,:),allocatable        :: dp         !first derivative
    real(idp), dimension(:,:),allocatable        :: ddp        !second derivative
    real(idp), dimension(2)                      :: endpts
    real(idp)                                    :: alpha
    real(idp)                                    :: beta
    real(idp)                                    :: left_edge
    real(idp)                                    :: right_edge
    logical  , dimension(2)                      :: drop=.false.!determines whether to fix then drop either of the endpoints.
    logical                                      :: called=.false.
  end type
  !*************************************************************************************************************************************
  !angular type
  !*************************************************************************************************************************************
  type angular
    integer                                       :: num, lmax
    real(idp), dimension(:),allocatable           :: w
    real(idp), dimension(:),allocatable           :: theta
    real(idp), dimension(:),allocatable           :: phi
    real(idp), dimension(:),allocatable           :: cos_theta
    real(idp), dimension(:),allocatable           :: sin_theta
    real(idp), dimension(:),allocatable           :: cos_phi
    real(idp), dimension(:),allocatable           :: sin_phi
  end type
  !*************************************************************************************************************************************
  !cartesian transformation of the spherical grid (the grid is still spherical)
  !*************************************************************************************************************************************
  type cartesian
    integer                                      :: num
    real(idp), dimension(:),allocatable          :: x
    real(idp), dimension(:),allocatable          :: y
    real(idp), dimension(:),allocatable          :: z
    real(idp), dimension(:),allocatable          :: w
  end type
  !**************************************************************************************************************************************
  !type containing information about each atom
  !**************************************************************************************************************************************
  type atom
    integer :: num_grid_pts
    integer :: lmax
    integer :: region_num
    type(radial), allocatable :: r(:)
    type(angular) :: omega
    type(cartesian), allocatable :: xyz(:)
    real(idp) :: center(3)
    real(idp) :: radius
  contains
    procedure :: initialize_atom
    procedure :: radial_grid
    procedure :: angular_grid
    procedure :: cartesian_grid
    procedure :: atom_grid_size
  end type
!**************************************************************************************************************************************
! type contains information about all points of the grid
!**************************************************************************************************************************************
  type atom_on_grid
    integer :: num_pts     !total number of points: r_num * ang_num
    integer :: r_num, ang_num, lmax
    !Cartesian coordinates in the center frame. The second dimension is n=1,2,3 corresponding
    !to x, y, z
    real(idp), allocatable :: xyz(:,:), w_xyz(:) 
    !Spherical coordinates in atom center, but not seperable. The leading dimension of
    !rhetaphi is the same as xyz with 3 dimensions in r, theta and phi 
    real(idp), allocatable :: rthetaphi(:,:), w_r(:), w_omega(:) 
    real(idp), allocatable :: becke_norm(:) ! It is kept seperate from actual w_xyz weight        
    real(idp) :: center(3), max_radius      ! Radius of atom 
    real(idp),allocatable :: rad_bound(:)   ! radial boundary points where bridge functions
                                            ! exist
  end type
!**************************************************************************************************************************************
  character (len=1600)                          :: card 
!**************************************************************************************************************************************
    type   mat_per_grid
      integer                 ::dim1, dim2
      real(idp), allocatable  ::mat(:,:)
      logical                 ::flag=.false.
      contains
      procedure,private :: initialize_mat
      procedure,private :: destroy_mat
      generic, public ::initialize =>initialize_mat
      generic, public ::destroy => destroy_mat
    end type
!*********************************************************************************
    type vec_per_grid    
       integer                :: num_pts
       real(idp), allocatable :: vec(:)
       logical                :: flag = .false.
       contains
       procedure:: initialize_vec
       generic, public :: initialize=>initialize_vec
    end type
!********************************************************************************

contains
!!********************************************************************************
!! Following 4 subroutines belong to procedures of initialization and destruction 
!! of matrices and vector types (see mat_per_grid and vec_per_grid) they 
!! enable storing a series of matrices and vectors of variable sizes
!!********************************************************************************
  subroutine initialize_mat(self, dim1, dim2)
     class(mat_per_grid) :: self
     integer, intent(in),optional :: dim1, dim2
     
     if(.not. allocated(self%mat))then
       if(present(dim1).and.present(dim2))then
         
          self%dim1=dim1
          self%dim2=dim2
     
          allocate(self%mat(dim1,dim2))
     
          self%flag = .true.
       else

          stop 'matrix in type mat_per_grid is not initialized with input dimensions'
       end if
     else
        
         self%dim1 = size(self%mat,1)
         self%dim2 = size(self%mat,2)
         self%flag = .true.
             
     endif      

  end subroutine  

!!***************************************************************
  subroutine destroy_mat(self)
    class(mat_per_grid) :: self
    
      if(allocated(self%mat))then
        deallocate(self%mat)
      end if
      self%dim1 = 0
      self%dim2 = 0
      self%flag = .false.  

  end subroutine
!!******************************************************************    
  subroutine initialize_vec(self, dim1)
     class(vec_per_grid) :: self
     integer, intent(in),optional :: dim1
     
     if(.not. allocated(self%vec))then
       if(present(dim1))then
         
          self%num_pts=dim1
          
     
          allocate(self%vec(dim1))
     
          self%flag = .true.
       else

          stop 'vector in type vec_per_grid is not initialized with an input dimension'
       end if
     else
        
         self%num_pts = size(self%vec)
         self%flag = .true.
             
     endif      

  end subroutine  
!!*******************************************************************
  subroutine destroy_vec(self)
    class(vec_per_grid) :: self
    
      if(allocated(self%vec))then
        deallocate(self%vec)
      end if
      self%num_pts = 0
      self%flag = .false.  

  end subroutine


  !!****************************************************************************************
  !! reading parameters of the grid. These include:
  !! lmax : this is the largest angular momentum number that should be supported by the grid
  !! llmax: this is a number of lmax that calc are done for llmax<=lmax
  !! region_num : fedvr supports, each region is associated with its own set of fedvr points
  !!              this is strictly a radial property
  !!****************************************************************************************
  subroutine read_write_gridparams(conf_file_name,num_atoms, grid_atom,atms,lmax_arr,rmax_arr)

    integer                                         :: num_atoms
    integer                                         :: i, ierr
    integer                                         :: lmax
    integer                                         :: region_num
    integer                                         :: cardlen, angnum

    integer, allocatable                            :: fixed_end(:,:)
    integer, allocatable                            :: shell_pts(:)
    integer, allocatable, optional                  :: lmax_arr(:)

    real(idp), allocatable                          :: edge_pts(:,:)
    real(idp), allocatable                          :: grid(:,:)
    real(idp), allocatable                          :: cen(:)
    real(idp), allocatable, optional                :: rmax_arr(:)


    character (len=:), allocatable                  :: center_filename
    character (len=32)                              :: rkind
    character (len=32)                              :: ang_grid_option
    character(len=*)                                :: conf_file_name
    character(len=17)                               ::default_conf_file = '../Input/input.in'

    type(atom) , allocatable                        :: atm(:)
    type(atom) , allocatable, optional              :: atms(:)
    type(atom)                                      :: cnt
    type(atom_on_grid), allocatable                 :: grid_atom(:)
    type(cfg_t)                                     :: conf
    logical                                         :: has_center
    logical                                         :: inputfile_exists

    ! call get_command_argument(1, conf_file_name, status=ierr)
    !
    ! if (ierr .gt. 0) then
    !   conf_file_name = default_conf_file
    !   print*, "No command line argument. Using default configuration file"
    !   print*, trim(conf_file_name)
    !   inquire(file=trim(conf_file_name),exist=inputfile_exists)
    !   if(.not. inputfile_exists) stop 'input file is missing!'
    ! else if (ierr == -1)then
    !   stop 'command argument was truncated'
    ! end if

    !call conf%read_file(conf_file_name)
    conf = parse_cfg(conf_file_name)


    call conf%get("DEFAULTS","number_of_atoms",num_atoms)
    write(iout, *) ' number of atoms: ', num_atoms

    !   call iosys ('read character "grid filename" from rwf',-1,0,0,grid_filename)
    !   write(iout,*)'       root of grid filename in m6037 = ', grid_filename
    !
    !   call iosys('open grid as new',0,0,0,grid_filename)
    !
    !
    !   num_atoms = intkey(ops, 'number_of_atoms', 1, ' ')
    !   write(iout, *) ' number of atoms: ', num_atoms
    !
    allocate(atm(1:num_atoms))
    !
    !
    !   rkind = chrkey(ops, 'r_type_quadrature', 'legendre', ' ')
    call conf%get("DEFAULTS",'r_type_quadrature',rkind)
    !
    !   ang_grid_option = chrkey(ops, 'angular_quad_type','gauss', ' ')
    call conf%get("DEFAULTS",'angular_quad_type',ang_grid_option)
    !
    write(iout, * ) 'angular grid type:',ang_grid_option
    !
    !
    !   lmax = intkey (ops, 'lmax',0, ' ')

    has_center = conf%has_section('center') 
    if(has_center)then
       call conf%get("center",'cent_lmax',lmax)
    !
    !   call iosys('write integer "lmax" to grid',&
    !     1, lmax, 0, ' ')
    !
       write(iout,*) 'read in lmax_center:',lmax
    !
       cnt%lmax = lmax
    !   card = ops
    !   !***************R*******************************************************************
    !   !***********************************************************************************
       call cnt%radial_grid(conf,rkind,'center')
       cnt%radius = cnt%r(size(cnt%r))%right_edge
       write(iout,*) 'center radius:',cnt%radius
    ! center is of course at center
       cnt%center = 0.d0
    !   !************************************************************************************
       write(iout,*) 'angular grid type:', ang_grid_option, len(trim(ang_grid_option))
    !   call conf%get("center", 'ang_grid_type', ang_grid_option)
    !   !
       call cnt%angular_grid('center',conf,ang_grid_option,lmax)
    !   !
    !   !*************************************************************************************
    !   !
       call cnt%cartesian_grid()
    !   !
       cnt%num_grid_pts = cnt%atom_grid_size()
       print*, 'number of grid points:', cnt%num_grid_pts
       write(iout, *) 'center grid points:',cnt%num_grid_pts
    !
    else
       write(iout,*) 'no center grid'
       write(*,*)    'no center grid'
    end if  

    do i = 1, num_atoms
      !
      ! if(i<10)then
      !    write(i_to_char,'(I1)') i
      ! else if(i<100) then
      !    write(i_to_char,'(I2)') i
      !  else if(i<1000) then
      !    write(i_to_char,'(I3)') i
      ! end if
      !center_filename = trim('atom_'//i_to_char)
      center_filename = trim('atom_'//itoc(i))
      write(iout,*) center_filename!, len(center_filename)
      !
      !     !call iosys('open '//center_filename//' as new',0,0,0,center_filename)
      !     call file_control(open_mat,mesa_run,file_name=trim(center_filename),&
      !       file_type='iosys',nature='new')
      !
      !     write(iout,*)  'atom_'//trim(itoc(i))//'_file'
      !
      !     call iosys ('write character "atom_'//trim(itoc(i))//'_file" to grid',&
      !       len(trim(center_filename)),0,0,center_filename)
      !
      !     call iosys ('write integer "atom_num" to '//trim(center_filename),1,i,0,' ')
      !
      !     call posinp('$'//trim(center_filename),card)
      !
      !     call cardin(card)
      !
      !
      !     write(iout,*) 'after:',card
      !
      !
      !     lmax = intkey (card, 'lmax',0, ' ')
      call conf%get(center_filename,'lmax',lmax)
      !
      !     write(iout,*) trim(center_filename)
      !
      !     call iosys('write integer "lmax" to '//trim(center_filename),&
      !       1, lmax, 0, ' ')
      !
      write(iout,*) 'read in lmax_'//itoc(i),':',lmax
      !
      atm(i)%lmax = lmax
      !
      !     call fparr(card, 'center', atm(i)%center, 3, ' ')
      !     in cfg_t arrays apparantly need to be allocatable!
      !     also using header card names like center causes seg fault
      !     also, bizarrely, you can't pass a number in unless it is a variable!!!
      !
      call conf%get(center_filename,'atom_center',cen)
      !
      if(size(cen)/=3) stop 'wrong dimensionality for center'
      atm(i)%center = cen
      write(iout,*) 'atom_'//itoc(i) , "center:", atm(i)%center
      !
      !     !***************R*******************************************************************
      !     !***********************************************************************************
      call atm(i)%radial_grid(conf,rkind,center_filename)
      !
      atm(i)%radius = atm(i)%r(size (atm(i)%r))%right_edge
      !
      write(iout, *) 'atom radius :', atm(i)%radius
      !     !************************************************************************************
      !
      write(iout,*) 'angular grid type:', ang_grid_option, len(trim(ang_grid_option))
      !
      call atm(i)%angular_grid(center_filename,conf,ang_grid_option,lmax)
      !
      !
      !     !*************************************************************************************
      !     !
      call atm(i)%cartesian_grid()
      !     !
      !     !************************************************************************************
      !     !
      atm(i)%num_grid_pts = atm(i)%atom_grid_size()
      print*, 'atom grid points:', atm(i)%num_grid_pts 
       write(iout, *) 'atom grid points:',atm(i)%num_grid_pts
      !
    end do
    !
    !   !    call make_overall_grid(grid,atm,cnt)
    if(has_center) then
    !
       call make_overall_grid_atom(grid_atom, atm, cnt)
    else
       call make_overall_grid_atom(grid_atom, atm)
    end if   
    print*, 'total number of points:', sum(atm(:)%num_grid_pts)+cnt%num_grid_pts
    !   !**************************************************
    !   !test the grid_atom
    !   !
    !   !   do i=1, num_atoms+1
    !
    !   !     write(iout,*) '*****',i, '**********'
    !   !     write(iout,*) 'atom number of radial pts:',grid_atom(i)%r_num
    !   !     write(iout,*) '1st atom number of angular pts:',grid_atom(i)%ang_num
    !   !     write(iout,*) '1st atom tot number of pts:',grid_atom(i)%num_pts
    !   !     write(iout, *) 'max_radius:', grid_atom(i)%max_radius
    !   !     write(iout, *) 'atom_center:', grid_atom(i)%center
    !
    !   !   end do
    !   !*************************************************
    !  if atms is present pass the atm type of center and atoms as well
       if (present(atms))then
         if(has_center)then
            allocate(atms(num_atoms + 1))
            atms(1) = cnt
            do i = 1, num_atoms 
               atms(i+1) = atm(i)
            end do
          else
            allocate(atms(num_atoms))
            do i = 1, num_atoms 
               atms(i) = atm(i)
            end do
          end if
       end if


       
       if( present(lmax_arr))then
         if(has_center)then
           allocate(lmax_arr(num_atoms + 1))
           lmax_arr(1)= cnt%lmax
           do i = 1, num_atoms
            lmax_arr(i+1) = atm(i)%lmax
           end do

         else
           
            allocate(lmax_arr(num_atoms))
          
            do i = 1, num_atoms
              lmax_arr(i) = atm(i)%lmax
            end do

         endif

       end if   

       if( present(rmax_arr))then
         if(has_center)then
          allocate(rmax_arr(num_atoms + 1))
          rmax_arr(1)= cnt%radius
          do i = 1, num_atoms
            rmax_arr(i+1) = atm(i)%radius
          end do
        else
     
          allocate(rmax_arr(num_atoms))
          
          do i = 1, num_atoms
            rmax_arr(i) = atm(i)%radius
          end do
           ! stop 'program the rmax_arr without center'     
        endif 
       end if   
    !
  end subroutine read_write_gridparams
  !
  ! !****************************************************************************************
  ! ! internal procedure of atom,
  ! ! on entry takes the radial region_num (support regions for fedvrs),
  ! ! type of quadrature of atom, and array to determine if the edge points are
  ! ! fixed or not
  ! ! and allocates the radial, angular, and cartesian
  ! ! regions of the atom
  ! !***************************************************************************************
  subroutine initialize_atom(atm, r_num, rkind,fend, edges, shell_pts)
    class(atom) :: atm
    integer     :: r_num, lmax, i
    integer     :: fend(:,:), shell_pts(:)
    real(idp)   :: edges(:)
    character (len=32) :: rkind
    !
    atm%region_num = r_num
    !
    !
    allocate(atm%r(r_num), atm%xyz(r_num))
    !
    atm%r(:)%ckind = rkind
    !
    if(size(fend,1) /= size(atm%r)) stop 'Input error: check that dvr region numbers match the intervals'
    atm%r(:)%fixed_end(1) = fend(:,1)
    atm%r(:)%fixed_end(2) = fend(:,2)
    !
    atm%r(:)%left_edge   = edges(1:r_num)
    atm%r(:)%right_edge   = edges(2:r_num+1)
    !
    atm%r(:)%num  = shell_pts(:)
    !
    do i = 1, r_num
      !
      if(atm%r(i)%called .eqv. .true.) deallocate(atm%r(i)%t, atm%r(i)%w, &
        atm%r(i)%d, atm%r(i)%b, atm%r(i)%p, atm%r(i)%dp, atm%r(i)%ddp,    &
        atm%r(i)%norm, atm%r(i)%pw)
      allocate(atm%r(i)%t(shell_pts(i)), atm%r(i)%w(shell_pts(i)), &
        atm%r(i)%d(shell_pts(i)), atm%r(i)%b(shell_pts(i)),        &
        atm%r(i)%p(shell_pts(i),shell_pts(i)) , atm%r(i)%dp(shell_pts(i),&
        shell_pts(i)), atm%r(i)%ddp(shell_pts(i),shell_pts(i)),          &
        atm%r(i)%norm(shell_pts(i)), atm%r(i)%pw(shell_pts(i)))
      !

    end do

     
     

    !
  end subroutine
  !
  ! !!********************************************************************************************
  ! !! internal procedure of atom
  ! !! on input it takes the type of radial grid (default is legendre)
  ! !! and the atom file name
  ! !! it reads parameters such of end points, number of points etc.
  ! !! generates the grid points and weights and writes them to the atom file
  ! !!********************************************************************************************
  subroutine radial_grid(atm,conf, rkind,header_name)
    !
    class(atom)                :: atm
    type(cfg_t)                :: conf
    !
    character (len=*)          :: header_name
    !
    integer                    :: region_num
    integer                    :: origin_fixed, end_fixed
    integer                    :: i
    integer, allocatable       :: fixed_end(:,:)
    integer, allocatable       :: shell_pts(:)
    integer                    :: rad_num
    !
    real(idp), allocatable     :: edge_pts(:)
    !
    character (len=*)          :: rkind
    !
    !   !
    !   !
    write(iout,*) '********************************'
    write(iout,*)'header:',header_name
    !   !call posinp('$'//trim(center_filename),card)
    !   !call cardin(card)
    !   region_num = intkey (card, 'region_num', 1, ' ')
    call conf%get(header_name,'region_num', region_num)
    !
    write(iout,*) 'region_num:', region_num
    !   call iosys('write integer "number of radial regions" to '//trim(center_filename),&
    !     1, region_num, 0, ' ')
    !   call iosys ('write character "radial quarature type" to '//trim(center_filename),&
    !     1,0,0,rkind)
    !   !! integer array of 0(not) and 1(yes) to determine if edge points of regions are fixed
    !   !! quadrature points
    allocate(fixed_end(region_num,2))
    !   call intarr(card, 'r_lside_fixed', fixed_end(:,1), region_num, ' ')
    call conf%get(header_name,'r_origin_fixed',origin_fixed)
    call conf%get(header_name,'r_endpt_fixed', end_fixed)
    fixed_end(:,:) = 1
    fixed_end(1,1) = origin_fixed
    fixed_end(region_num,2) = end_fixed
    write(iout,*)'left_fixed_ends:',fixed_end(:,1)
    !   call intarr(card, 'r_rside_fixed', fixed_end(:,2), region_num, ' ')
    write(iout,*)'right_fixed_ends:',fixed_end(:,2)
    !
    !   allocate(edge_pts(region_num))
    call conf%get(header_name,'r_intervals',edge_pts,region_num+1)
    !   call fparr(card, 'r_left_edge', edge_pts(:,1), region_num, ' ')
    !   call fparr(card, 'r_right_edge', edge_pts(:,2), region_num, ' ')
    write(iout,*) 'edge points: ',edge_pts(:)!, size(edge_pts)
    !   write(iout,*) 'right_edge:', edge_pts(:,2)
    !
    allocate(shell_pts(region_num))
    call conf%get(header_name,'r_num_shell_pts', shell_pts, region_num)
    write(iout, *) 'number of points per interval:',shell_pts
    !   call intarr(card, 'r_num_shell_pts', shell_pts(:), region_num, ' ')
    !
    call atm%initialize_atom(region_num, rkind, fixed_end, edge_pts, &
      shell_pts)
    !   !
    !   !!*******************************************************************************************
    !   !!  write the radial part of the grid to file
    !   !!*******************************************************************************************
    !   call iosys ('write real "left radial regions" to '//trim(center_filename), &
    !     region_num,edge_pts(:,1),0 ,' ')
    !   call iosys ('write real "right radial regions" to '//trim(center_filename), &
    !     region_num,edge_pts(:,2),0,' ')
    !   call iosys ('write integer "fixed left points" to '//trim(center_filename), &
    !     region_num,fixed_end(:,1),0,' ')
    !   call iosys ('write integer "fixed right points" to '//trim(center_filename),&
    !     region_num,fixed_end(:,2),0,' ')
    !   call iosys ('write integer "number of points per radial region" to '//trim(center_filename),&
    !     region_num,shell_pts(:),0,' ')
    !
    !   !!*********************************************************************************************
    !   !!make the radial part of each atom
    !   !!*********************************************************************************************
    call make_radial_grid(atm%r(:), region_num)
      
        rad_num = 0
    !
       do i=1, region_num
    !     call iosys('write real "radial pts region'//trim(itoc(i))//'" to '//trim(center_filename)    &
    !       ,atm%r(i)%num, atm%r(i)%t,0,' ')
    !     !write(iout,*) 'r points:',atm%r(i)%t
    !     call iosys('write real "radial weights region'//trim(itoc(i))//'" to '//trim(center_filename)&
    !       ,atm%r(i)%num, atm%r(i)%w,0,' ')
    !     !write(iout,*) 'r weights:',atm%r(i)%w
          rad_num = rad_num + atm%r(i)%num

       end do
       
       print*, '------------------------------------------------------'
       print*, header_name
       print*, 'number of radial pts:', rad_num
    !
  end subroutine
  !
  ! !!************************************************************************************************************
  ! !  given type radial and number of support regions to place gauss-type quadrature points on
  ! !  the routine makes the grid.
  ! !  the r type must include information about whether endpoints are fixed (part of grid) or not for each region.
  ! !!************************************************************************************************************
  subroutine make_radial_grid(r, region_num)
    !
    type(radial)            :: r(:)
    integer, intent(in)     :: region_num
    integer                 :: ir, j
    !
    do ir = 1, region_num
      !
      !
      !     !notice, b is an internal scratch array for the quadrature subroutine
      if(r(ir)%fixed_end(1)==1 .and. r(ir)%fixed_end(2)==0)then
        !left point is only fixed
        r(ir)%kpts=1
        r(ir)%endpts(1)= -1.d0
        r(ir)%endpts(2)= 1.d0
      elseif(r(ir)%fixed_end(1)==0 .and. r(ir)%fixed_end(2)==1)then
        r(ir)%kpts=1
        r(ir)%endpts(1)= 1.d0
        r(ir)%endpts(2)= -1.d0
      elseif(r(ir)%fixed_end(1)==1 .and. r(ir)%fixed_end(2)==1)then
        !both are fixed
        r(ir)%kpts=2
        r(ir)%endpts(1)=-1.d0
        r(ir)%endpts(2)= 1.d0
      else
        !none of the end points are fixed (endpts is irrelevant) or
        r(ir)%kpts = 0
      end if
      !
      !
      !!assigning the gaussian quadrature & mapping from -1,1 to the quadrature end points
      !kind, n, kpts, endpts, b, t, w
      call gauss_quadrature(trim(r(1)%ckind),r(ir)%num, r(ir)%kpts,&
        r(ir)%endpts, r(ir)%b, r(ir)%t, r(ir)%w)
      !
      r(ir)%endpts(1)= r(ir)%left_edge
      !
      r(ir)%endpts(2)= r(ir)%right_edge
      !
      call map_to_endpoints(r(ir)%t,r(ir)%w,r(ir)%endpts,r(ir)%num)
      !
      do j= 1,size(r(ir)%t(:))
        if(r(ir)%t(j) < 0.d0) r(ir)%t(j) = 0.d0
      end do
      !     !        assigning r^2 dr
      r(ir)%d = r(ir)%w * r(ir)%t * r(ir)%t
      !
      !fill in dvr functions on the grid along with their derivatives
      call cpoly(r(ir)%p, r(ir)%dp, r(ir)%ddp, r(ir)%t, r(ir)%num)
      !
      r(ir)%called=.true.
      !
      !
    end do
    !
    !make sure normalization variables of dvrs are done
    call normalization(r, region_num)
    !
  end subroutine
  !
  ! !!*********************************************************************************************************************
  ! !
  ! !
  ! !!*********************************************************************************************************************
  subroutine read_angular_params_gauss(omega,header,conf, lmax)
    !
    type(angular)     :: omega
    type(azimuthal)   :: phi
    type(polar)       :: theta
    type(cfg_t)       :: conf
    !
    integer           :: lmax
    integer           :: lmaxhalf
    integer           :: angnum
    !
    real(idp)           :: two_pi
    !
    character(len=*)  :: header
    !
    !
    two_pi = 2.d0 * pi
    lmaxhalf = lmax/2
    !
    write(iout,*)  'Phi:'
    !   !********phi***********************************************************************************************
    !   !
    !
    !   !type of quadrature rule
    call conf%get(header,'phi_type_quadrature',phi%ckind)
    !phi%ckind=chrkey(card,'phi_type_quadrature','trapezoidal',' ')
    write(iout,*)'phi quadrature=',phi%ckind
    !   !number of quadrature points
    call conf%get(header,'phi_quadrature_size',phi%num)
    !phi%num=intkey(card,'phi_quadrature_size', 10, ' ')
    write(iout,*)'initial phi size=',phi%num
    !   !variables pretaining to Gauss quadrature
    !phi%alpha= fpkey(ops,'phi_alpha',0.,' ')
    !phi%beta = fpkey(ops,'phi_beta',0.,' ')
    phi%alpha = 0.d0; phi%beta= 0.d0
    !   !constrained points: 0 means none, 1 for one end, 2 for both ends
    !phi%kpts=intkey(ops,'phi_num_constrained_points',0,' ')
    !   call conf%get(header,'phi_num_constrained_points',phi%kpts)
    phi%kpts = 1
    write(iout,*)'kpts=',phi%kpts
    !   !end point values
    phi%endpts(1)= 0.d0!fpkey(ops,'phi_left_endpoint',0.d0,' ')
    phi%endpts(2)= two_pi!fpkey(ops,'phi_right_endpoint',two_pi,' ')
    phi%called=.false.
    !   !write(iout,*)'phi number of points in region:',i,':',phi(i)%num
    !
    do while( 2 * lmax  >=  phi%num - 1 )! For the reason for this check Weideman, Am. Math. Monthly, vol 109. pp.21-36 (see page 24)
      phi%num = phi%num + 1
    end do
    !
    write(iout,*)'final phi number of points :',phi%num
    !
    allocate(phi%b(1:phi%num),phi%t(1:phi%num),phi%w(1:phi%num),phi%d(1:phi%num))
    !
    !   !write phi data to file
    !   call iosys ('write character "phi_quad_type" to '//filename,1,0,0,phi%ckind)
    !   call iosys ('write integer "phi_size" to '//filename ,1,phi%num,0 ,' ')
    !   write(iout,*)phi%num
    !   call iosys ('read integer "phi_size" from '//filename ,1,phi%num,0 ,' ')
    write(iout,*)phi%num
    !   call iosys ('write real "phi_left_endpoint" to '//filename,1,phi%endpts(1),0,' ')
    !   call iosys ('write real "phi right endpoint" to '//filename,1,phi%endpts(2),0,' ')
    !
    !
    !
    write(iout,*)  'Theta:'
    !   !********theta***********************************************************************************************
    !
    !   !type of quadrature rule
    !theta%ckind=chrkey(ops,'theta_type_quadrature','legendre',' ')
    call conf%get(header, 'theta_type_quadrature',theta%ckind)
    !
    write(iout,*)'theta quadrature=',theta%ckind
    !   !number of quadrature points
    !theta%num=intkey(ops,'theta_quadrature_size',10,' ')
    call conf%get(header, 'theta_quadrature_size', theta%num)
    !
    write(iout,*)'initial theta size=',theta%num
    !   !variables pretaining to Gauss quadrature
    theta%alpha= 0.d0!fpkey(ops,'theta_alpha',0.,' ')
    theta%beta = 0.d0!fpkey(ops,'theta_beta' ,0.,' ')
    !   !constrained points: 0 means none, 1 for one end, 2 for both ends
    !theta%kpts=intkey(ops,'theta_num_constrained_points',0,' ')
    !   call conf%get(header, 'theta_num_constrained_points', theta%kpts)
    theta%kpts = 2
    write(iout,*)'theta kpts=',theta%kpts
    !   !end point values
    theta%endpts(1)= -1.d0!fpkey(ops,'theta_left_endpoint',-1.d0,' ')
    theta%endpts(2)= 1.d0!fpkey(ops,'theta_right_endpoint',1.d0,' ')
    !write(iout,*)'left=',theta%endpts(1);  write(iout,*)'right=',theta%endpts(2)
    theta%called=.false.
    !   !!***check the rough ylm precision on  the angular part of the grid
    !   !!*** roughly associated with 3*lmax+6
    !
    !do while( theta%num  < 3 * (lmax+2) ) !!This was found through trial and error with plm*plm integrals this makes a grid 
                                          !! that is good for 2*lmax
    do while( theta%num  < 3 * (lmaxhalf+2) ) !This was found through trial and error with plm*plm integrals
      theta%num = theta%num + 1
    end do
    !
    !
    write(iout,*)'final theta number of points=',theta%num
    !
    allocate(theta%b(1:theta%num),theta%t(1:theta%num),theta%w(1:theta%num))
    allocate(theta%d(1:theta%num))
    !
    !   !write theta data to file
    !   call iosys ('write character "theta_quad_type" to '//filename,1,0,0,theta%ckind)
    !   call iosys ('write integer "theta_size" to '//filename ,1,theta%num,0 ,' ')
    !   call iosys ('write real "theta_left_endpoint" to '//filename,1,theta%endpts(1),0,' ')
    !   call iosys ('write real "theta_right_endpoint" to '//filename,1,theta%endpts(2),0,' ')
    !
    angnum = theta%num * phi%num
    write(iout,*) 'total angular numbers of points:',angnum
    !
    if(phi%called .eqv. .true. .or. theta%called .eqv. .true.)then
      deallocate(omega%w,omega%theta, omega%phi, &
        omega%cos_theta,omega%sin_theta   , &
        omega%cos_phi, omega%sin_phi        )
    end if
    !
    allocate(omega%w(angnum),omega%theta(angnum), omega%phi(angnum), &
      omega%cos_theta(angnum),omega%sin_theta(angnum)           , &
      omega%cos_phi(angnum), omega%sin_phi(angnum)                )
      omega%lmax = lmax
    !
    call make_angular_mixed_grid(phi, theta, omega)
    !
    !   !!*************************************************
    !   ! write to filename
    !   !!************************************************
    !   call iosys('write real "theta" to '//filename,omega%num, omega%theta,0,' ')
    !   call iosys('write real "phi" to '//filename,omega%num, omega%phi,0,' ')
    !   call iosys('write real "omega weights" to '//filename,omega%num, omega%w,0,' ')
    !
  end subroutine
  !
  ! !!************************************************************************************************************
  ! !
  ! !************************************************************************************************************
  subroutine make_angular_mixed_grid(phi, theta, omega)
    type(azimuthal) :: phi
    type(polar)     :: theta
    type(angular)   :: omega

    integer         :: l, j, k
    real(idp), parameter :: eps = 1.d-13
    !*************Angular Part**************************************************************************************************
    !***************************************************************************************************************************

    !     The mixed quadrature angular part that constructs a set of phi and theta points indepenndantly
    write(iout,*)'mixed angular quadrature'
    !     write(iout,*)  'Phi:'
    ! !********phi***************************************************************************************************************
    ! !


    !notice, b is an internal scratch array for the quadrature subroutine

    write(iout,*)'quadrature=',phi%ckind

    !kind, n, kpts, endpts, b, t, w
    call gauss_quadrature(trim(phi%ckind), phi%num,  &
      phi%kpts, phi%endpts, phi%b, phi%t, phi%w)

    !    write(iout,*)'left=',phi%endpts(1);  write(iout,*)'right=',phi%endpts(2)
    call map_to_endpoints(phi%t,phi%w,phi%endpts,phi%num)
    !    write(iout,*)phi%t
    !write(iout,*) phi%num, phi%endpts, phi%t, phi%w
    ! assigning d phi
    phi%d = phi%w

    phi%called=.true.


    !     write(iout,*)  'Theta:'
    ! !********theta*****************************************************************************************************************
    !
    !notice, b is an internal scratch array for the quadrature subroutine
    !
    call gauss_quadrature(trim(theta%ckind), theta%num,  &
      theta%kpts, theta%endpts,&
      theta%b, theta%t, theta%w)

    !  call map_to_endpoints(theta%t,theta%w,theta%endpts,theta%num)
    !
    ! assigning Sin(theta) dtheta in z coordinate (theta is really z)
    theta%d = theta%w !* sin(theta%t)

    theta%called=.true.



    !assigning overall angular part: omega
    l=0
    do j= 1 , theta%num
      do k= 1, phi%num
        l=l+1
        !write(iout,*) theta%t(j), acos(theta%t(j)),sin(acos(theta%t(j)))
        if(abs(theta%t(j)-1.d0)<eps ) then
          !numerical inaccuracies could lead to z>1
          omega%theta(l) = 0.d0
        elseif( abs(theta%t(j)+1.d0)<eps)then

          omega%theta(l) = pi
        else

          omega%theta(l)  = acos(theta%t(j))      !theta%t(j)!  !z=cos_theta
        end if
        !write(iout, *) omega%theta(l)
        omega%sin_theta(l)= sin(omega%theta(l))!sin(theta%t(j))!sin( acos(theta(i)%t(j)))
        omega%cos_theta(l)= theta%t(j)!cos(theta%t(j))
        omega%phi(l)      = phi%t(k)
        omega%sin_phi(l)  = sin(phi%t(k))
        omega%cos_phi(l)  = cos(phi%t(k))
        omega%w(l)        = theta%d(j)*phi%d(k)
      end do !k
    end do !j
    omega%num=l

    print*, 'final mixed grid precision (spherical harmonics up to lmax=):', &
     omega%lmax
    print*, 'total num of angular pts:', omega%num
    print*,  'number of theta pts:', theta%num
    print*,  'number of phi pts:', phi%num
    print*, '---------------------------------------------'

  end subroutine

  ! !************************************************************************************************************
  ! !
  ! !************************************************************************************************************
  subroutine read_angular_params_lebedev(omega,header,conf,lmax)
    use lebedev_quadrature
    integer           :: ord, j
    integer           :: lmax
    integer           :: angnum
    integer           :: rule
    character(len=*)  :: header

    type(angular)     :: omega
    type(lebedev)     :: leb_ang
    type(cfg_t)       :: conf
    !
    j = 0
    !   !order = intkey(card,'lebedev_order', ord,1, ' ')
    call conf%get(header,'lebedev_rule',rule)
    call leb_ang%get(rule)
    !   order=lprecision_table(rule)
    write(iout,*) 'initial lebedev precision (spherical harmonics up to lmax=):',&
      leb_ang%precision, 'lebedev num pts:', leb_ang%num
    if(leb_ang%precision < lmax .and. rule /= rule_max) then
      j = rule + 1
      do while(j <= rule_max)
        call leb_ang%get(j)
        if (leb_ang%precision>=lmax)then
          exit
        else
          j = leb_ang%rule + 1
        end if
      end do
    endif
    write(iout,*) 'final lebedev precision (spherical harmonics up to lmax=):', &
      leb_ang%precision, 'lebedev num pts:', leb_ang%num
    !print*, '--------------------------------------------'
    !print*, header
    print*, 'final lebedev precision (spherical harmonics up to lmax=):', &
      leb_ang%precision, 'lebedev num pts:', leb_ang%num
    print*, '---------------------------------------------'
    !
    !   write(iout,*)'Lebedev number of points :',order
    !
    !   call leb_ang%get(ord)!Generate_Lebedev_Points_Weights(leb_ang, leb_rule)
    !
    !
    angnum    = leb_ang%num
    !
    !   write(iout,*)'overall angular number of points:', angnum
    !
    omega%num = angnum
    !
    if(allocated(omega%theta) .or. allocated(omega%phi))         then
      deallocate(omega%theta, omega%sin_theta, omega%cos_theta, &
        omega%phi, omega%sin_phi, omega%cos_phi, omega%w)
    end if
    !
    allocate(omega%theta(angnum), omega%sin_theta(angnum), omega%cos_theta(angnum), &
      omega%phi(angnum), omega%sin_phi(angnum), omega%cos_phi(angnum), omega%w(angnum))
    !   !        write(iout,*)'lebedev_order:',order
    !   !!*************************************************************
    !   !!   fill the angular type omega with leb_ang info
    omega%theta(:)     = leb_ang%theta(:)
    omega%sin_theta(:) = sin(leb_ang%theta(:))
    omega%cos_theta(:) = cos(leb_ang%theta(:))
    omega%phi(:)       = leb_ang%phi(:)
    omega%sin_phi(:)   = sin(leb_ang%phi(:))
    omega%cos_phi(:)   = cos(leb_ang%phi(:))
    omega%w(:)         = 4.d0*pi*leb_ang%w(:)

    ! do j = 1, angnum

    !   print*, 'theta:',omega%theta(j), 'phi:', omega%phi(j), 'w:', omega%w(j)

    ! end do
    !   print*, 'surface integral:', sum(leb_ang%w(:)*omega%sin_theta(:))/4.d0/pi


    !
    !   !!*************************************************
    !   ! write to filename
    !   !!************************************************
    !   ! call iosys('write real "theta" to '//filename,omega%num, omega%theta,0,' ')
    !   ! call iosys('write real "phi" to '//filename,omega%num, omega%phi,0,' ')
    !   ! call iosys('write real "omega weights" to '//filename,omega%num, omega%w,0,' ')
    !
  end subroutine
  !
  !
  subroutine angular_grid(atm,center_filename,conf,ang_grid_option,lmax)

    class(atom)      :: atm
    type(cfg_t)      :: conf
    character(len=*) :: ang_grid_option
    character(len=*) :: center_filename
    integer          :: lmax

    if (trim(ang_grid_option) == 'gauss' .or. ang_grid_option == 'mixed') then

      !
      call read_angular_params_gauss(atm%omega,trim(center_filename),conf, lmax)
      !

    elseif (trim(ang_grid_option) == 'lebedev') then
      !
      !   !
      call read_angular_params_lebedev(atm%omega,trim(center_filename),conf, lmax)
      !   !

    end if


  end subroutine
  ! !!****************************************************************************************
  ! !! Reading the actual components of the spherical single-centered grid
  ! !! These are read into the types r (radial component) and Omega (angular components)
  ! !!****************************************************************************************
  ! subroutine read_radial_angular_grid(region_num, r, omega)
  !   integer                                         :: region_num
  !   integer                                         :: i
  !   character (len=4)                               :: itoc
  !   type(radial),        dimension(:),allocatable   :: r
  !   type(angular),       dimension(:),allocatable   :: omega
  !
  !   allocate(r(region_num), omega(region_num))
  !
  !   !!*******************************************************************************************
  !   !!  read the radial part of the grid from file
  !   !!*******************************************************************************************
  !   call iosys ('read real "left radial regions" from grid' ,region_num,r(:)%left_edge,0 ,' ')
  !   call iosys ('read real "right radial regions" from grid',region_num,r(:)%right_edge,0,' ')
  !   call iosys ('read integer "fixed left points" from grid',region_num,r(:)%fixed_end(1),0,' ')
  !   call iosys ('read integer "fixed right points" from grid',region_num,r(:)%fixed_end(2),0,' ')
  !   call iosys ('read integer "number of points per radial region" from grid',region_num,r(:)%num,0,' ')
  !   !! read weights and points of r
  !   do i = 1, region_num
  !     allocate(r(i)%t(r(i)%num),r(i)%w(r(i)%num),r(i)%norm(r(i)%num))
  !
  !     call iosys('read real "radial pts region'//itoc(i)//'" from grid',r(i)%num, r(i)%t,0,' ')
  !     call iosys('read real "radial weights region'//itoc(i)//'" from grid',r(i)%num, r(i)%w,0,' ')
  !
  !   end do
  !
  !   !!************************************************************************************************************
  !   !!  read the angular grid from file
  !   !!************************************************************************************************************
  !   call iosys ('read integer "omega number of points per radial region" from grid' ,region_num,omega(:)%num,0 ,' ')
  !   write(iout,*)"omega number of points per radial region",omega(:)%num
  !   do i=1, region_num
  !     allocate(omega(i)%theta(omega(i)%num),omega(i)%phi(omega(i)%num),omega(i)%w(omega(i)%num))
  !     call iosys('read real "theta region'//itoc(i)//'" from grid',omega(i)%num, omega(i)%theta,0,' ')
  !     call iosys('read real "phi region'//itoc(i)//'" from grid',omega(i)%num, omega(i)%phi,0,' ')
  !     call iosys('read real "omega weights region'//itoc(i)//'" from grid',omega(i)%num, omega(i)%w,0,' ')
  !   end do
  !
  !
  ! end subroutine
  !
  ! !!********************************************************************************************************************
  ! !! Reading the cartesian corrdinates for the Spherical single centered grid
  ! !!********************************************************************************************************************
  ! subroutine read_cartesian_grid(region_num,xyz)
  !   integer                                             :: region_num
  !   integer                                             :: i
  !   type(cartesian),     dimension(:),allocatable       :: xyz
  !   character (len=4)                                   :: itoc
  !
  !   allocate(xyz(region_num))
  !   !!************************************************************************************************************
  !   !!  read the xyz grid from file
  !   !!************************************************************************************************************
  !   call iosys ('read integer "cartesian number of points per radial region" from grid' ,region_num,xyz(:)%num,0,' ')
  !   do i = 1, region_num
  !     allocate(xyz(i)%x(xyz(i)%num),xyz(i)%y(xyz(i)%num),xyz(i)%z(xyz(i)%num),xyz(i)%w(xyz(i)%num))
  !     call iosys('read real "x points for region '//itoc(i)//'" from grid',xyz(i)%num,xyz(i)%x,0,' ')
  !     call iosys('read real "y points for region '//itoc(i)//'" from grid',xyz(i)%num,xyz(i)%y,0,' ')
  !     call iosys('read real "z points for region '//itoc(i)//'" from grid',xyz(i)%num,xyz(i)%z,0,' ')
  !     call iosys('read real "weights for region '//itoc(i)//'" from grid',xyz(i)%num,xyz(i)%w,0,' ' )
  !   end do
  ! end subroutine
  !
  ! !!********************************************************************************************************************
  ! !! If the angular part of the grid is made of combination of theta (polar) and phi (azimuthal) pieces
  ! !! These could be read separately.
  ! !!********************************************************************************************************************
  ! subroutine read_mixed_angular_grid(region_num, theta, phi)
  !   type(polar),         dimension(:),allocatable       :: theta
  !   type(azimuthal),     dimension(:),allocatable       :: phi
  !   character(len=32)                                   :: ang_option
  !   character (len=4)                                   :: itoc
  !   integer                                             :: i, region_num, f_len
  !
  !   ! !!*************************************************************************************************************
  !   ! !!  if the angular grid is mixed grid, then read theta and phi in
  !   ! !!*************************************************************************************************************
  !
  !   call iosys ('read character "angular quadrature type" from grid',32,0,0,ang_option)
  !   call pakstr(ang_option,f_len)
  !   !write(iout,*)ang_option, ang_option(1:f_len)
  !   if(ang_option(1:f_len) == 'gauss')then
  !     write(iout,*)'mixed angular part'
  !     call iosys ('read integer "phi size per region" from grid' ,region_num,phi(:)%num,0 ,' ')
  !     !write(iout,*)"phi quadrature size per radial region:"
  !
  !     call iosys ('read integer "theta quadrature size per radial region" from grid' ,region_num,theta(:)%num,0 ,' ')
  !
  !     !write(iout,*)"theta quadrature size per radial region:"
  !     !do i= 1, region_num
  !     !write(iout,*)phi(i)%num
  !     !write(iout,*)theta(i)%num
  !     !end do
  !
  !     do i=1, region_num
  !       allocate(theta(i)%t(theta(i)%num),theta(i)%w(theta(i)%num))
  !
  !       allocate(phi(i)%t(phi(i)%num),phi(i)%w(phi(i)%num))
  !       call iosys('read real "mixtheta '//itoc(i)//'" from grid',theta(i)%num, theta(i)%t,0,' ')
  !       call iosys('read real "mixtheta w '//itoc(i)//'" from grid',theta(i)%num, theta(i)%w,0,' ')
  !       call iosys('read real "mixphi '//itoc(i)//'" from grid',phi(i)%num, phi(i)%t,0,' ')
  !       call iosys('read real "mixphi w '//itoc(i)//'" from grid',phi(i)%num, phi(i)%w,0,' ')
  !     end do
  !   else
  !     write(iout, *) 'The grid is not a mixed angular grid', ang_option
  !   end if
  !
  !
  ! end subroutine
  !
  !
  subroutine cartesian_grid(atm)!,center_filename)

    class(atom) :: atm
    !character(len=*) :: center_filename

    call make_cartesian_grid(atm%xyz, atm%r, atm%omega)

  end subroutine
  !
  !
  subroutine make_cartesian_grid(xyz, r, omega)

    type(cartesian), allocatable :: xyz(:)
    type(radial)    :: r(:)
    type(angular)   :: omega

    integer         :: region_num
    integer         :: i, ii, j, k, l


    region_num = size(r)
    if(.not. allocated(xyz)) allocate(xyz(region_num))

    do i = 1, region_num

      xyz(i)%num = r(i)%num * omega%num
      allocate(xyz(i)%x(xyz(i)%num),xyz(i)%y(xyz(i)%num),xyz(i)%z(xyz(i)%num),xyz(i)%w(xyz(i)%num))

    end do

    do ii= 1, region_num
      l=0
      do i= 1 , r(ii)%num
        do j= 1 , omega%num!theta(ii)%num
          !do k= 1, phi(ii)%num
          l=l+1
          ! x = r Sin(theta) Cos(phi)
          ! write(iout,*)  r(ii)%t(i),omega%sin_theta(j),omega%cos_phi(j)
          ! if(l>100)stop
          xyz(ii)%x(l) = r(ii)%t(i)*omega%sin_theta(j)*omega%cos_phi(j)!*Sin(theta(ii)%t(j))*Cos(phi(ii)%t(k))
          ! y = r Sin(theta) Sin(phi)
          xyz(ii)%y(l) = r(ii)%t(i)*omega%sin_theta(j)*omega%sin_phi(j)!*Sin(theta(ii)%t(j))*Sin(phi(ii)%t(k))
          ! z = r Cos(theta)
          xyz(ii)%z(l) = r(ii)%t(i)*omega%cos_theta(j)!Cos(theta(ii)%t(j))
          ! weights
          !write(iout,*)'r%d(',ii,')=',r(ii)%d(i)
          !write(iout,*)'omega%w(',ii,')=',omega%w(i)
          xyz(ii)%w(l) = r(ii)%d(i)*omega%w(j)!theta(ii)%w(j)*phi(ii)%w(k)
          if(xyz(ii)%w(l).lt.0.d0)then
            print*, 'sin theta:', omega%sin_theta(j)
            print*, 'r^2dr_weight:',r(ii)%d(i)
            print*, 'dOmega_weight:', omega%w(j)
            stop 'cartesian weight less than zero!'

          endif
          !write(iout,*)l,'(',xyz(ii)%x(l),xyz(ii)%y(l),xyz(ii)%z(l),xyz(ii)%w(l),')'
          !end do !k
        end do !j
      end do !i
    end do !ii

  end subroutine
  ! !!*********************************************************************************************************************
  ! !! separate grid that is in order of creation. It lists each points x, y, z and weight values without regional
  ! !! separation. However, the boundary points x, y, z are repeated but the weights on the right and left of the
  ! !! boundary could be different.
  ! !!*********************************************************************************************************************
  subroutine make_overall_grid(grid,atm,cnt)
    integer :: gridsize
    integer :: num_atoms
    integer :: region_num
    integer :: ir, iatm, j, ipts, pts
    real(idp), allocatable :: grid(:,:)
    type(atom)  :: atm(:)
    type(atom)  :: cnt

    !!*************************************************************************************************************
    !! 'pour' the regional cartesian grid into an overall grid
    !!*************************************************************************************************************
    num_atoms = size(atm)


    gridsize = cnt%num_grid_pts + sum(atm(:)%num_grid_pts)
    write(iout,*)'grid size:',gridsize
    allocate(grid(1:gridsize,1:4))
    j=0
    region_num = cnt%region_num
    do ir = 1, region_num
      pts = cnt%xyz(ir)%num
      do ipts = 1, pts
        j = j + 1
        grid(j,1)= cnt%xyz(ir)%x(ipts)
        !        grid(j+1:j+xyz(i)%num,2)=xyz(i)%y
        grid(j,2)=  cnt%xyz(ir)%y(ipts)
        !        grid(j+1:j+xyz(i)%num,3)=xyz(i)%z
        grid(j,3)= cnt%xyz(ir)%z(ipts)
        !        grid(j+1:j+xyz(i)%num,4)=xyz(i)%w
        grid(j,4)= cnt%xyz(ir)%w(ipts)
        !        j=j+xyz(i)%num
      end do
    end do


    do iatm = 1, num_atoms
      region_num = atm(iatm)%region_num
      do ir= 1, region_num
        pts = atm(iatm)%xyz(ir)%num
        do ipts = 1, pts
          j=j+1
          !        grid(j+1:j+xyz(i)%num,1)=xyz(i)%x
          grid(j,1)= atm(iatm)%xyz(ir)%x(ipts)+atm(iatm)%center(1)
          !        grid(j+1:j+xyz(i)%num,2)=xyz(i)%y
          grid(j,2)=  atm(iatm)%xyz(ir)%y(ipts) +atm(iatm)%center(2)
          !        grid(j+1:j+xyz(i)%num,3)=xyz(i)%z
          grid(j,3)= atm(iatm)%xyz(ir)%z(ipts) +atm(iatm)%center(3)
          !        grid(j+1:j+xyz(i)%num,4)=xyz(i)%w
          grid(j,4)= atm(iatm)%xyz(ir)%w(ipts)
          !        j=j+xyz(i)%num
        end do
      end do
    end do

  end subroutine
  !
  function atom_grid_size(atm) result(num)

    class(atom) :: atm
    integer     :: num

    num = sum(atm%r(:)%num)
    num = num * atm%omega%num

  end function
  !
  subroutine make_overall_grid_atom(grid_atom, atm, cnt)

    type(atom_on_grid) , allocatable :: grid_atom(:)
    type(atom)  :: atm(:)
    type(atom), optional  :: cnt
    integer   :: i,j, iatm , num_atoms, num_pts
    integer   :: ir, pts, ipts
    integer   :: loc, grid_size, region_num
    integer   :: rpts, angpts, iang, irpts

    !transformation and transfere of information to grid_atom

    num_atoms = size(atm)
   !! print*, num_atoms; stop

    if (present(cnt)) then

      allocate(grid_atom(num_atoms+1))
      loc = 1
      grid_size = sum(cnt%xyz(:)%num)
      allocate(grid_atom(loc)%xyz(grid_size,3),grid_atom(loc)%w_xyz(grid_size))
      allocate(grid_atom(loc)%becke_norm(grid_size))
      allocate(grid_atom(loc)%rthetaphi(grid_size,3),grid_atom(loc)%w_r(grid_size))
      allocate(grid_atom(loc)%w_omega(grid_size))
      region_num = cnt%region_num
      allocate(grid_atom(loc)%rad_bound(region_num))
      j = 0
      do ir = 1, region_num

        pts = cnt%xyz(ir)%num

        do ipts = 1, pts
          j = j + 1
          grid_atom(loc)%xyz(j,1)= cnt%xyz(ir)%x(ipts)
          !        grid(j+1:j+xyz(i)%num,2)=xyz(i)%y
          grid_atom(loc)%xyz(j,2)=  cnt%xyz(ir)%y(ipts)
          !        grid(j+1:j+xyz(i)%num,3)=xyz(i)%z
          grid_atom(loc)%xyz(j,3)= cnt%xyz(ir)%z(ipts)
          !        grid(j+1:j+xyz(i)%num,4)=xyz(i)%w
          grid_atom(loc)%w_xyz(j)= cnt%xyz(ir)%w(ipts)
          !        j=j+xyz(i)%num

        end do
      end do

      grid_atom(loc)%num_pts = j

      if(j /= grid_size) stop 'wrong sizes of overall grid cnt'
      !call lnkerr( 'wrong sizes of overall grid cnt')


      j = 0
      grid_atom(loc)%r_num = 0
      angpts = cnt%omega%num
      grid_atom(loc)%ang_num = angpts

      do ir = 1, region_num

        rpts = cnt%r(ir)%num
        grid_atom(loc)%r_num = grid_atom(loc)%r_num + rpts

        do irpts = 1, rpts

          do iang = 1, angpts

            j = j + 1
            grid_atom(loc)%rthetaphi(j,1)= cnt%r(ir)%t(irpts)

            grid_atom(loc)%rthetaphi(j,2)= cnt%omega%theta(iang)

            grid_atom(loc)%rthetaphi(j,3)= cnt%omega%phi(iang)

            grid_atom(loc)%w_r(j)= cnt%r(ir)%w(irpts)

            grid_atom(loc)%w_omega(j)= cnt%omega%w(iang)

          end do !iang

        end do !irpts


        grid_atom(loc)%rad_bound(ir) = cnt%r(ir)%t(rpts)

      end do !ir

     ! print*, 'radial number of points:', grid_atom(loc)%r_num
      grid_atom(loc)%max_radius = cnt%radius
      grid_atom(loc)%center = 0.d0
      grid_atom(loc)%lmax = cnt%lmax

    else

      allocate(grid_atom(num_atoms))
      loc = 0

    end if




    do iatm = 1, num_atoms

      j = 0

      region_num = atm(iatm)%region_num

      grid_size = sum(atm(iatm)%xyz(:)%num)

      allocate(grid_atom(iatm + loc)%xyz(grid_size,3))
      allocate(grid_atom(iatm + loc)%w_xyz(grid_size))
      allocate(grid_atom(iatm + loc)%becke_norm(grid_size))
      allocate(grid_atom(iatm + loc)%rthetaphi(grid_size,3))
      allocate(grid_atom(iatm + loc)%w_r(grid_size))
      allocate(grid_atom(iatm + loc)%w_omega(grid_size))
      allocate(grid_atom(iatm + loc)%rad_bound(region_num))

      do ir= 1, region_num

        pts = atm(iatm )%xyz(ir)%num

        do ipts = 1, pts

          j=j+1
          !        grid(j+1:j+xyz(i)%num,1)=xyz(i)%x
          grid_atom(iatm + loc )%xyz(j,1)= atm(iatm)%xyz(ir)%x(ipts)+atm(iatm)%center(1)
          !        grid(j+1:j+xyz(i)%num,2)=xyz(i)%y
          grid_atom(iatm + loc )%xyz(j,2)=  atm(iatm)%xyz(ir)%y(ipts) +atm(iatm)%center(2)
          !        grid(j+1:j+xyz(i)%num,3)=xyz(i)%z
          grid_atom(iatm + loc )%xyz(j,3)= atm(iatm)%xyz(ir)%z(ipts) +atm(iatm)%center(3)
          !        grid(j+1:j+xyz(i)%num,4)=xyz(i)%w
          grid_atom(iatm + loc )%w_xyz(j)= atm(iatm)%xyz(ir)%w(ipts)
          !        j=j+xyz(i)%num

        end do


      end do

      grid_atom(iatm + loc )%num_pts = j

      if(j /= grid_size)then
        print*, 'wrong sizes of overall grid atm_'//itoc(iatm)
        stop
      endif
      !call lnkerr('wrong sizes of overall grid atm_'//itoc(iatm))


      j = 0
      grid_atom(iatm + loc)%r_num = 0
      angpts = atm(iatm )%omega%num
      grid_atom(iatm + loc)%ang_num = angpts


      do ir = 1, region_num

        rpts = atm(iatm)%r(ir)%num
        grid_atom(iatm + loc)%r_num = grid_atom(iatm + loc)%r_num + rpts


        do irpts = 1, rpts

          do iang = 1, angpts


            j = j + 1
            grid_atom(iatm+loc)%rthetaphi(j,1)= atm(iatm)%r(ir)%t(irpts)

            grid_atom(iatm+loc)%rthetaphi(j,2)= atm(iatm)%omega%theta(iang)

            grid_atom(iatm+loc)%rthetaphi(j,3)= atm(iatm)%omega%phi(iang)

            grid_atom(iatm+loc)%w_r(j)= atm(iatm)%r(ir)%w(irpts)

            grid_atom(iatm+loc)%w_omega(j)= atm(iatm)%omega%w(iang)

          end do !iang angpts

        end do !irpts rpts

        grid_atom(iatm + loc)%rad_bound(ir)=atm(iatm)%r(ir)%t(rpts)

      end do !ir region_num


     ! print*, 'radial number of points:', grid_atom(iatm + loc)%r_num
      grid_atom(iatm + loc )%max_radius = atm(iatm)%radius
      grid_atom(iatm + loc )%center = atm(iatm)%center
      grid_atom(iatm + loc)%lmax = atm(iatm)%lmax


    end do  !iatm



  end subroutine
  ! !   !*********************************************************************************************
  ! !   !! map [-1,1] quadrature points on arbitrary [a,b]=[endpts(1), endpts(2)]
  ! !   !  y = (b-a)/2 x + (a+b)/2, with x the [-1,1] region quadrature points and
  ! !   !  y the [a,b] region quadrature points and
  ! !   !  w_[a,b] = (b-a)* w_[-1,1]/2 are the mapped weights
  ! !   !*********************************************************************************************
  ! !   subroutine map_to_endpoints( t, w, endpts, n )
  ! !     !all rules are from -1 to 1 range and have to be mapped to
  ! !     !the desired range:
  ! !
  ! !     implicit none
  ! !     integer                                         :: i
  ! !     integer                                         :: n
  ! !     real(idp), dimension(n)                         :: t
  ! !     real(idp), dimension(n)                         :: w
  ! !     real(idp), dimension(2)                         :: endpts
  ! !     write(iout,*)'endptsin routine:',endpts(1),endpts(2)
  ! !     do i= 1 , n
  ! !       t(i) = t(i) * (endpts(2)-endpts(1)) * 0.5 + (endpts(2)+endpts(1)) * 0.5
  ! !       w(i) = w(i) * 0.5 * ( endpts(2) - endpts(1) )
  ! !     end do
  ! !   end subroutine map_to_endpoints
  !

  
  subroutine change_gridparams(grid_atom,                        &
                               num_r_cent,edgepts_cent,lmax_cent,&
                               num_atoms,num_r_atms,edge_pts,lmax_atms,cen_atms)
    !*********************************************************************
    ! grid type that includes all information about atoms and central grid
    type(atom_on_grid), allocatable                 :: grid_atom(:)
    !*********************************************************************
    ! character option programmed for Lebedev only NOT mixed (product) angular grid
    character (len=7), parameter                    :: ang_grid_option='lebedev'
    !*********************************************************************
    integer, intent(in)                             :: num_atoms
    integer                                         :: i, ierr
    !*******************************************************************************
    ! lamx associated with central grid (uniform at every r)
    integer, intent(in)                             :: lmax_cent
    ! lmax associated with each atom, array of integer
    integer, intent(in)                             :: lmax_atms(:)
    ! number of radial grid points in an array of region_num size for atoms and center
    integer, intent(in)                             :: num_r_atms(:,:),num_r_cent(:)
    ! number of regions, a variable and discardable integer
    integer                                         :: region_num
    ! 
    integer, allocatable                            :: fixed_end(:,:)
    ! 
    real(idp), intent(in)                           :: edge_pts(:,:), edgepts_cent(:)
    real(idp), intent(in)                           :: cen_atms(:,:)
    !
    character (len=32)                              :: rkind
    !
    type(atom) , allocatable                        :: atm(:)
    type(atom)                                      :: cnt


    if(allocated(grid_atom))then
      deallocate(grid_atom)
    end if  
    !
    rkind = 'legendre'
    !
    cnt%lmax = lmax_cent
   !*****************************************
   ! radial part of central grid
    region_num = size(num_r_cent) 
    allocate(fixed_end(region_num,2))
    !few checks:
     if( size(num_r_cent)/=region_num) stop 'wrong radial point dimension in change_gridparams'
     if( size(edgepts_cent)/=region_num+1) stop 'wrong radial interval boundaries in change_gridparams'
    !***********************************************************
    !always no fixed point at origin and fixed points elsewhere!
    fixed_end(:,:) = 1
    fixed_end(1,1) = 0
    fixed_end(region_num,2) = 1
    !***************************************
    !
    call cnt%initialize_atom(region_num, rkind, fixed_end, edgepts_cent, &
                             num_r_cent)
    call make_radial_grid(cnt%r(:),region_num)
    !   !***********************************************************************************
    cnt%radius = cnt%r(size(cnt%r))%right_edge
    cnt%center = 0.d0
    !   !************************************************************************************
    call set_angular_params_lebedev(cnt%omega,lmax_cent)
    !   !*************************************************************************************
    call cnt%cartesian_grid()
    !   !
    cnt%num_grid_pts = cnt%atom_grid_size()

    if (num_atoms>0) then    
        allocate(atm(1:num_atoms))
    !
    do i = 1, num_atoms
         atm(i)%lmax = lmax_atms(i)
   !*****************************************
   ! radial part of central grid
         region_num = size(num_r_atms(:,i),1) 
         call atm(i)%initialize_atom(region_num, rkind, fixed_end, edge_pts(:,i), &
                                  num_r_atms(:,i))
         call make_radial_grid(atm(i)%r(:),region_num)
    !   !***********************************************************************************
         atm(i)%radius = atm(i)%r(size(atm(i)%r))%right_edge
         atm(i)%center = 0.d0
    !   !************************************************************************************
        call set_angular_params_lebedev(atm(i)%omega,atm(i)%lmax)
    !   !*************************************************************************************
        call atm(i)%cartesian_grid()
    !   !
        atm(i)%num_grid_pts = atm(i)%atom_grid_size()
      !
      !
        if(size(cen_atms,1)/=3) stop 'wrong dimensionality for center'
        if(size(cen_atms,2)/=num_atoms) stop 'wrong dimensionality for center'
        atm(i)%center(:) = cen_atms(:,i)
      !
      !
    end do
    !
    else 
       allocate(atm(0))
    end if
    !
    !print*, 'size of atm:',size(atm)
    call make_overall_grid_atom(grid_atom, atm, cnt)
    !
  end subroutine change_gridparams

  subroutine set_angular_params_lebedev(omega,lmax)
         
    use lebedev_quadrature
    integer                       :: ord, j
    integer, intent(in)           :: lmax
    integer                       :: angnum
    integer                       :: rule

    type(angular)     :: omega
    type(lebedev)     :: leb_ang
    !
    j = 0
    rule = 1
    call leb_ang%get(rule)
    if(leb_ang%precision < lmax .and. rule /= rule_max) then
      j = rule + 1
      do while(j <= rule_max)
        call leb_ang%get(j)
        if (leb_ang%precision>=lmax)then
          exit
        else
          j = leb_ang%rule + 1
        end if
      end do
    endif
    !write(iout,*) 'final lebedev precision (spherical harmonics up to lmax=):', &
    !  leb_ang%precision, 'lebedev num pts:', leb_ang%num

    !
    !   write(iout,*)'Lebedev number of points :',order
    !
    !   call leb_ang%get(ord)!Generate_Lebedev_Points_Weights(leb_ang, leb_rule)
    !
    print*, '--------------------------------------------'
    print*, 'lmax:', lmax
    print*, 'final lebedev precision (spherical harmonics up to lmax=):', &
      leb_ang%precision, 'lebedev num pts:', leb_ang%num
    print*, '---------------------------------------------'
    !
    angnum    = leb_ang%num
    !
    !   write(iout,*)'overall angular number of points:', angnum
    !
    omega%num = angnum
    !
    if(allocated(omega%theta) .or. allocated(omega%phi))         then
      deallocate(omega%theta, omega%sin_theta, omega%cos_theta, &
        omega%phi, omega%sin_phi, omega%cos_phi, omega%w)
    end if
    !
    allocate(omega%theta(angnum), omega%sin_theta(angnum), omega%cos_theta(angnum), &
      omega%phi(angnum), omega%sin_phi(angnum), omega%cos_phi(angnum), omega%w(angnum))
    !   !        write(iout,*)'lebedev_order:',order
    !   !!*************************************************************
    !   !!   fill the angular type omega with leb_ang info
    omega%theta(:)     = leb_ang%theta(:)
    omega%sin_theta(:) = sin(leb_ang%theta(:))
    omega%cos_theta(:) = cos(leb_ang%theta(:))
    omega%phi(:)       = leb_ang%phi(:)
    omega%sin_phi(:)   = sin(leb_ang%phi(:))
    omega%cos_phi(:)   = cos(leb_ang%phi(:))
    omega%w(:)         = 4.d0*pi*leb_ang%w(:)

    ! do j = 1, angnum

    !   print*, 'theta:',omega%theta(j), 'phi:', omega%phi(j), 'w:', omega%w(j)

    ! end do
    !   print*, 'surface integral:', sum(leb_ang%w(:)*omega%sin_theta(:))/4.d0/pi


    !
    !   !!*************************************************
    !   ! write to filename
    !   !!************************************************
    !   ! call iosys('write real "theta" to '//filename,omega%num, omega%theta,0,' ')
    !   ! call iosys('write real "phi" to '//filename,omega%num, omega%phi,0,' ')
    !   ! call iosys('write real "omega weights" to '//filename,omega%num, omega%w,0,' ')
    !
      

  end subroutine   

  subroutine read_stock_params_change_grid_lmax(conf_file_name  ,&
                               num_r_cent,edgepts_cent,lmax_cent,&
                               num_atoms,num_r_atms,edge_pts,lmax_atms,cen_atms)

    integer, intent(out)                            :: num_atoms
    integer                                         :: i, ierr
    !*******************************************************************************
    ! lamx associated with central grid (uniform at every r)
    integer, intent(out)                             :: lmax_cent
    ! lmax associated with each atom, array of integer
    integer, allocatable,intent(out)                             :: lmax_atms(:)
    ! number of radial grid points in an array of region_num size for atoms and center
    integer, allocatable,intent(out)                             :: num_r_atms(:,:),num_r_cent(:)
    ! number of regions, a variable and discardable integer
    integer                                         :: region_num
    ! 
    ! 
    real(idp), intent(out),allocatable              :: edge_pts(:,:), edgepts_cent(:)
    real(idp), intent(out),allocatable              :: cen_atms(:,:)
    real(idp), dimension(:), allocatable            :: cen
    integer                                         :: cardlen, angnum

    character (len=:), allocatable                  :: center_filename
    character (len=32)                              :: rkind
    character (len=32)                              :: ang_grid_option
    character(len=*)                                :: conf_file_name
    character(len=17)                               ::default_conf_file = '../Input/input.in'

    type(cfg_t)                                     :: conf

    logical                                         :: inputfile_exists


    conf = parse_cfg(conf_file_name)

    call conf%get("DEFAULTS","number_of_atoms",num_atoms)
    print*, ' number of atoms: ', num_atoms
    !
    call conf%get("DEFAULTS",'angular_quad_type',ang_grid_option)
    print*, 'angular grid type:',ang_grid_option
    !
    call conf%get("center",'cent_lmax',lmax_cent)
    !   !***************R*******************************************************************
    !   !***********************************************************************************
    call conf%get("center",'region_num', region_num) 
    ! allocations
    allocate(edgepts_cent(region_num + 1))
    call conf%get("center",'r_intervals',edgepts_cent,region_num+1)
    !
    allocate(num_r_cent(region_num))
    call conf%get("center",'r_num_shell_pts', num_r_cent, region_num)
    
    allocate(cen_atms(3,num_atoms))

    do i = 1, num_atoms
       
      center_filename = trim('atom_'//itoc(i))

      call conf%get(center_filename,'atom_center',cen)
      print*, cen
      cen_atms(:,i)= cen(:)
     
    end do  
    
  end subroutine  

  
  
  subroutine change_angular_grid_params(grid_atom,atms,lmax,num)
    !*********************************************************************
    ! grid type that includes all information about atoms and central grid
    type(atom_on_grid),allocatable      :: grid_atom(:)
    type(atom)                          :: atms(:)
    !*********************************************************************
    !*********************************************************************
    integer, intent(in)                 :: num
    integer                             :: num_atoms
    integer                             :: i, ierr
    !*******************************************************************************
    ! lamx associated with central grid first element and atomic grids (2:num_atoms-1)
    ! (uniform at every r)
    integer, intent(in)                 :: lmax(:)
    logical                             :: has_center=.false.

    if(allocated(grid_atom))then
      deallocate(grid_atom)
    end if
    
    num_atoms = size(atms)
    if(num_atoms == num+1) has_center = .true.

   ! if(has_center)then
       if(size(lmax)/=num_atoms) stop 'mismatched lmax array and atms in change_angular_grid_params'   
   ! endif   
    do i = 1, num_atoms

       deallocate(atms(i)%omega%w ,atms(i)%xyz,&
                   atms(i)%omega%theta,        &
                   atms(i)%omega%phi ,         &
                   atms(i)%omega%cos_theta,    &
                   atms(i)%omega%sin_theta,    &
                   atms(i)%omega%cos_phi ,     &
                   atms(i)%omega%sin_phi        )
        
       atms(i)%lmax = lmax(i)          
       call set_angular_params_lebedev(atms(i)%omega,atms(i)%lmax)

       call atms(i)%cartesian_grid()
    !   !
       atms(i)%num_grid_pts = atms(i)%atom_grid_size()

   end do

   if(has_center)then
     call make_overall_grid_atom(grid_atom, atms(2:num_atoms), atms(1))
   else
     call make_overall_grid_atom(grid_atom, atms(1:num_atoms))
   end if  

  end subroutine 


  subroutine change_radial_grid_params(grid_atom,atms,rad)
    !*********************************************************************
    ! grid type that includes all information about atoms and central grid
    type(atom_on_grid),allocatable      :: grid_atom(:)
    type(atom)                          :: atms(:)
    !*********************************************************************
    !*********************************************************************
    integer                             :: num_atoms
    integer                             :: i, ierr, ilast
    integer                             :: num_new
    !*******************************************************************************
    ! lamx associated with central grid first element and atomic grids (2:num_atoms-1)
    ! (uniform at every r)
    real(idp), intent(in)                 :: rad(:)
    real(idp)                             :: rlast_size


    if(allocated(grid_atom))then
      deallocate(grid_atom)
    end if
    
    num_atoms = size(atms)
    if(size(rad)/=num_atoms) stop 'mismatched rad array and atms in change_radial_grid_params'

    do i = 1, num_atoms
       ilast = atms(i)%region_num 
!       print*, 'before:',i, atms(i)%r(ilast)%num
       deallocate(atms(i)%r(ilast)%w ,atms(i)%xyz  &
                   ,atms(i)%r(ilast)%t             &
                  ! ,atms(i)%r(ilast)%norm          &
                   ,atms(i)%r(ilast)%b             &
                   ,atms(i)%r(ilast)%d             &
                  ! ,atms(i)%r(ilast)%pw            &
                  ! ,atms(i)%r(ilast)%p             &
                  ! ,atms(i)%r(ilast)%dp            &
                  ! ,atms(i)%r(ilast)%ddp           &
                   )
       
       !readjust the number of radial points in the last region by a rough ratio
       rlast_size = atms(i)%r(ilast)%right_edge - atms(i)%r(ilast)%left_edge
       num_new = floor(atms(i)%r(ilast)%num / rlast_size * ( rad(i) - atms(i)%r(ilast)%left_edge ) )      
       atms(i)%r(ilast)%right_edge = rad(i)
       atms(i)%radius              = rad(i)
       atms(i)%r(ilast)%num = num_new
              
       call set_radial_params(atms(i)%r(ilast),num_new)

       call atms(i)%cartesian_grid()
    !   !
       atms(i)%num_grid_pts = atms(i)%atom_grid_size()

!       print*, 'after:',i, atms(i)%r(ilast)%num
   end do

   
   call make_overall_grid_atom(grid_atom, atms(2:num_atoms), atms(1))

  end subroutine 


  subroutine set_radial_params(r,num)
      
    type(radial) :: r
    integer      :: num, j


     
      allocate(r%t(num), r%w(num), &
        r%d(num), r%b(num))
      r%num = num
      !     !notice, b is an internal scratch array for the quadrature subroutine
      if(r%fixed_end(1)==1 .and. r%fixed_end(2)==0)then
        !left point is only fixed
        r%kpts=1
        r%endpts(1)= -1.d0
        r%endpts(2)= 1.d0
      elseif(r%fixed_end(1)==0 .and. r%fixed_end(2)==1)then
        r%kpts=1
        r%endpts(1)= 1.d0
        r%endpts(2)= -1.d0
      elseif(r%fixed_end(1)==1 .and. r%fixed_end(2)==1)then
        !both are fixed
        r%kpts=2
        r%endpts(1)=-1.d0
        r%endpts(2)= 1.d0
      else
        !none of the end points are fixed (endpts is irrelevant) or
        r%kpts = 0
      end if

      call gauss_quadrature(trim(r%ckind),r%num, r%kpts,&
        r%endpts, r%b, r%t, r%w)
      !
      r%endpts(1)= r%left_edge
      !
      r%endpts(2)= r%right_edge
      !
      call map_to_endpoints(r%t,r%w,r%endpts,r%num)
      !
      do j= 1,size(r%t(:))
        if(r%t(j) < 0.d0) r%t(j) = 0.d0
      end do
      !     !        assigning r^2 dr
      r%d = r%w * r%t * r%t
      !
      !
      r%called=.true.

      end subroutine  

!>deck Normalization.f90
!!**begin prologue     Normalization
!!**date written       20170410   (yymmdd)
!!**revision date               (yymmdd)
!!**keywords           lobatto functions
!!**author             gharibnejad (nist)
!!**source
!!**purpose            Normalization of dvr
!!**references
!!**routines called    
!!**end prologue       Normalization
subroutine normalization(r,n)

integer                        :: i
integer, intent(in)            :: n
type(radial), dimension(n)     :: r
!! case where there is only one region, simply norm_i = 1/sqrt(weight_i)
if (n == 1) then

   r(1)%norm(:) = 1.d0 / sqrt(r(1)%w(:))

else

   do i= 1, n-1
      !normalizatio of the first point of first region is filled
      if (i==1) then
        r(1)%norm(1) = 1.d0 / sqrt(r(1)%w(1))
        r(1)%pw(1) = r(1)%w(1)
      end if
      !all mid points follow just norm_i = 1/sqrt(weight_i) rule
      r(i)%norm(2:r(i)%num-1) = 1.d0 / sqrt(r(i)%w(2:r(i)%num-1))
      r(i)%pw(2:r(i)%num-1) = r(i)%w(2:r(i)%num-1)

          if((r(i)%fixed_end(2)==1 .and. r(i+1)%fixed_end(1)==1) .or. &
              (r(i)%t(r(i)%num) == r(i+1)%t(1)) )then

              r(i)%norm(r(i)%num)= 1.d0 / sqrt(r(i)%w(r(i)%num) + r(i+1)%w(1))
              r(i)%pw(r(i)%num)  = r(i)%w(r(i)%num) + r(i+1)%w(1)
              !write(iout,*)'ws:',r(i)%w(r(i)%num) , r(i+1)%w(1)
              !write(iout,*)'nor:',r(i)%norm(r(i)%num)
              r(i+1)%norm(1)     = 1.d0 / sqrt(r(i)%w(r(i)%num) + r(i+1)%w(1))
              r(i+1)%pw(1)     = r(i)%w(r(i)%num) + r(i+1)%w(1)
              !write(iout,*)'nor:',r(i+1)%norm(1)
          else

              r(i)%norm(r(i)%num)= 1.d0 / sqrt(r(i)%w(r(i)%num))
              r(i)%pw(r(i)%num)= r(i)%w(r(i)%num)
              r(i+1)%norm(1)     = 1.d0 / sqrt(r(i+1)%w(1))
              r(i+1)%pw(1)     = r(i+1)%w(1)



          endif


   end do

   !last region is filled
   r(n)%norm(2:r(n)%num) = 1.d0 / sqrt(r(n)%w(2:r(n)%num))
   r(n)%pw(2:r(n)%num) = r(n)%w(2:r(n)%num)

end if

end subroutine normalization
!!**********************************************************
!!Given dvr support grid points on the radial grid 
!!a contiguous radial grid is made, but in order to use 
!!this radial grid, attention should be paid to weights
!!on the left and right side of the boundaries, where 
!!the so-called dvr bridge functions reside.
!!**********************************************************
subroutine make_contiguous_r(rad, r, w,indx)

  type (radial)                   :: rad(:)
  real(idp),allocatable           :: r(:)
  real(idp),allocatable, optional :: w(:)
  integer , allocatable, optional :: indx(:)
  integer                    :: ir, nr, rs
  integer                    :: fi, la, np
  
  nr = size(rad)

  rs = sum(rad(:)%num) - nr + 1

  allocate(r(rs))

  if(present(w))then
    if(allocated(w))then
      deallocate(w)
    endif  
    allocate(w(rs))
  endif

  if(present(indx))then
    if (allocated(indx))then
      deallocate(indx)
    endif
    if(nr > 1) allocate(indx(nr-1))
  endif

  fi = 1

  if ( nr == 1 ) then

       r = rad(1)%t
       if(present(w)) w = rad(1)%w
  
  else

        
         if(present(w))then
             
           !fill the first element of the first region
             
             np = rad(1)%num
             
       
             r(fi:np) = rad(1)%t(1:np)
             w(fi:np) = rad(1)%w(1:np) 
             w(np)    = w(np) + rad(2)%w(1)
             if(present(indx)) indx(1) = np
             !print*, fi, np, r(fi:np)
             !take care of the middle region
             ! if nr-1<2 it will skip the loop
             fi = np + 1

            do ir = 2, nr-1

               np = rad(ir)%num - 1

               la = rad(ir)%num + fi - 2

               r(fi:la)=rad(ir)%t(2:np)
               w(fi:la)=rad(ir)%w(2:np)

    
               w(la) = w(la) + rad(ir+1)%w(1)

               if(present(indx)) indx(ir)=la

               fi = la + 1

             end do  
  
                np = rad(nr)%num
                la = fi + rad(nr)%num - 2

                 r(fi:la) = rad(nr)%t(2:np)
                 w(fi:la) = rad(nr)%w(2:np)
                 !print*, r
                 !print*, indx
          else
             
             !fill the first element of the first region
             
             np = rad(1)%num
             
       
             r(fi:np) = rad(1)%t(1:np)
             !print*, fi, np, r(fi:np)
             !take care of the middle region
             ! if nr-1<2 it will skip the loop
             fi = np + 1
             do ir = 2, nr-1
                 
                 np = rad(ir)%num - 1

                 la = rad(ir)%num + fi - 2

                 r(fi:la)=rad(ir)%t(2:np)

                 !print*, fi,la,r(fi:la)

                 fi = la + 1

             end do  
             
             !fill in the last region
             np = rad(nr)%num

             la = fi + rad(nr)%num - 2

              r(fi:la) = rad(nr)%t(2:np)
             
              !print*, fi,la,r(fi:la); stop
         
            end if

  endif

end subroutine  

!!*******************************************************
!! Make a list of what points of central grid are inside
!! the smaller grids
!!*******************************************************

subroutine find_cen_pts_inside_atms(xyz,cent,rmax,list)

    use data_type_sl_list
    !type(atom_on_grid) :: atm_grid(:)
    real(idp),intent(in)          :: xyz(:,:) , cent(:)
    real(idp)                     :: val, rmax
    integer                       :: i, j
    integer                       :: num_pts
    integer, allocatable          :: list(:)
    type(Type_SL_List)            :: lst
     
    num_pts = size(xyz,1)
    
    if(allocated(list))then
      deallocate(list)
    endif

    !loop over points of center grid
    do i = 1, num_pts 
         
         !find the radius of point wrt cent of atom

        ! val = dot_product(xyz(i,:) - cent(:), xyz(i,:) - cent(:))
        ! if( sqrt(val) <= rmax ) then

          val = find_pt_rad_2_cent( xyz(i,:),cent)
          if ( val <= rmax ) then
             !add index to list
             call lst%putt(i)

         end if       

    end do   

    allocate(list(lst%l))

    do i = 1, lst%l

      call lst%get(i,list(i))
      print*, list(i), find_pt_rad_2_cent( xyz(i,:), cent)

    end do   



end subroutine


function find_pt_rad_2_cent(pt, cen) result (rad)
 
  real(idp) :: pt(3), cen(3)
  real(idp) :: rad
  real(idp) :: diff(3)
  
  diff(:) = pt(:) - cen(:)

  rad =  dot_product(diff , diff) 

  rad = sqrt( rad )


end function  

end module gridparams
