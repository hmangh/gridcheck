module becke_luca
  use commonparams
  use gridparams, only: atom_on_grid
  implicit none
  !!***************************************************!!
  !!  Based on Becke paper                              !
  !! of (A. D. Becke, J. Chem. Phys. vol 88, no 4, 1988)!
  !!***************************************************!!
  private

  public form_becke_luca, form_becke_luca2, form_becke_luca_nico


  interface
    function fitting_func(dist_indx,expn, dist_array,v0) result(func)
      use commonparams, only: idp
      integer   :: dist_indx
      real(idp) :: expn
      real(idp) :: dist_array(:)
      real(idp) :: v0
      real(idp) :: func
    end function
  end interface  

contains
!*********************************************************
!
!*********************************************************
  function select_fitting_func(luca_opt) result(f_ptr)
    integer, intent(in) :: luca_opt
    procedure(fitting_func), pointer :: f_ptr

    f_ptr => null()

    select case(luca_opt)

     case(0)
       print*, 'scheme_0'
       f_ptr => scheme_0

     case(1)

       print*, 'scheme_1'
       f_ptr => scheme_1

     case(2)

       print*, 'scheme_2'
       f_ptr => scheme_2

     case(3)

       print*, 'scheme_3'
       f_ptr => scheme_3

     case default

        stop 'error in fitting scheme number in becke_luca module'

    end select

    end function  

!*********************************************************
!
!*********************************************************
    function scheme_0(indx, expn, dist, radius) result(func)
      integer   :: indx
      real(idp) :: expn
      real(idp) :: dist(:)
      real(idp) :: radius
      real(idp) :: func

      ! regular becke, no fitting is done here.
      ! if distance of point from grid center is less than 
      ! radius of center count it in, otherwise zero it

      if (dist(indx) <= radius) then
          func = 1.d0
      else
          func = 0.d0
      end if
          
    end function  

!*********************************************************
!
!*********************************************************
    function scheme_1(indx,expn, dist, radius) result(func)
      integer   :: indx
      real(idp) :: expn
      real(idp) :: dist(:)
      real(idp) :: radius
      real(idp) , parameter :: alpha = log(1.d-20)
      real(idp) :: func

      func = exp(alpha * (dist(indx)/radius) ** (2 * expn))

    end function  

!*********************************************************
!
!*********************************************************
    function scheme_2(indx,expn, dist, v0) result(func)
      integer   :: indx
      integer   :: j
      real(idp) :: expn
      real(idp) :: dist(:)
      real(idp) :: potential
      real(idp) :: v0
      real(idp) :: func
!      real(idp), parameter :: tol = 1.d-17

      !indx here doesn't matter, the potential is over all atoms
      ! starting with indx=2 to size(dist)

      potential = get_point_potential(dist)

!      potential = 0.d0
!      print*,indx, size(dist), dist; stop
!      do j = 2, size(dist)
!       if( dist(j) /= 0.d0) then
!         potential = potential + 1.d0/dist(j)
!       end if 
!      end do


      func = exp(- (v0/potential) ** (2 * expn))
!      if(func < tol) func = 0.d0

    end function  

    
!*********************************************************
!
!*********************************************************
    function scheme_3(indx,expn, dist, v0) result(func)
      integer   :: indx
      integer   :: j
      integer, parameter :: n2 = 4 

      real(idp) :: expn
      real(idp) :: dist(:), v0
      real(idp) :: potential
      real(idp) :: x
      real(idp), parameter :: RPAlpha = 0.1d0
      real(idp)            :: func

      !indx here doesn't matter, the potential is over all atoms
      ! starting with 2 
!      potential = 0.d0
!      do j = 2, size(dist)
!       if( dist(j) /= 0.d0) then
!         potential = potential + 1.d0/dist(j)
!       end if 
!      end do
      potential = get_point_potential(dist)

      x = (RPAlpha**n2+(v0/potential)**n2)**(1.d0/n2) - RPAlpha

      func = exp(- x ** (2 * expn))

    end function  

!**********************************************************
! finds potential of a point with respect to atomic centers
! assuming charge of unit 1 at each center.
!**********************************************************

  function get_point_potential(dist) result(pot)

     real(idp) :: dist(:)
     real(idp) :: pot
     integer   :: i
     real(idp), parameter :: tol = 1.d-40
     
     pot = 0.d0

     do i= 2, size(dist)
!       if(dist(i) < tol) then
!        pot = pot + 1.0 / tol
!       else 
        pot = pot + 1.0 / dist(i)   
!       end if
     end do 

  end function
  !*******************************************************!
  ! given atomic radius (and optionally the radius of the !
  ! center grid) this subroutine calculates the scaled    !
  ! atomic radii according to the perscription of Appendix!
  ! of (A. D. Becke, J. Chem. Phys. vol 88, no 4, 1988)   !
  !*******************************************************!
  subroutine scale_atomic_radii(a_ij,atm)

    type(atom_on_grid)::atm(:)

    real(idp), allocatable :: a_ij(:,:)
    real(idp)              :: u, chi
    integer                :: i, j, size_array


    size_array = size(atm)


    allocate(a_ij(size_array, size_array))

    do i= 1, size(atm)
      do j = 1, size(atm)

        chi = atm(i)%max_radius/atm(j)%max_radius
        !u = (chi - 1)/(chi + 1.d0)
        a_ij(i,j) = 0.25d0 * (1.d0/chi - chi) !u/( u * u - 1.d0)
        if( abs(a_ij(i,j)) > 0.5d0) a_ij(i,j) = sign(0.5d0, a_ij(i,j))

      end do
    end do


  end subroutine

  !***********************************************************!
  ! given a series of atom types and optionally a center      !
  ! the subroutine returns the matrix of inter nuclear        !
  ! distances. If the center is given the last row and column !
  ! of the matrix include the the distance of each atomic     !
  ! nuclues to the center                                     !
  !***********************************************************!
  subroutine inter_nuclear_distance(r_ab,atm)
    real(idp), allocatable :: r_ab(:,:)
    real(idp)              :: u(3)

    type(atom_on_grid) :: atm(:)

    integer    :: i, j
    integer    :: size_array



    size_array = size(atm)


    allocate( r_ab(size_array, size_array))

    do i = 1, size(atm)
      do j = i, size(atm)
        u  =  atm(i)%center - atm(j)%center
        r_ab(i,j)   =  dot_product (u , u)
        r_ab(j,i) = r_ab(i,j)

      end do
    end do
    r_ab = sqrt(r_ab)


  end subroutine
!****************************************************************
! Checks to see if a central grid is present
!****************************************************************
  function central_grid_present(num_atoms, num_grids) result(loc)
 !--------------------------------------------------------------
    integer :: loc
    integer :: num_atoms
    integer :: num_grids
 !--------------------------------------------------------------   
    ! if the given num_atoms matches the number of grids
    ! then it is assumed that there is no "central" grid.
    ! However, there could be a central grid as a fake atom
    ! that would participate in Becke reweighting if num_atoms
    ! input into the routine matches the num_grids
    if(num_atoms == num_grids-1)then
      !then the central atom is stored in the first location
      !in the array, atom_1_indx=2
      loc = 1
    elseif(num_atoms == num_grids)then
      !then there is no central grid, or it is considered as an atom
      !and treated like the rest, atom_1_indx=1
      loc = 0
    else
      ! num_atoms /= num_grids or num_grids - 1
      !call lnkerr('num_atoms should be the same or 1 less of number of grids')
      stop 'num_atoms should be the same or 1 less of number of grids'
    end if

    end function

   !***************************************************  
   ! Optional output 
   !***************************************************
    subroutine open_output(iatm,uid)
    !---------------------------------
    integer :: iostat, uid
    integer, intent(in) :: iatm
    character(len=512) :: iomsg
    character(len=24)  :: strnBuf
    !----------------------------------
            write(strnBuf,"(i0)") iatm
            open(newunit=uid,&
              file   ="Grid_"//trim(adjustl(strnBuf)),&
              form   ="formatted", &
              status ="unknown", &
              action ="write", &
              iostat =iostat, &
              iomsg  =iomsg)
            if(iostat/=0)then
              write(*,"(a)") trim(iomsg)
              stop
            endif
    end subroutine

   !*******************************************************
   ! writing to output
   !*******************************************************
    subroutine write_output(cartesian_pt, becke_norm, uid)
    !-----------------------------------------------------
        real(idp) :: cartesian_pt(3)
        real(idp) :: becke_norm
        integer   :: uid        
    !----------------------------------------------------
                write(uid,"(4(x,e24.16))") &
                  cartesian_pt(1), & 
                  cartesian_pt(2), &
                  cartesian_pt(3), &
                  becke_norm

    end subroutine            

!********************************************************
! regular becke weights are found
!********************************************************
    function get_becke_weights(indx1, num_grids, dist, &
                             r_jk, a_jk, numiter) result(p)
    !------------------------------------------------------
      integer :: indx1
      integer :: num_grids
      integer :: numiter
      integer :: j, k

      real(idp) :: fac, norm
      real(idp) :: dist(:)
      real(idp) :: r_jk(:,:)
      real(idp) :: a_jk(:,:)
      real(idp) :: p(num_grids)
    !-----------------------------------------------------
      
        p(:) = 1.d0

        do j = indx1, num_grids

          do k = indx1, j-1

              fac = find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter)

              !if(1.d0 - fac < 0.d0 .or. 1.d0 + fac < 0.d0 ) then
              !  write(iout,*) '> 1 factors: ','iatm:',iatm,'ipt:',ipt,'fac:', fac
              !end if

              p(j) = p(j) * 0.5d0 * (1.d0 - fac)

          end do

          do k = j+1, num_grids


              fac= find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter)

              !if(1.d0 - fac < 0.d0 .or. 1.d0 + fac < 0.d0 ) then
              !  write(iout,*) '> 1 factors: ','iatm:',iatm,'ipt:',ipt,'fac:', fac
              !end if

              p(j) = p(j) * 0.5d0 * (1.d0 - fac)

          end do

        end do  !j loop

      
        norm = 0.d0

        do  j = indx1, num_grids

          norm = norm + p(j)

        end  do !j loop

        do j = indx1, num_grids
          p(j) = p(j)/norm
!          print*,'atm', p(j)
        end do


           

    end function


!***********************************************************************
!*********************************************************
!
!*********************************************************
    subroutine modify_becke_weights(func,p, atom_1_indx, num_grids,&
                                      v0, dist, exp_n, alpha)
       !----------------------------------------------------------------
             procedure(fitting_func), pointer :: func 
             integer, intent(in) :: atom_1_indx, num_grids
             integer             :: j 
             real(idp), intent(in) :: dist(:), exp_n, alpha
             real(idp)             :: p(:), sum_refitted_w , v0(:)            
       !----------------------------------------------------------------                
             sum_refitted_w = 0.d0
             do j = atom_1_indx, num_grids

               p(j) = p(j) * func(j, exp_n,dist, v0(j))  !exp(alpha*(dist(j)/radius(j))**(2*exp_n))
!              p(j) = p(j) * 1.d0 / (1.d0 + exp(-exp_n * (0.5d0*beta*atm(j)%max_radius - dist(j)))) 
!               if(dist(j) >= 0.7d0 * radius)then
!                   p(j) = p(j) * exp(alpha* ((dist(j) - 0.7d0 * radius)/radius)**(2*exp_n))
!               end if  
!              print*, 'dist from atm:', dist(j)
!              print*,'atm', p(j)
               sum_refitted_w = sum_refitted_w + p(j)
             end do

             p(1) = 1.d0 - sum_refitted_w
             if(p(1) < 0.d0) then
                   !print*,'less!', p(1)
                   !stop 'less than zero p(1)!!'
                 p(1) = 0.d0
             end if
!            print*,'cent',p(1)

    end subroutine    


!*********************************************************
!
!*********************************************************
    subroutine select_fitting_params(atm,Luca_opt,v0)
     !-------------------------------------------   
      type(atom_on_grid)     :: atm(:)
      real(idp), allocatable :: v0(:)
      integer                :: Luca_opt
     !-------------------------------------------
      
      allocate(v0(size(atm)))

      select case(Luca_opt)

      case(1,0)

        v0(:) = atm(:)%max_radius
      
      case(2)

        v0(:) = 1.d0      
     
      case(3) 

        v0(:) = 0.75d0 

      end select 

    end subroutine   


!********************************************************
!
!
!********************************************************
  subroutine form_becke_luca(atm,num_atoms,exp_n ,Luca)
!--------------------------------------------------------
    type(atom_on_grid) :: atm(:)

    integer, parameter :: numiter = 3
    integer,intent(in) :: num_atoms
    integer    :: iatm, atom_1_indx
    integer    :: i, j, k
    integer    :: ipt, loc, num_grids
    integer    :: uid

    real(idp)  :: exp_n  ! = 2.0
    real(idp), parameter   :: beta =  1.3d0
    real(idp), parameter   :: alpha = log(1.d-20)
    real(idp), allocatable :: r_jk(:,:)
    real(idp), allocatable :: a_jk(:,:)
    real(idp), allocatable :: dist(:),p(:), v0(:)
    real(idp)              :: radius, norm, fac, fac1
    real(idp)              :: sum_refitted_w

    procedure(fitting_func), pointer :: fit_func
    integer, optional      :: Luca
 !--------------------------------------------------------
!    print*, alpha

    
    if(present(Luca)) then

       fit_func => select_fitting_func(Luca)
       call select_fitting_params(atm,Luca,v0)

    else

       fit_func => select_fitting_func(0)
       call select_fitting_params(atm,0,v0)

    end if   

    call inter_nuclear_distance(r_jk, atm)

    call scale_atomic_radii(a_jk, atm)

    !option not to scale:
    ! a_jk = 0.d0

    num_grids = size(atm)

    loc = central_grid_present(num_atoms, num_grids)

    atom_1_indx = loc + 1
    write(iout,*)'first atom index:', atom_1_indx,'central grid?:', loc,&
      'total num of atoms:',num_atoms, 'number of grids:',num_grids

    allocate(dist(num_grids),p(num_grids))

    !loop over all grids: atoms and the central grid, if present!
    do iatm = 1, num_grids

     !call open_output(iatm,uid)

      !loop over all grid points
      do ipt = 1, atm(iatm)%num_pts

        !go over all centers
        !find distance of point (ipt) from all  centers

        do j = 1, num_grids
          ! write(iout,*) atm(iatm)%xyz(ipt,:)!, atm(j)%center
          dist(j) = distance_pt_from_center(atm(iatm)%xyz(ipt,:),atm(j)%center)
          !write(iout,*) dist(j)
        end do !j loop

        p = get_becke_weights(atom_1_indx, num_grids, dist, r_jk, a_jk, numiter)


       ! if(present(Luca))then
          ! Luca Argenti fix
          ! Up to here the central grid has no Becke weights
          ! the usual Becke weights for atomic centers is refitted
          ! with a decaying exponential function with respect to
          ! the atomic centers.
          if(loc == 1 ) then

            call modify_becke_weights (fit_func,p, atom_1_indx, num_grids,&
                                       v0, dist, exp_n, alpha)
            

          else

            write(iout, *) 'Luca option is not performed for the Becke weights. '
            write(iout, *) 'num_grids is equal to the num_atoms. ',&
              'Central grid is not counted as an atom in Luca option'

          end if

        !end if

        atm(iatm)%becke_norm(ipt) = p(iatm)

        !call write_output(atm(iatm)%xyz(ipt,:),p(iatm),uid)

        !call lnkerr ('stopping')
      end do !ipt loop

      !      close(uid)
    end do !iatm loop


  end subroutine

  !************************************************************************************
  ! finds distance of a cartesian point in the grid from a center point
  !************************************************************************************
  function distance_pt_from_center( atom_i_coord,center) result(dis)

    integer   :: i
    real(idp) :: atom_i_coord(:)
    real(idp) :: center(3), diff(3)
    real(idp) :: dis



    do i = 1, 3

      diff(i) = (atom_i_coord(i) - center(i))
      diff(i) = diff(i) * diff(i)

    end do

    dis = sqrt(sum(diff(:)))


  end function

  !***************************************************************
  !
  !***************************************************************
  function radius_pt(atom_i_pt) result(rad)

    real(idp) :: atom_i_pt(:)
    real(idp) :: coord2(3)
    real(idp) :: rad
    integer   :: i

    do i= 1, 3

      coord2(i) = atom_i_pt(i) * atom_i_pt(i)

    end do

    rad = sqrt(sum(coord2(:)))

  end function


!*********************************************************
!
!*********************************************************
  function find_factor(dist1, dist2, r_12, a_12,numiter)result(fac)

    real(idp) :: dist1, dist2, r_12, a_12
    real(idp) :: diff, fac, s

    integer   :: numiter
    integer   :: iter
    !    write(iout,*) 'distances:',dist1,dist2, 'r_12:',r_12
    diff = (dist1 - dist2)/ r_12
    fac = diff + a_12*(1.d0 - diff * diff)

    do iter = 1, numiter
      s = (1.5d0 - 0.5d0 * fac * fac) * fac
      fac = s
    end do

    !    write(iout,*) 'diff:',diff
    !    write(iout,*) 'fac:',fac

  end function

!*********************************************************
!
!*********************************************************
  subroutine form_becke_luca_nico(atm,num_atoms,exp_n ,Luca)

    type(atom_on_grid) :: atm(:)

    integer,intent(in) :: num_atoms
    integer    :: iatm, atom_1_indx
    integer    :: i, j, k
    integer    :: ipt, loc, num_grids
    integer, parameter :: numiter = 3
   real(idp) :: exp_n!, parameter :: exp_n   = 4.0

    real(idp), parameter   :: alpha = log(1.d-20)
    real(idp), allocatable :: r_jk(:,:)
    real(idp), allocatable :: a_jk(:,:)
    real(idp), allocatable :: dist(:),p(:)
    real(idp)              :: radius, norm, fac, fac1
    real(idp)              :: sum_refitted_w, ar

    logical, optional      :: Luca

    integer :: iostat, uid
    character(len=512) :: iomsg
    character(len=24)  :: strnBuf

    print*, alpha

    call inter_nuclear_distance(r_jk, atm)

    call scale_atomic_radii(a_jk, atm)
    !don't scale:
    !a_jk = 0.d0

    num_grids = size(atm)

    ! if the given num_atoms matches the number of grids
    ! then it is assumed that there is no "central" grid.
    ! However, there could be a central grid as a fake atom
    ! that would participate in Becke reweighting if num_atoms
    ! input into the routine matches the num_grids
    if(num_atoms == num_grids-1)then
      !then the central atom is stored in the first location
      !in the array, atom_1_indx=2
      loc = 1
    elseif(num_atoms == num_grids)then
      !then there is no central grid, or it is considered as an atom
      !and treated like the rest, atom_1_indx=1
      loc = 0
    else
      ! num_atoms /= num_grids or num_grids - 1
      !call lnkerr('num_atoms should be the same or 1 less of number of grids')
      stop 'num_atoms should be the same or 1 less of number of grids'
    end if

    atom_1_indx = loc + 1
    write(iout,*)'first atom index:', atom_1_indx,'central grid?:', loc,&
      'total num of atoms:',num_atoms, 'number of grids:',num_grids

    allocate(dist(num_grids),p(num_grids))

    !loop over all grids: atoms and the central grid, if present!
    do iatm = 1, num_grids

      !      write(strnBuf,"(i0)") iatm
      !      open(newunit=uid,&
      !        file   ="Grid_"//trim(adjustl(strnBuf)),&
      !        form   ="formatted", &
      !        status ="unknown", &
      !        action ="write", &
      !        iostat =iostat, &
      !        iomsg  =iomsg)
      !      if(iostat/=0)then
      !        write(*,"(a)") trim(iomsg)
      !        stop
      !      endif

      !loop over all grid points
      do ipt = 1, atm(iatm)%num_pts
        !go over all centers
        !find distance of point (ipt) from all  centers

        do j = 1, num_grids
          ! write(iout,*) atm(iatm)%xyz(ipt,:)!, atm(j)%center
          dist(j) = distance_pt_from_center(atm(iatm)%xyz(ipt,:),atm(j)%center)
          
          !write(iout,*) dist(j)
        end do !j loop

        p(:) = 1.d0

!        do j = atom_1_indx, num_grids
!
!          do k = atom_1_indx, j
!
!            if(k /= j) then
!              if(iatm==1) then
!              fac= find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter)
!
!              if(1.d0 - fac < 0.0) then
!                write(iout,*) '> 1 factors: ','iatm:',iatm,'ipt:',ipt,'fac:', fac
!              end if
!!
!              p(j) = p(j) * 0.5d0 * (1.d0 - fac)
!              p(k) = p(k) * 0.5d0 * (1.d0 + fac)
!              elseif (dist(j) <= atm(j)%max_radius .and. dist(k) <= atm(k)%max_radius ) then 
!              fac= find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter)
!
!              if(1.d0 - fac < 0.0) then
!                write(iout,*) '> 1 factors: ','iatm:',iatm,'ipt:',ipt,'fac:', fac
!              end if
!
!              p(j) = p(j) * 0.5d0 * (1.d0 - fac)
!              p(k) = p(k) * 0.5d0 * (1.d0 + fac)
!              end if
!              ! write(iout,*)'jk:',j,k,'p(j):', p(j), 'p(k):',p(k)
!            end if
!
!          end do
!
!          !          do k = j+1, num_atoms
!
!          !            fac= find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter)
!
!          !          end do
!        end do  !j loop



        norm = 0.d0


!        do  j = atom_1_indx, num_grids

!          norm = norm + p(j)

!        end  do !j loop

!        do j = atom_1_indx, num_grids
!          p(j) = p(j)/norm
!          print*, p(j)
!        end do


        if(present(Luca))then
          ! Luca Argenti fix
          ! Up to here the central grid has no Becke weights
          ! the usual Becke weights for atomic centers is refitted
          ! with a decaying exponential function with respect to
          ! the atomic centers.
          if(loc /= 0 ) then
            sum_refitted_w = 0.d0
            do j = atom_1_indx, num_grids
              ar = 0.5d0 * atm(j)%max_radius
              
              if(dist(j) > ar) then
              p(j) = p(j) * exp(alpha*(dist(j)/atm(j)%max_radius)**(2*exp_n))
              end if
!              print*, p(j)
              sum_refitted_w = sum_refitted_w + p(j)
            end do

            p(loc) = 1.d0 - sum_refitted_w
!            print*,p(loc)

          elseif(Luca .eqv. .true.) then

            write(iout, *) 'Luca option is not performed for the Becke weights. '
            write(iout, *) 'num_grids is equal to the num_atoms. ',&
              'Central grid is not counted as an atom in Luca option'

          end if

        end if
        atm(iatm)%becke_norm(ipt) = p(iatm)

        !        write(uid,"(4(x,e24.16))") &
        !          atm(iatm)%xyz(ipt,1), & 
        !          atm(iatm)%xyz(ipt,2), &
        !          atm(iatm)%xyz(ipt,3), &
        !          atm(iatm)%becke_norm(ipt) 

        !call lnkerr ('stopping')
      end do !ipt loop

      !      close(uid)
    end do !iatm loop


  end subroutine form_becke_luca_nico
!************************************************************
! trying to use the nuclear potential to perhaps make smoother
! profiles
!*************************************************************
  subroutine form_becke_luca2(atm,num_atoms,exp_n ,Luca)
!------------------------------------------------------------
    type(atom_on_grid) :: atm(:)

    integer,intent(in) :: num_atoms
    integer    :: iatm, atom_1_indx
    integer    :: i, j, k
    integer    :: ipt, loc, num_grids
    integer    :: iostat, uid
    integer, parameter :: numiter = 3

    real(idp)              :: exp_n  ! = 2.0
    real(idp)              :: dist_Rz_jk
    real(idp), parameter   :: beta =  1.3d0
    real(idp), parameter   :: alpha = log(1.d-20)
    real(idp), allocatable :: r_jk(:,:)
    real(idp), allocatable :: a_jk(:,:)
    real(idp), allocatable :: dist(:),p(:)
    real(idp), allocatable :: cen_z(:), pot_Rz(:)
    real(idp)              :: radius, norm, fac, fac1
    real(idp)              :: sum_refitted_w
    real(idp)              :: pot_r_j, nor, vec_R(3)
    
    logical, optional      :: Luca

    character(len=512) :: iomsg
    character(len=24)  :: strnBuf

    real(idp)            :: x
    real(idp), parameter :: RPAlpha = 0.1d0
    real(idp), parameter :: V0 = 0.75d0
    integer  , parameter :: n2 = 4
  !----------------------------------------------------
!    print*, alpha

    call inter_nuclear_distance(r_jk, atm)

    call scale_atomic_radii(a_jk, atm)
    !don't scale:
    !a_jk = 0.d0

    num_grids = size(atm)

    ! if the given num_atoms matches the number of grids
    ! then it is assumed that there is no "central" grid.
    ! However, there could be a central grid as a fake atom
    ! that would participate in Becke reweighting if num_atoms
    ! input into the routine matches the num_grids
    if(num_atoms == num_grids-1)then
      !then the central atom is stored in the first location
      !in the array, atom_1_indx=2
      loc = 1
    elseif(num_atoms == num_grids)then
      !then there is no central grid, or it is considered as an atom
      !and treated like the rest, atom_1_indx=1
      loc = 0
    else
      ! num_atoms /= num_grids or num_grids - 1
      !call lnkerr('num_atoms should be the same or 1 less of number of grids')
      stop 'num_atoms should be the same or 1 less of number of grids'
    end if

    atom_1_indx = loc + 1
    write(iout,*)'first atom index:', atom_1_indx,'central grid?:', loc,&
      'total num of atoms:',num_atoms, 'number of grids:',num_grids

    allocate(dist(num_grids),p(num_grids))
    
    allocate(cen_z(atom_1_indx:num_grids), pot_Rz(atom_1_indx:num_grids))

!    pot_Rz(:) = 0.d0 
!    do j = atom_1_indx, num_grids
!
!      cen_z(j) = atm(j)%center(3)
!
!      do k = atom_1_indx, num_grids  
!        ! Let's look at the value of pot_r for r = R along the z-axis
!        ! R_z:
!        
!        dist_Rz_jk = distance_pt_from_center( [0.d0,0.d0,atm(j)%max_radius*sign(1.d0,cen_z(j))*0.5d0], &
!                                           atm(k)%center )
!
!        pot_Rz(j) = pot_Rz(j) + 1.d0 / dist_Rz_jk                                 
!    
!      end do                                   
!    
!    end do     

    !loop over all grids: atoms and the central grid, if present!
    do iatm = 1, num_grids




      !      write(strnBuf,"(i0)") iatm
      !      open(newunit=uid,&
      !        file   ="Grid_"//trim(adjustl(strnBuf)),&
      !        form   ="formatted", &
      !        status ="unknown", &
      !        action ="write", &
      !        iostat =iostat, &
      !        iomsg  =iomsg)
      !      if(iostat/=0)then
      !        write(*,"(a)") trim(iomsg)
      !        stop
      !      endif

      !loop over all grid points
      do ipt = 1, atm(iatm)%num_pts

        !* ****************************************************
        ! try to look along the vector of the point to the r = R
           
        pot_Rz(:) = 0.d0
        do j = atom_1_indx, num_grids

         nor = distance_pt_from_center(atm(iatm)%xyz(ipt,:),atm(j)%center)
         vec_R =  atm(iatm)%xyz(ipt,:)-atm(j)%center
         vec_R = vec_R/ nor

           do k = atom_1_indx, num_grids  
        
               dist_Rz_jk = distance_pt_from_center( atm(j)%max_radius*vec_R, &
                                           atm(k)%center )
       !         if(dist_Rz_jk /= atm(j)%max_radius) then
       !           print*, dist_Rz_jk, atm(j)%max_radius
       !           stop 'no!'
       !         end if  
               pot_Rz(j) = pot_Rz(j) + 1.d0 / dist_Rz_jk                                 
    
           end do

            ! pot_Rz(j) = 1.d0 / atm(j)%max_radius       
        end do
        

        !go over all centers
        !find distance of point (ipt) from all  centers
        do j = 1, num_grids
          ! write(iout,*) atm(iatm)%xyz(ipt,:)!, atm(j)%center
          dist(j) = distance_pt_from_center(atm(iatm)%xyz(ipt,:),atm(j)%center)
          !write(iout,*) dist(j)
        end do !j loop

        ! finding the bare (hydrogenic) nucleus potential at point ipt
        ! notice looping only over atomic centers 
        pot_r_j = 0.d0
        do j = atom_1_indx, num_grids
              pot_r_j = pot_r_j + 1.d0/dist(j)
        end do 

        p(:) = 1.d0

        do j = atom_1_indx, num_grids

          do k = atom_1_indx, j

            if(k /= j) then

              fac= find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter)

             ! if(1.d0 - fac < 0.d0 .or. 1.d0 + fac < 0.d0 ) then
             !   write(iout,*) '> 1 factors: ','iatm:',iatm,'ipt:',ipt,'fac:', fac
             ! end if

              p(j) = p(j) * 0.5d0 * (1.d0 - fac)
              p(k) = p(k) * 0.5d0 * (1.d0 + fac)
              ! write(iout,*)'jk:',j,k,'p(j):', p(j), 'p(k):',p(k)
            end if

          end do

          !          do k = j+1, num_atoms

          !            fac= find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter)

          !          end do
        end do  !j loop



        norm = 0.d0


        do  j = atom_1_indx, num_grids

          norm = norm + p(j)

        end  do !j loop

        do j = atom_1_indx, num_grids

           p(j) = p(j)/norm

!          print*,'atm', p(j)

        end do


        if(present(Luca))then
          ! Luca Argenti fix
          ! Up to here the central grid has no Becke weights
          ! the usual Becke weights for atomic centers is refitted
          ! with a decaying exponential function with respect to
          ! the atomic centers.
          

          if(loc == 1 ) then
            sum_refitted_w = 0.d0
            do j = atom_1_indx, num_grids
               radius = atm(j)%max_radius
               x = (RPAlpha**n2+(V0/pot_r_j)**n2)**(1.d0/n2) - RPAlpha
!               print*, pot_r_j, exp(- (1.d0/pot_r_j)**(2*exp_n))
!               p(j) = p(j) * exp(- (1.d0/pot_r_j)**(2*exp_n))
               p(j) = p(j) * exp(- x**(2*exp_n))
!               p(j) = p(j) *exp(- pot_r_j**exp_n)
!               p(j) = p(j) * exp(alpha * (pot_Rz(j)/pot_r_j)**(2*exp_n))
!               p(j) = p(j) * exp(alpha* (pot_Rz(j)/pot_r_j)**(exp_n))
!              p(j) = p(j) * 1.d0 / (1.d0 + exp(-exp_n * (0.5d0*beta*atm(j)%max_radius - dist(j)))) 
!               if(dist(j) >= 0.7d0 * radius)then
!                   p(j) = p(j) * exp(alpha* ((dist(j) - 0.7d0 * radius)/radius)**(2*exp_n))
!               end if  
!              print*, 'dist from atm:', dist(j)
!              print*,'atm', p(j)
              sum_refitted_w = sum_refitted_w + p(j)
            end do

            p(loc) = 1.d0 - sum_refitted_w
!            print*,'cent',p(loc)

          elseif(Luca .eqv. .true.) then

            write(iout, *) 'Luca option is not performed for the Becke weights. '
            write(iout, *) 'num_grids is equal to the num_atoms. ',&
              'Central grid is not counted as an atom in Luca option'

          end if

        end if
        atm(iatm)%becke_norm(ipt) = p(iatm)

        !        write(uid,"(4(x,e24.16))") &
        !          atm(iatm)%xyz(ipt,1), & 
        !          atm(iatm)%xyz(ipt,2), &
        !          atm(iatm)%xyz(ipt,3), &
        !          atm(iatm)%becke_norm(ipt) 

        !call lnkerr ('stopping')
      end do !ipt loop

      !      close(uid)
    end do !iatm loop


  end subroutine
  
!!******************************************************
!!Scheme 1 for finding the radii for atomic grids
!! This is abandoned and not implemented
!!******************************************************
subroutine scheme1(num_cen,cen, radii, z_arr,a_ij)
  
  integer :: num_cen
  integer :: i, j
  ! this is the center matrix, is supposed to be
  ! dim1 = 3 for x,y,z coordinates of each center 
  ! dim2 = number of centers
  real(idp) :: cen(3,num_cen)
  ! the scaling array according to Bragg-Slater radii
  ! this is in fact a parameter in the scaling that 
  ! determines where the cell boundary is. If a_{ij} = 0
  ! cell boundary is the plane intersected by the axis between
  ! two nuclei. 
  real(idp) :: a_ij(num_cen, num_cen)
  
  real(idp) :: radii(num_cen)

  real(idp) :: z_arr(num_cen)

  real(idp) :: potential, sol(num_cen, num_cen)

  real(idp) :: chai, mu 
  
  ! Find the boundary on the axis between a pair of nuclei
  ! In Becke's formulation the shifted boundary parametr is
  ! \nu_{ij} = \mu_{ij} + a_{ij} (1 - \mu_{ij}^2)
  ! the boundary is at \nu_{ij} = 0 so the solution of |\mu_{ij}| < 1.
  ! determines the place of the boundary point on the axis.
   a_ij = 0.d0

   do i = 1, num_cen
     do j = i+1 , num_cen

       chai = radii(i)/radii(j)
       mu   = chai - 1.d0
       mu   = mu / (chai + 1.d0)
       a_ij(i,j) = mu / (mu * mu - 1.d0)     

       if( abs(a_ij(i,j)) > 0.5d0) a_ij(i,j) = sign(0.5d0, a_ij(i,j))

       if(a_ij(i,j) == 0.d0) then
          
           sol (i, j) = 0.5d0

       else

           sol(i,j) = (-1.d0 + sqrt( 1.d0 + 4.d0 * a_ij(i,j)))*(-0.5d0) / a_ij(i,j) 
  
       end if   

     end do
   end do 

!   do i = 1, num_cen
!      do j = i+1, num_cen

!           sol(i,j) * dist(i,j)   

  !systematically find the radius of each atom by examining 
  !bare nucleus charge potential 

  ! potential = potentual + 1.d0 /    

  end subroutine 

end module
