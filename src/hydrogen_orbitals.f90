module hydrogen_orbitals
  use commonparams
  implicit none
  private
  !public

  contains

    subroutine get_hydrogen_orbital_functions(grid,orbitals,num_orbitals)
      integer  :: num_orbitals
      integer  :: grid_size

      real(idp),allocatable :: orbitals(:,:)
      real(idp) :: grid (:,:)
      real(idp), allocatable :: r(:)
      grid_size = size(grid,1)
      allocate(orbitals(grid_size,num_orbitals),r(grid_size))




    end subroutine

    function one_s(grid) result(s1)
      real(idp) :: grid (:,:)
      real(idp),allocatable :: s1(:), r(:)
      allocate(s1(size(grid,1)),r(size(grid,1)))
      r(:) = grid(:,1)*grid(:,1) + grid(:,2)*grid(:,2) + grid(:,3)*grid(:,3)
      s1(:) = exp(- r(:) )
      s1 = 1.d0/sqrt(pi) * s1
    end function

    function two_s(grid) result(s2)
      real(idp) :: grid (:,:)
      real(idp),allocatable :: s2(:), r(:)
      allocate(s2(size(grid,1)),r(size(grid,1)))
      r(:) = grid(:,1)*grid(:,1) + grid(:,2)*grid(:,2) + grid(:,3)*grid(:,3)
      s2(:) = exp(- 0.5d0 * r(:))
      s2 = 1.d0/sqrt(pi)/sqrt(8.d0) * s2
      s2 = s2 * (1.d0 - 0.5d0 * r)
    end function


    function three_s(grid) result(s3)
      real(idp) :: grid (:,:)
      real(idp),allocatable :: s3(:), r(:)
      allocate(s3(size(grid,1)),r(size(grid,1)))
      r(:) = grid(:,1)*grid(:,1) + grid(:,2)*grid(:,2) + grid(:,3)*grid(:,3)
      s3(:) = exp(- r(:)/3.d0)
      s3 = 1.d0/sqrt(3.d0 * pi)/81.d0 * s3
      s3 = s3 * (27.d0 - 18.d0 * r + 2.d0 * r * r )
    end function

end module
