module gto_mod
    use commonparams, only: idp
    use lmindx_map, only: lookup_l
    use sphericalHarmonics, only: spherical_harmonics_trans_shell
    implicit none

    private

    public gtos_data, gto_atom_orbital

    type gtos_data
        integer :: num, l
        character(len=1)::symbol
        ! exponents of the Gaussian type orbitals
        real(idp), allocatable :: expon (:)
        ! contraction coefficients of GTO
        real(idp), allocatable :: cont_coeff(:)
        ! power of x,y,z of the Cartesian GTO
        integer  , allocatable :: cart_pwr(:,:)
        ! Cartesian to Spherical harmonics transform matrix
        real(idp), allocatable :: cart2sph_coef(:,:)
        contains
        procedure:: initialize_gto
        generic, public :: initialize=>initialize_gto    
    end type gtos_data

    type gto_atom_orbital
        integer :: num
        type (gtos_data), allocatable :: gto (:)
        real(idp) :: center(3)
        contains 
        procedure :: initialize_atom_orbital
        generic, public :: initialize=>initialize_atom_orbital
    end type gto_atom_orbital

    contains

    !initialize gto type
    subroutine initialize_gto(self,n,symb)
        class(gtos_data):: self
        integer, intent(in) :: n
        integer :: l, size_cart
        character(len=1):: symb

        if (.not. allocated(self%expon) .and. .not. allocated(self%cont_coeff))then

            self%num = n
            self%symbol = symb
            l = lookup_l(symb)
            self%l = l

            allocate(self%expon(n))
            allocate(self%cont_coeff(n))

            size_cart = (l+1) * (l+2) / 2
 
            allocate(self%cart_pwr(3,size_cart))
            allocate(self%cart2sph_coef(-l:l,size_cart))

            ! fill the powers and transformation matrix of cart to sph harmonics
            call spherical_harmonics_trans_shell(l, self%cart_pwr, self%cart2sph_coef)

        else
            
            stop 'attempt to reallocate already allocated gtos_data '

        end if
       

    end subroutine

    !initialize atom_orbitals type
    subroutine initialize_atom_orbital(self,n,cent)
        class(gto_atom_orbital):: self
        integer, intent(in) :: n
        real(idp) :: cent(3)
        

        if (.not. allocated(self%gto) )then

            self%num = n

            self%center = cent

            allocate(self%gto(n))           
            

        else
            
            stop 'attempt to reallocate already allocated gto_atom_orbital '

        end if
       

    end subroutine

end module    