module sphereVolumeTest
use commonparams
use gridparams, only : atom_on_grid
implicit none
private

public integral_sphere

contains
!************************************************************
!Description : checks the overall integral of the grid to
!              see how far off from the actual sphere volume
!              it might be after redistribution of weights
!              with various schemes, such as becke
!@input      : defined type:: atm_grid: (description of type 
!                             is in gridparams) 1d-array
!            : real :: rad : radius of overall grid (i.e. 
!                            largest radius
!************************************************************
  subroutine integral_sphere(atm_grid, rad)
   !-------------------------------------------------------- 
    type(atom_on_grid)     :: atm_grid(:)
    
    real(idp),allocatable  :: intg(:)
    real(idp)              :: rad
    real(idp)              :: analytic
    real(idp)              :: dist   
    real(idp),parameter    :: eps = 1.d-13
    integer                :: num_cent
    integer                :: ipt, icent, j, tot_gsiz
    integer                :: count
   !------------------------------------------------------- 
   ! if(.not. allocated(atm_grid))then
   !    call 
   ! end if   
    num_cent  = size(atm_grid)
    tot_gsiz  = sum(atm_grid(:)%num_pts)
    allocate(intg(num_cent))

    ! write(iout,*) 'check if w_n adds to 1'
    ! do icent = 1, num_cent

    !   do ipt = 1, atm_grid(icent)%num_pts

    !     write(iout,*) sum(atm_grid(icent)%becke_norm(ipt))

    !   end do
    ! end do

    do icent = 1, num_cent
    intg(icent) = 0.d0
    count = 0
      do ipt = 1, atm_grid(icent)%num_pts

!        if(atm_grid(icent)%rthetaphi(ipt,1) <= rad) then
        dist = dot_product(atm_grid(icent)%xyz(ipt,:),atm_grid(icent)%xyz(ipt,:))
        dist = sqrt(dist)
        if(dist <= rad+eps) then  
          count = count + 1
          intg(icent) = intg(icent) + &
            atm_grid(icent)%becke_norm(ipt)*&
            atm_grid(icent)%w_xyz(ipt)
        end if



      end do
      print*, 'for center', icent,':'
      print*, count, 'points out of',atm_grid(icent)%num_pts ,'lie inside radius of', rad
    end do
    analytic = 4.d0 * pi * rad **3/3.d0

    write(iout,*) 'integral of sphere with radius=',rad
    write(*,*) 'integral of sphere with radius=',rad
    if(num_cent > 1) then
      write(iout,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
      write(*,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
    end if  
    write(iout,*) 'intgral:', intg(:), sum(intg)
    write(iout,*) 'analytical:',analytic
    write(iout,*) 'percent diff:', abs(analytic - sum(intg))/analytic * 100.0
    write(*,*) 'intgral:', intg(:), sum(intg)
    write(*,*) 'analytical:',analytic
    write(*,*) 'percent diff:', abs(analytic - sum(intg))/analytic * 100.0

  end subroutine
!******************************************************************************  

  end module
