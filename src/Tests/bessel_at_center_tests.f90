module bessel_at_center_test
  use commonparams
  use gridparams, only: atom_on_grid
implicit none  
private
public j0_at_center, j1_at_center, rj1_at_center

contains

  subroutine j0_at_center(atm_grid,kappa,rad)
   type(atom_on_grid) :: atm_grid(:)
   real(idp)          :: kappa
   real(idp),allocatable  :: intg(:)
   real(idp)              :: rad
   real(idp)              :: analytic
   real(idp)              :: dist   
   real(idp),parameter    :: eps = 1.d-13
   integer                :: num_cent
   integer                :: ipt, icent, j, tot_gsiz
   integer,allocatable    :: ptcount(:)
  !**************************************************
    print*, 'kappa:',kappa
    num_cent  = size(atm_grid)
    tot_gsiz  = sum(atm_grid(:)%num_pts)
    allocate(intg(num_cent),ptcount(num_cent))


    do icent = 1, num_cent
    intg(icent) = 0.d0
    ptcount(icent) = 0
      do ipt = 1, atm_grid(icent)%num_pts

        dist = dot_product(atm_grid(icent)%xyz(ipt,:),atm_grid(icent)%xyz(ipt,:))
        dist = sqrt(dist)
        
        if(dist <= rad+eps) then  
          ptcount(icent) = ptcount(icent)  + 1
          if(dist >= 1.d-16)then
          intg(icent) = intg(icent)        + &
          sin( kappa * dist)/dist/kappa    * &
            atm_grid(icent)%becke_norm(ipt)* &
            atm_grid(icent)%w_xyz(ipt)
          else
          intg(icent) = intg(icent)        + &
            atm_grid(icent)%becke_norm(ipt)* &
            atm_grid(icent)%w_xyz(ipt)
          endif
        end if



      end do
      print*, 'for center', icent,':'
      print*, ptcount(icent), 'points out of',atm_grid(icent)%num_pts ,'lie inside radius of', rad
    end do

    print*, sum(ptcount), 'total number of points in integration region'

    analytic = 4.d0 * pi *( sin(rad*kappa)-  rad*kappa* cos(rad*kappa))/kappa**3

    write(iout,*) 'integral of sphere with radius=',rad
    write(*,*) 'integral of sphere with radius=',rad
    if(num_cent > 1) then
      write(iout,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
      write(*,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
    end if  
    write(iout,*) 'intgral:', intg(:), sum(intg)
    write(iout,*) 'analytical:',analytic
    write(iout,*) 'percent diff:', abs(analytic - sum(intg))/analytic * 100.0
    write(*,*) 'intgral:', intg(:), sum(intg)
    write(*,*) 'analytical:',analytic
    write(*,*) 'percent diff:', abs(analytic - sum(intg))/analytic * 100.0



 

  end subroutine

  subroutine j1_at_center(atm_grid,kappa,rad)
   type(atom_on_grid) :: atm_grid(:)
   real(idp)          :: kappa
   real(idp)              :: rad
   real(idp),allocatable  :: intg(:), intg2(:)
   real(idp)              :: analytic
   real(idp)              :: dist   
   real(idp),parameter    :: eps = 1.d-13
   integer                :: num_cent
   integer                :: ipt, icent, j, tot_gsiz
   integer,allocatable    :: ptcount(:)
  !**************************************************

    num_cent  = size(atm_grid)
    tot_gsiz  = sum(atm_grid(:)%num_pts)
    allocate(intg(num_cent),ptcount(num_cent), intg2(num_cent))


    do icent = 1, num_cent
    intg(icent) = 0.d0
    intg2(icent) = 0.d0
    ptcount(icent) = 0
      do ipt = 1, atm_grid(icent)%num_pts

        dist = dot_product(atm_grid(icent)%xyz(ipt,:),atm_grid(icent)%xyz(ipt,:))
        dist = sqrt(dist)
        
        if(dist <= rad+eps) then  
          ptcount(icent) = ptcount(icent)  + 1
          !if(dist >= 1.d-20)then
          intg(icent) = intg(icent)        + &
          ! (  (sin(kappa * dist)/dist**2/kappa**2)   - &
          !    (cos(kappa * dist)/dist/kappa)  )   * &
          (sin(kappa * dist)/dist**2/kappa**2) * &
            atm_grid(icent)%becke_norm(ipt)* &
            atm_grid(icent)%w_xyz(ipt)

            intg2(icent) = intg2(icent)        + &
            ! (  (sin(kappa * dist)/dist**2/kappa**2)   - &
            !    (cos(kappa * dist)/dist/kappa)  )   * &
            (cos(kappa * dist)/dist/kappa) * &
              atm_grid(icent)%becke_norm(ipt)* &
              atm_grid(icent)%w_xyz(ipt)  
 
         !endif
        end if



      end do
      print*, 'for center', icent,':'
      print*, ptcount(icent), 'points out of',atm_grid(icent)%num_pts ,'lie inside radius of', rad
    end do

    print*, sum(ptcount), 'total number of points in integration region'

    analytic = 4.d0 * pi *(2.d0- rad*kappa* sin(rad*kappa)- 2.d0* cos(rad*kappa))/kappa**3

    write(iout,*) 'integral of sphere with radius=',rad
    write(*,*) 'integral of sphere with radius=',rad
    if(num_cent > 1) then
      write(iout,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
      write(*,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
    end if  
!    write(iout,*) 'intgral:', intg(:), sum(intg)
    write(iout,*) 'analytical:',analytic
    write(iout,*) 'percent diff:', abs(analytic - sum(intg))/analytic * 100.0
    write(*,*) 'intgral:', intg(:)-intg2(:), sum(intg)-sum(intg2)
    write(*,*) 'analytical:',analytic
    write(*,*) 'percent diff:', abs(analytic - sum(intg)+sum(intg2))/analytic * 100.0
   end subroutine



   subroutine rj1_at_center(atm_grid,kappa,rad)
    type(atom_on_grid) :: atm_grid(:)
    real(idp)          :: kappa
    real(idp)              :: rad
    real(idp),allocatable  :: intg(:), intg2(:)
    real(idp)              :: analytic
    real(idp)              :: dist   
    real(idp),parameter    :: eps = 1.d-13
    integer                :: num_cent
    integer                :: ipt, icent, j, tot_gsiz
    integer,allocatable    :: ptcount(:)
   !**************************************************
 
     num_cent  = size(atm_grid)
     tot_gsiz  = sum(atm_grid(:)%num_pts)
     allocate(intg(num_cent),ptcount(num_cent), intg2(num_cent))
 
 
     do icent = 1, num_cent
     intg(icent) = 0.d0
     intg2(icent) = 0.d0
     ptcount(icent) = 0
       do ipt = 1, atm_grid(icent)%num_pts
 
         dist = dot_product(atm_grid(icent)%xyz(ipt,:),atm_grid(icent)%xyz(ipt,:))
         dist = sqrt(dist)
         
         if(dist <= rad+eps) then  
           ptcount(icent) = ptcount(icent)  + 1
           if(dist >= 1.d-20)then
           intg(icent) = intg(icent)        + &
           ! (  (sin(kappa * dist)/dist**2/kappa**2)   - &
           !    (cos(kappa * dist)/dist/kappa)  )   * &
           (sin(kappa * dist)/dist/kappa**2) * &
             atm_grid(icent)%becke_norm(ipt)* &
             atm_grid(icent)%w_xyz(ipt)
 
             intg2(icent) = intg2(icent)        + &
             ! (  (sin(kappa * dist)/dist**2/kappa**2)   - &
             !    (cos(kappa * dist)/dist/kappa)  )   * &
             (cos(kappa * dist)/kappa) * &
               atm_grid(icent)%becke_norm(ipt)* &
               atm_grid(icent)%w_xyz(ipt)  
  
           endif
         end if
 
 
 
       end do
       print*, 'for center', icent,':'
       print*, ptcount(icent), 'points out of',atm_grid(icent)%num_pts ,'lie inside radius of', rad
     end do
 
     print*, sum(ptcount), 'total number of points in integration region'
 
     analytic = 4.d0 * pi *((3.d0- rad*rad*kappa*kappa)*sin(rad*kappa)- 3.d0*rad*kappa*cos(rad*kappa))/kappa**4
 
     write(iout,*) 'integral of sphere with radius=',rad
     write(*,*) 'integral of sphere with radius=',rad
     if(num_cent > 1) then
       write(iout,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
       write(*,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
     end if  
 !    write(iout,*) 'intgral:', intg(:), sum(intg)
     write(iout,*) 'analytical:',analytic
     write(iout,*) 'percent diff:', abs(analytic - sum(intg))/analytic * 100.0
     write(*,*) 'intgral:', intg(:)-intg2(:), sum(intg)-sum(intg2)
     write(*,*) 'analytical:',analytic
     write(*,*) 'percent diff:', abs(analytic - sum(intg)+sum(intg2))/analytic * 100.0
    end subroutine
 

end module  
