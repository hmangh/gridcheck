module tests
  use commonparams
  use gridparams
  use sphereVolumeTest
  use radialPtsTest
  use bessel_at_center_test
  implicit none

  private

  public  suite_of_tests

  !public  integral_sphere,
  public                   integral_yukawa   &
    ,integral_yukawa_all_atm_centers   &
    ,integral_gaussian_all_atm_centers &
    ,integral_psi1N_psi2O12            &
    ,test_default_gbs, test_gbs_range  &
    ,test_gbsbess_range, test_yukawa_range
  public  overlap_integral_two_gaussians_at_atm_centers &
    ,  lmax_vs_alpha_test, rmax_vs_alpha_test  & 
    , test_ylm_on_grid, test_ylm2_on_grid      &
    , test_hydrogen, test_bspline



contains

  !************************************************************  
  ! Description: Given an input file, determines how many and
  !              what tests to run
  ! Input : string, inp_filename: input file name that includes 
  !              options for what set of tests to run
  ! Output     : null (depends on the option)
  !************************************************************

  subroutine suite_of_tests(inp_filename)
    use cfgio_mod
    !----------------------------------------------
    character(len=*)  , intent(in)  :: inp_filename
    character(len=128), allocatable :: dummy (:)
    character(len=128)              :: dummy1
    character(len=:)  , allocatable :: test_option
    character(len=:)  , allocatable :: weighting_option   

    type(cfg_t) :: conf

    logical     :: found_exp_coef

    integer     :: num_tests
    integer     :: icount

    real(idp)              :: expn
    real(idp), allocatable :: alpha_vals(:)
    !----------------------------------------------

    conf = parse_cfg(inp_filename)

    call conf%get("Tests","num_tests",num_tests)

    allocate(dummy(num_tests))

    call conf%get("Tests","option",dummy)

    call conf%get("Tests","weight_option", dummy1)

    !********************************************
    !check two optional input parameters
    !*******************************************
    ! a) alpha values in exponentials:
    !     EXP(-0.5 \alpha x) where x=r or x=r^2
    !*******************************************
    found_exp_coef = conf%has_key("Tests","exp_coef")

    if (found_exp_coef) then

      call conf%get("Tests","exp_coef",alpha_vals)

    end if

    !********************************************      

    do icount = 1, num_tests

      test_option = trim(dummy(icount))
      print*, test_option!, len(test_option)
      weighting_option = trim(dummy1)

      if(weighting_option == 'becke_luca' .or. &
        weighting_option == 'becke_luca1' .or.&
        weighting_option == 'becke_luca2' .or.&
        weighting_option == 'becke_luca3') then

      call conf%get("Tests", 'fitting_exponent',expn)

      if(found_exp_coef)then

        call select_option(inp_filename,test_option,weighting_option, expn, alpha_vals )

      else

        call select_option(inp_filename,test_option,weighting_option, expn )

      endif  

    else

      if(found_exp_coef)then
        call select_option(inp_filename,test_option,weighting_option,alpha_vals=alpha_vals )
      else
        call select_option(inp_filename,test_option,weighting_option )
      endif

    end if


  end do        



end subroutine


!******************************************************************  
! Description: Given an option, atom on grid type and an optional 
!              exponential power determines which weighting scheme
!               to run and fills: 
!               atm_grid(atom_index)%becke_norm(num_pts)
! Input      :: string, w_opt : determines what set of
!               weighting schemes is to be run
!            :: type(atom_on_grid) atm_grid array : a class that 
!               keeps tabs on contributions of each atom/center
!               to the overall grid. (description is in gridparams)
!            :: real expn : optional power for certain weighting
!                schemes 
!    
! Output     :: null (depends on the option) is reflected in
!               the becke_norm properties of atm_grid
!******************************************************************
subroutine weighting_scheme(w_opt,atm_grid,expn)
  use becke_luca
  use voroni 
  !-----------------------------------------------------------------
  character(len=*) :: w_opt

  type(atom_on_grid) :: atm_grid(:)

  real(idp), optional :: expn
  real(idp)           :: expn_dummy

  integer             :: num_atoms
  !--------------------------------------------------------------- 

  if(.not. present(expn)) expn_dummy = 1.0

  num_atoms = size(atm_grid) - 1

  select case ( w_opt)

  case('becke_luca', 'becke_luca1')

    call form_becke_luca(atm_grid,num_atoms,expn,Luca=1)

  case('becke_luca2')

    call form_becke_luca(atm_grid,num_atoms,expn,Luca=2)

  case('becke_luca3')

    call form_becke_luca(atm_grid,num_atoms,expn,Luca=3)

  case('becke')

    call form_becke_norms(atm_grid)

  case ('becke_nico')

    call voroni_nico(atm_grid)

  case ('becke_lucchese')

    call voroni_lucchese_original(atm_grid)

  case('becke_luca0')

    call form_becke_luca(atm_grid, num_atoms, expn_dummy, Luca=0)

  case ('normal', 'single-centered', 'none')

    !skip, no special weighting is necessary, this just ensures 
    ! Becke weights are all 1.
    call Novoronoi(atm_grid)

  case default

    stop 'No such weighting option is valid or implemented'

  end select

end subroutine  


!************************************************************  
! Description: Given an option, determines which tests to run
! Input      :: string, test_option : determines what set of
!               tests to run
!            :: string inp_file : name of the file with input
!               parameters 
!            :: string w_opt : determines what weighting scheme
!               is to be used
! Output     :: null (depends on the option)
!************************************************************

subroutine select_option(inp_file, test_option, w_opt, expn, alpha_vals)
  use h2_test
  use cfgio_mod
  !---------------------------------------------
  character(len=*), intent(in)   :: inp_file
  character(len=*), intent(in)   :: test_option
  character(len=*), intent(in)   :: w_opt
  character(len=:), allocatable  :: filename_c, filename_a

  integer               :: num_atoms
  integer, allocatable  :: lmax_array(:)
  integer               :: i

  type(atom_on_grid), allocatable :: atm_grid(:)
  type(atom)        , allocatable :: atms(:)
  type(cfg_t)                     :: conf

  real(idp)              :: kappa
  real(idp), optional    :: expn
  real(idp), optional    :: alpha_vals(:)
  real(idp), allocatable :: rmax_array(:)
  real(idp), allocatable :: cent(:)

  logical :: single_cen_offcen_present = .false.
  logical :: alpha_present = .false.
  logical :: kappa_present = .false.
  !---------------------------------------------

  ! Construct the grid according to input file 
  ! or default file parameters
  call read_write_gridparams(inp_file,num_atoms,&
    atm_grid,atms,lmax_array,rmax_array)
  !********************************************************************
  ! Here choose from a list of weight redistribution schemes
  !********************************************************************  

  print*, 'weighting_option:', w_opt

  if(present(expn))then
    call weighting_scheme(w_opt, atm_grid, expn)
  else
    call weighting_scheme(w_opt, atm_grid)
  endif    

  print*, "test_option:",test_option

  if(present(alpha_vals))then

    print*, alpha_vals
    alpha_present = .true.

  end if


  conf = parse_cfg(inp_file)
  if(conf%has_key("Tests", "kappa"))then
    call conf%get("Tests", "kappa",kappa)
    kappa_present = .true.
  endif   

  if(num_atoms == 0 ) then

    if(conf%has_key("center", "single_center_offset_center"))then
      call conf%get("center", "single_center_offset_center",cent)
      if(size(cent) /= 3) stop 'single_center_offset_center key must have 3 dimensions!'
      single_cen_offcen_present = .true.
    endif

  end if        
  !**********************************************************************************
  !This should turn into a routine of its own 
  !       if(w_opt == 'becke_luca'.or. w_opt=='becke_luca1')then
  !         filename_c = 'center_weights.dat'
  !         filename_a = 'atm1_weights.dat'
  !       elseif( w_opt == 'becke_luca2')then
  !         filename_c = 'center_weights1.dat'
  !         filename_a = 'atm1_weights1.dat'
  !       end if  
  !
  !       if(num_atoms > 0) then
  !        open(unit=15, file=filename_c)
  !          do i = 1, atm_grid(1)%num_pts        
  !              write(15,'(4Es15.4)') atm_grid(1)%xyz(i,1),&
  !                                    atm_grid(1)%xyz(i,2),&
  !                                    atm_grid(1)%xyz(i,3),&
  !                                    atm_grid(1)%becke_norm(i)!* &
  !                                    !atm_grid(1)%w_xyz(i)
  !          end do              
  !        close(15)                              
  !        open(unit=15, file=filename_a)
  !          do i = 1, atm_grid(2)%num_pts        
  !              write(15,'(4Es15.4)') atm_grid(2)%xyz(i,1),&
  !                                    atm_grid(2)%xyz(i,2),&
  !                                    atm_grid(2)%xyz(i,3),&
  !                                    atm_grid(2)%becke_norm(i)!* &
  !                                    !atm_grid(2)%w_xyz(i)
  !          end do
  !         close(15)
  !        end if       
  !*********************************************************************************** 
  ! Select which test to run
  select case (test_option)

  case ('integral_sphere')

!    call integral_sphere(atm_grid, rad=   &
!      atm_grid(1)%max_radius)
    call integral_sphere(atm_grid, rad=16.d0)


  case ('yukawa_alpha_range')

    if(single_cen_offcen_present) then   

      if(present(alpha_vals))then

        call test_yukawa_range(atm_grid,num_atoms,alpha_vals=alpha_vals,&
          single_cent_offcen_coord=cent)

      else

        call test_yukawa_range(atm_grid,num_atoms,single_cent_offcen_coord=cent)

      end if

    else

      if(present(alpha_vals))then

        call test_yukawa_range(atm_grid,num_atoms,alpha_vals=alpha_vals)

      else

        call test_yukawa_range(atm_grid,num_atoms)

      end if


    end if        

  case ('gaussian_basis_set_range')

    if(single_cen_offcen_present .and. alpha_present) then

      call test_gbs_range(atm_grid,num_atoms,alpha_vals=alpha_vals,&
        single_cent_offcen_coord=cent)

    elseif(single_cen_offcen_present)then

      call test_gbs_range(atm_grid,num_atoms,single_cent_offcen_coord=cent)

    elseif(alpha_present)then

      call test_gbs_range(atm_grid,num_atoms,alpha_vals=alpha_vals)

    else

      call test_gbs_range(atm_grid,num_atoms)

    endif    

  case ('bessel0_at_cent')
    !print*, alpha_vals, kappa; stop

    call j0_at_center(atm_grid, kappa,0.8d0*atm_grid(1)%max_radius)

  case ('bessel1_at_cent')
    !print*, alpha_vals, kappa; stop

    call j1_at_center(atm_grid, kappa,atm_grid(1)%max_radius)

  case ('r*bessel1_at_cent')
    !print*, alpha_vals, kappa; stop

    call rj1_at_center(atm_grid, kappa,0.8d0*atm_grid(1)%max_radius)  

  case ('besse1_at_cent')
    !print*, alpha_vals, kappa; stop

    call integral_bessel_at_center(atm_grid, [0.d0,0.d0,0.d0], kappa)

  case ('sin_at_cent')
    !print*, alpha_vals, kappa; stop

    call integral_sin_at_center(atm_grid, [0.d0,0.d0,0.d0], kappa)

  case ('psi1_psi2_overlap')
    !print*, alpha_vals, kappa; stop

    call integral_psi1N_psi2O12(atm_grid, num_atoms,alpha_vals, kappa)

  case ('gaussian+bessel_range')

    if(kappa_present .and. alpha_present .and. single_cen_offcen_present) then

      call test_gbsbess_range(atm_grid,num_atoms, alpha_vals= alpha_vals, kappa_val=kappa, &
        single_cent_offcen_coord=cent)

    elseif(kappa_present .and. alpha_present)then                   

      call test_gbsbess_range(atm_grid,num_atoms, alpha_vals= alpha_vals, kappa_val=kappa)

    elseif(kappa_present .and. single_cen_offcen_present)then

      call test_gbsbess_range(atm_grid,num_atoms, kappa_val=kappa, &
        single_cent_offcen_coord=cent)

    elseif(alpha_present .and. single_cen_offcen_present) then

      call test_gbsbess_range(atm_grid,num_atoms, alpha_vals=alpha_vals, &
        single_cent_offcen_coord=cent)

    else

      call test_gbsbess_range(atm_grid,num_atoms)

    end if

  case ( 'default_gaussian_basis_set')

    call test_default_gbs(atm_grid)

  case ( 'change_radius_center' )

    call change_radius('c',atm_grid, atms, w_opt) 

  case ( 'change_radius_atoms' )

    call change_radius('a',atm_grid, atms, w_opt)

  case ( 'change_lmax_center' )

    call change_lmax('c',atm_grid, atms, w_opt) 

  case ( 'change_lmax_atoms' )

    call change_lmax('a',atm_grid, atms, w_opt)

  case (  'H2_direct_exchange_test' )

    !               call test_hydrogen(atm_grid, atms)   
    call test_h2(atm_grid, atms)

  case ( 'test_ylm_on_grid' )

    call test_ylm_on_grid(atm_grid(1), 2*atm_grid(1)%lmax)

  case ( 'test_ylm2_on_grid' )

    call test_ylm2_on_grid(atm_grid(1), atm_grid(1)%lmax)

  case default

    stop 'this test is invalid or not implemented'

  end select

end subroutine  


!*********************************************************************  
! Description:: This procedure varies the radii of either the atomic
!               grids or the central grid. It assumes uniform
!               sizes for the atoms and should be used cautiously.
!               in effect it destroys the input setup
!
! Input      :: string, opt : determines whether to vary the central
!                             or the atomic radii
!
!            :: defined type atm_grid : 1d-array of type atom_on_grid
!                 defined in grid param, containing information about
!                 the atom/center contributions to the overall grid
!
!            :: defined type atms : 1d-array of type atom as defined in 
!                                   gridparams
!            :: string w_opt : weighting option
!               
! Output     :: null (depends on the option) changes properties of atms
!               and atm_grid                 
!*********************************************************************
subroutine change_radius(opt, atm_grid, atms, w_opt)
  !-------------------------------------------------------------
  real(idp), allocatable :: rmax_array(:)
  real(idp), parameter   :: max_R = 5.d0 
  real(idp), parameter   :: min_r = 1.d0
  real(idp), parameter   :: rad_increment = 1.d0
  real(idp)              :: rad
  real(idp)              :: expn

  type(atom_on_grid), allocatable :: atm_grid(:)
  type(atom)        , allocatable :: atms(:)

  character(len=1)  , intent(in)  :: opt
  character(len=*)  , intent(in)  :: w_opt

  integer  :: iexp
  integer  :: num_atoms
  integer, parameter :: iexp_min=1, iexp_max=5
  !-------------------------------------------------------------

  rad = min_r

  num_atoms = size(atm_grid) - 1

  allocate(rmax_array(size(atm_grid)))

  rmax_array(1) = atm_grid(1)%max_radius
  rmax_array(2:num_atoms) = atm_grid(2:num_atoms)%max_radius

  if (opt == 'a' .and. num_atoms /=0 )then

    do iexp = iexp_min, iexp_max
      print*, '*******','exp_power:',iexp,'*******'
      expn = real(iexp)
      do while (rad <= max_R)
        rmax_array(2:num_atoms + 1) = rad
        !                print*, 'before'
        !                call test_gbs_range(atm_grid)
        call change_radial_grid_params(atm_grid, atms, rmax_array)
        !                print*, 'after'

        !     here are two variables: weighting scheme and the particular test      
        call weighting_scheme(w_opt,atm_grid,expn)
        !      !perform the test on a fixed set of gaussians
        call rmax_vs_alpha_test(atm_grid,rad,iexp,max_R)
        rad = rad + rad_increment
      end do
      rad = min_r
    end do    

  elseif(opt == 'c' .or. num_atoms == 0 ) then

    print*, '*******','exp_power:',iexp,'*******'
    do iexp = iexp_min, iexp_max
      expn = real(iexp)
      do while (rad <= max_R)
        rmax_array(1) = rad
        call change_radial_grid_params(atm_grid, atms, rmax_array)
        call weighting_scheme(w_opt,atm_grid,expn)
        call rmax_vs_alpha_test(atm_grid,rad,iexp,max_R)
        rad = rad + rad_increment
      end do
      rad = min_r    
    end do

  else

    stop 'error options or number of atoms error in change_radius'

  endif

end subroutine  

!*************************************************************
! Description: This routine tests a default set of gaussian  
!              functions on the multi-centred atomic grid for
!              a certain atomic size and exponent of a fitting 
!              function
!
! @Input     : class atm_grid that contains information about
!              number of atomic grids and all grid parameters
!              
!              float rmax is the radial size of the atomic
!              grid. It is assumed that they are of the same 
!              size 
!        
!            : integer iexp is the exponent of the refitted 
!              function       
!              
!            : float upper_r is an optinal entry that would 
!              signal rmax ceiling and closes the output file
!
! Output     : an output ASCII file that is called 
!               Alpha_rmax_exponent_lmax if becke_luca fitting
!               is used name could change if simple becke scheme
!               is used. The output includes 
!               rmax  error of integrating the gaussian function
!               with parameter alpha  
!**************************************************************** 
subroutine rmax_vs_alpha_test(atm_grid,rmax,iexp,upper_r)
  !---------------------------------------------------------
  type(atom_on_grid) :: atm_grid(:)

  integer :: num_atms
  integer :: i, j, iter
  integer :: iexp
  integer :: lmax
  integer :: num_test_func

  real(idp) :: cent(3)
  real(idp) :: default_alphas(5)
  real(idp), allocatable:: err(:,:)
  real(idp), allocatable:: alpha_array(:,:)
  real(idp):: cent_array(3,3)
  real(idp)             :: alpha
  real(idp), intent(in) :: rmax
  real(idp), optional   :: upper_r

  character(len=5) :: expn_str
  character(len=5) :: lmax_str   

  logical itsopen 
  !----------------------------------------------------------

  print*, 'radius:',rmax    
  num_atms = size(atm_grid) - 1

  num_test_func = 5 !default value
  default_alphas = [1.d+3,1.d+2,1.d+1,1.0d0,1.d-1]

  if(num_atms == 0) then
    allocate(alpha_array(num_test_func,size(cent_array,2)))
  else
    allocate(alpha_array(num_test_func,num_atms))
  end if   

  do i = 1, num_atms
    alpha_array(:,i) = default_alphas(:)
  end do   

  !    alpha_array = reshape(&
  !                    [0.9910616896D+02,0.1805231239D+02,0.4885660238D+01, &
  !                     0.3780455879D+01,0.8784966449D+00,0.2857143744D+00, &
  !                     [0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
  !                     0.5033151319D+01,0.1169596125D+01,0.3803889600D+00, &
  !                     0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
  !                     0.5033151319D+01,0.1169596125D+01,0.3803889600D+00, &
  !                     0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
  !                     0.5033151319D+01,0.1169596125D+01,0.3803889600D+00],&
  !                     shape(alpha_array))
  !    alpha_array = reshape([1.d+3,1.d+2,1.d+1,1.0d0,1.d-1, &!0.1805231239D+02,  &
  !                           1.d+3,1.d+2,1.d+1,1.0d0,1.d-1, &
  !                           1.d+3,1.d+2,1.d+1,1.0d0,1.d-1],&! [0.9910616896D+02,0.1805231239D+02,  &

  ! 0.9910616896D+03,0.1805231239D+02,  &! 0.1307093214D+03,0.2380886605D+02,  &
  !  0.1307093214D+03,0.2380886605D+02], &
  !                     shape(alpha_array))

  cent_array = reshape(&![0.0D+0, 0.0D+0,  2.0D+0 ,&
    ! 0.0D+0, 0.0D+0, -2.0D+0 ,&
  ! 0.0D+0, 0.0D+0,  5.0D+0],&
  [0.0D+0,0.0D+0, 0.690883871163182D+0, &     
    0.0D+0, 2.14030380875115D+0,-0.302167207327661D+0, &     
    !0.0D+0, 0.D+0, 0.0D0,&!0.690883871163182D+0,&     
  0.0D+0,-2.14030380875115D+0,-0.302167207327661D+0],&
    ! 0.0D+0,0.0D+0,5.0D+0],&
  shape(cent_array))

  allocate(err,source=alpha_array)                

  lmax = atm_grid(2)%lmax
  !     print'(F3.1)',rsize; stop

  inquire(unit=10, opened=itsopen) 
  if ( itsopen ) then
    !write(*,*) 'Its open already'
  else
    write(expn_str,'("exp_",I0)') iexp 
    write(lmax_str,'("L_",I0)') lmax
    !       print*, expn_str, size_str; stop
    !       open(unit=10, file='Alpha_rmax_'//expn_str//trim(lmax_str)//'.txt')
    open(unit=10, file='Alpha_rmax_nico'//trim(lmax_str)//'.txt')
    write(10,'(A8,12(",",F8.3))') 'rmax',(alpha_array(:,j),j=1,num_atms)
  end if

  !     write(10,*) 'alpha', ',', 'percent error'
  !    print*, size(alpha_array,1)
  iter = size(alpha_array,1)
  print*, 'number of atoms:',num_atms
  if (num_atms == 0)then


    !       do i = 1, size(alpha_array,2)
    !         cent = cent_array(:,i)
    !         do j = 1, iter
    !            call integral_gaussian(atm_grid, cent, alpha_array(j,i),err(j,i))
    !         end do   
    !       end do

    call test_gbs_range(atm_grid,num_atms,err,default_alphas)
    write(10, '(F8.3,12(",",Es12.4))')rmax,(err(:,j),j=1,size(cent_array,2))

  else

    !       do i = 1 , num_atms
    !          cent = atm_grid(i+1)%center![0.d0, 0.d0, 2.d0]
    !          do j = 1, iter
    !            call integral_gaussian(atm_grid, cent, alpha_array(j,i),err(j,i))
    !          end do
    !       end do
    call test_gbs_range(atm_grid,num_atms,err,default_alphas)
    write(10, '(F8.3,12(",",Es12.4))')rmax,(err(:,j),j=1,num_atms)

  end if  
  if(present(upper_r))then
    if (rmax == upper_r)then
      close(10)
    endif
  endif  

end subroutine

!****************************************************
!
!****************************************************
subroutine change_lmax(opt, atm_grid, atms, w_opt)
  use lebedev_quadrature
  !--------------------------------------------------- 
  type(lebedev)                   :: lb, lbm
  type(atom_on_grid), allocatable :: atm_grid(:)
  type(atom)        , allocatable :: atms(:)

  real(idp)              :: expn

  character(len=1)  , intent(in)  :: opt
  character(len=*)  , intent(in)  :: w_opt

  integer              :: l_rule
  integer              :: upper_l
  integer              :: iexp
  integer              :: num_atoms
  integer, parameter   :: max_rule = 47
  integer, parameter   :: iexp_min=5, iexp_max=5
  integer, parameter   :: min_l_rule = 3
  integer, allocatable :: lmax_array(:)
  !---------------------------------------------------

  num_atoms = size(atm_grid) - 1
  print*, 'num_atoms:', num_atoms    
  allocate(lmax_array(size(atm_grid))) 

  lmax_array(:) = atm_grid(:)%lmax
  print*, 'lmax_array:', lmax_array
  call lbm%get(max_rule)
  print*, 'max_rule:', max_rule
  upper_l = lbm%precision
  print*, 'upper_l:', upper_l
  l_rule = min_l_rule
  !call lb%get(l_rule)
  !print*, 'starting l:', lb%precision

  if (opt == 'a') then

    print*,'atoms lmax is changing:'

    do iexp = iexp_min, iexp_max

      print*, '*******','exp_power:',iexp,'*******'

      expn = real(iexp)

      do while( l_rule <= max_rule )

        call lb%get(l_rule)

        lmax_array(2:num_atoms + 1) = lb%precision

        print*, 'rule:', l_rule, 'lmax:',lmax_array(2)

        call change_angular_grid_params(atm_grid, atms, lmax_array, num_atoms)

        l_rule = lb%rule + 1

        call weighting_scheme(w_opt,atm_grid,expn)

        call lmax_vs_alpha_test(atm_grid,num_atoms,lmax_array(2),iexp,upper_l)


      end do

      l_rule = min_l_rule

    end do

  elseif (opt == 'c')then

    do iexp = iexp_min, iexp_max
      print*, '*******','exp_coeff:',iexp,'*******'

      expn = real(iexp)

      do while( l_rule <= max_rule )

        call lb%get(l_rule)

        lmax_array(1) = lb%precision

        !print*, 'rule:', l_rule, 'lmax:',lmax_array(1)
        call change_angular_grid_params(atm_grid, atms, lmax_array, num_atoms)

        call weighting_scheme(w_opt,atm_grid,expn)

        call lmax_vs_alpha_test(atm_grid,num_atoms,lmax_array(1),iexp,upper_l)

        l_rule = lb%rule + 1

      end do

      l_rule = min_l_rule

    end do

  else

    stop 'wrong opt for change_lmax (argument 1)'

  end if  

end subroutine  

!*************************************************************
! Description: This routine tests a default set of gaussian  
!              functions on the multi-centred atomic grid for
!              various angular grid sizes and exponents of a 
!              fitting function
!
! @Input     : defined type, dim(# of grids):: atm_grid that
!              contains information about number of atomic 
!              grids and all grid parameters
!              
!              integer :: num_atms : number of atoms
!                                    
!            : real :: lmax: determines the angular size of the
!              atomic grid. It is assumed that they are of the
!              same size 
!        
!            : integer iexp is the exponent of the refitted 
!              function       
!              
!            : real upper_l is an optinal entry that would 
!              signal lmax ceiling and closes the output file
!
! Output     : an output ASCII file that is called 
!               Alpha_lmax_exponent_lmax if becke_luca fitting
!               is used name could change if simple becke scheme
!               is used (exponent drops). The output includes 
!               rmax  error of integrating the gaussian function
!               with parameter alpha  
!**************************************************************** 


subroutine lmax_vs_alpha_test(atm_grid,num_atms,lmax,iexp,upper_l)
  !----------------------------------------------------------------------
  type(atom_on_grid)  :: atm_grid(:)
  integer             :: num_atms
  integer             :: num_test_func
  integer             :: i, j, iter
  integer, intent(in) :: lmax
  integer, optional   :: iexp
  integer, optional   :: upper_l

  real(idp), allocatable :: err(:,:)
  real(idp), allocatable :: alpha_array(:,:)
  real(idp), allocatable :: default_alphas(:)
  real(idp)              :: cent(3)
  real(idp)              :: cent_array(3,3)
  real(idp)              :: alpha
  real(idp)              :: rsize

  character(len=15) :: expn_str
  character(len=15) :: size_str   

  logical :: itsopen, has_center
  !---------------------------------------------------------------------- 


  num_test_func = 6 !default value
  allocate(default_alphas(num_test_func))    
  default_alphas = [1.d+3,1.d+2,1.d+1,1.0d0,1.d-1,1.d-2]

  if(num_atms == 0) then
    allocate(alpha_array(num_test_func,size(cent_array,2)))
  else
    allocate(alpha_array(num_test_func,num_atms))
  end if   

  do i = 1, num_atms
    alpha_array(:,i) = default_alphas(:)
  end do   
  !    alpha_array = reshape(&
  !                    [0.9910616896D+02,0.1805231239D+02,0.4885660238D+01, &
  !                     0.3780455879D+01,0.8784966449D+00,0.2857143744D+00, &
  !                     [0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
  !                     0.5033151319D+01,0.1169596125D+01,0.3803889600D+00, &
  !                     0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
  !                     0.5033151319D+01,0.1169596125D+01,0.3803889600D+00, &
  !                     0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
  !                     0.5033151319D+01,0.1169596125D+01,0.3803889600D+00],&
  !                     shape(alpha_array))
  !    alpha_array = reshape([1.d+3,1.d+2,1.d+1,1.0d0,1.d-1, &!0.1805231239D+02,  &
  !                           1.d+3,1.d+2,1.d+1,1.0d0,1.d-1, &
  !                           1.d+3,1.d+2,1.d+1,1.0d0,1.d-1],&! [0.9910616896D+02,0.1805231239D+02,  &

  ! 0.9910616896D+03,0.1805231239D+02,  &! 0.1307093214D+03,0.2380886605D+02,  &
  !  0.1307093214D+03,0.2380886605D+02], &
  !                     shape(alpha_array))
  cent_array = reshape(&![0.0D+0, 0.0D+0,  2.0D+0 ,&
    ! 0.0D+0, 0.0D+0, -2.0D+0 ,&
  ! 0.0D+0, 0.0D+0,  5.0D+0],&
  [0.0D+0,0.0D+0, 0.690883871163182D+0, &     
    0.0D+0, 2.14030380875115D+0,-0.302167207327661D+0, &     
    !0.0D+0, 0.D+0, 0.0D0,&!0.690883871163182D+0,&     
  0.0D+0,-2.14030380875115D+0,-0.302167207327661D+0],&
    ! 0.0D+0,0.0D+0,5.0D+0],&
  shape(cent_array))

  allocate(err,source=alpha_array)                
  if(num_atms == size(atm_grid) - 1) then
    has_center = .true.
  elseif(num_atms == size(atm_grid)) then
    has_center = .false.
  else
    print*, 'num_atms:',num_atms, 'size atm_grid:', size(atm_grid)
    stop 'size of atm_grid does not match num_atms in lmax_vs_alpha test.f90'  
  endif  

  rsize = atm_grid(2)%max_radius
  !     print'(F4.1)',rsize; stop

  inquire(unit=10, opened=itsopen) 
  if ( itsopen ) then
    !write(*,*) 'Its open already'
  else
    write(size_str,'("R_",F4.1)') rsize
    if(present(iexp))then
      write(expn_str,'("exp_",I1)') iexp 
      !       print*, expn_str, size_str; stop
      open(unit=10, file='Alpha_lmax_'//trim(expn_str)//trim(size_str)//'.txt')
      write(10,'(A4,10(",",F8.3))') 'lmax',(alpha_array(:,j),j=1,2)!num_atms)
    else
      ! print*, size_str; stop
      open(unit=10, file='Alpha_lmax_'//trim(size_str)//'.txt')
      write(10,'(A4,10(",",F8.3))') 'lmax',(alpha_array(:,j),j=1,2)!num_atms)
    endif 
  end if

  !     write(10,*) 'alpha', ',', 'percent error'
  !    print*, size(alpha_array,1)
  iter = size(alpha_array,1)
  if (num_atms == 0 .and. has_center)then

    !       do i = 1, 2!size(alpha_array,2)
    !         cent = cent_array(:,i)
    !         do j = 1, iter
    !            call integral_gaussian(atm_grid, cent, alpha_array(j,i),err(j,i))
    !         end do   
    !       end do

    call test_gbs_range(atm_grid,num_atms, err, default_alphas)
    write(10, '(I3,12(",",Es12.4))')lmax,(err(:,j),j=1,2)!size(alpha_array,2))

  else

    if(has_center)then
      !        do i = 1 , num_atms
      !          cent = atm_grid(i+1)%center![0.d0, 0.d0, 2.d0]
      !          do j = 1, iter
      !            call integral_gaussian(atm_grid, cent, alpha_array(j,i),err(j,i))
      !          end do
      !        end do
      !         print*,  allocated(err) 
      call test_gbs_range(atm_grid,num_atms ,err,default_alphas)


    else

      do i = 1 , num_atms
        cent = atm_grid(i)%center![0.d0, 0.d0, 2.d0]
        do j = 1, iter
          call integral_gaussian(atm_grid, cent, alpha_array(j,i),err(j,i))
        end do
      end do
      !          call test_gbs_range(atm_grid, err)

    endif  

    ! write(10, '(I3,12(",",Es12.4))')lmax,(err(:,j),j=1,num_atms)
    write(10, '(I3,10(",",Es12.4))')lmax,(err(:,j),j=1,2)!num_atms)

  end if  
  if(present(upper_l))then
    if (lmax == upper_l)then
      close(10)
    endif
  endif  

end subroutine  

!!************************************************************
!!Description : checks the overall integral of the grid to
!!              see how far off from the actual sphere volume
!!              it might be after redistribution of weights
!!              with various schemes, such as becke
!!@input      : defined type:: atm_grid: (description of type 
!!                             is in gridparams) 1d-array
!!            : real :: rad : radius of overall grid (i.e. 
!!                            largest radius
!!************************************************************
!  subroutine integral_sphere(atm_grid, rad)
!   !-------------------------------------------------------- 
!    type(atom_on_grid)     :: atm_grid(:)
!    
!    real(idp),allocatable  :: intg(:)
!    real(idp)              :: rad
!    real(idp)              :: analytic
!    
!    integer                :: num_cent
!    integer                :: ipt, icent, j, tot_gsiz
!   !------------------------------------------------------- 
!   ! if(.not. allocated(atm_grid))then
!   !    call 
!   ! end if   
!    num_cent  = size(atm_grid)
!    tot_gsiz  = sum(atm_grid(:)%num_pts)
!    allocate(intg(num_cent))
!    intg = 0.d0
!
!    ! write(iout,*) 'check if w_n adds to 1'
!    ! do icent = 1, num_cent
!
!    !   do ipt = 1, atm_grid(icent)%num_pts
!
!    !     write(iout,*) sum(atm_grid(icent)%becke_norm(ipt))
!
!    !   end do
!    ! end do
!
!    do icent = 1, num_cent
!      do ipt = 1, atm_grid(icent)%num_pts
!
!
!        if(atm_grid(icent)%rthetaphi(ipt,1) <= rad) then
!          intg(icent) = intg(icent) + &
!            atm_grid(icent)%becke_norm(ipt)*&
!            atm_grid(icent)%w_xyz(ipt)
!        end if
!
!
!
!      end do
!    end do
!    analytic = 4.d0 * pi * rad **3/3.d0
!
!    write(iout,*) 'integral of sphere with radius=',rad
!    if(num_cent > 1) then
!      write(iout,*) 'atomic spheres of radius =', atm_grid(2)%max_radius
!    end if  
!    write(iout,*) 'intgral:', intg(:), sum(intg)
!    write(iout,*) 'analytical:',analytic
!    write(iout,*) 'percent diff:', abs(analytic - sum(intg))/analytic * 100.0
!
!  end subroutine
!
!**************************************************
!Description :  Given alpha and center finds 
!               integral of a Yukawa function at 
!               the given center and on the grid
!Input       :  defined type :: atm_grid 1d-array
!            :  real :: cent: center of function
!                       in cartesian coordinates,
!                        1-darray
!            :  real :: alpha : exponent coeff. of 
!                               the Yukawa
!**************************************************
subroutine integral_yukawa(atm_grid, cent, alpha, error)

  type(atom_on_grid)     :: atm_grid(:)
  ! real(idp), allocatable :: yukawa(:,:)
  real(idp), allocatable :: intg(:)
  real(idp), optional    :: error
  real(idp)              :: analytic
  real(idp)              :: cent(3)
  real(idp)              :: alpha
  real(idp)              :: rmax
  real(idp)              :: err

  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j

  write(iout,*)'*********************************'
  write(iout,*)' Yukawa test:',&
    ' do integral of a Yukawa at a certain center on the grid'
  write(iout,*) "Yukawa's center:", cent
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  write(iout,*)'*********************************'
  !*********************************
  ! allocate(yukawa(size_grid,3))
  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts

      intg(icent) = intg(icent) + &
        yuk(atm_grid(icent)%xyz(ipt,:),&
        atm_grid(icent)%w_xyz(ipt),&
        atm_grid(icent)%becke_norm(ipt),alpha,cent)
      !write(iout,*)ipt, atm_grid(icent)%xyz(ipt,:)
      !write(iout,*)   atm_grid(icent)%w_xyz(ipt)
      !write(iout,*)   atm_grid(icent)%becke_norm(ipt)
    end do
  end do

  analytic =  4.d0 * pi* 1.d0/alpha**2 &
    -exp(-alpha*rmax)*(1.d0+rmax*alpha)/alpha**2

  err=abs( analytic- sum(intg) )/analytic * 100.d0
  if(present(error)) error = err

  write(iout,*)'result:', intg(:), sum(intg)
  write(iout,*)'analytical:',analytic
  write(iout,*) 'percent diff:', err
  print*, alpha, err

end subroutine
!******************************************************************************
!Description : given parameters of a Yukawa function it places the functuon
!              on each of the present atomic centers and tests the accuracy
!              of grid integration 
!Input       : defined type, dimension(number of grids):: atm_grid : 
!                                         decribed in gridparams module
!            
!            : real, alpha :: the coeff. of the Yukawa's exponential 
!output      : real, accuracy of the integration in %diff wrt analytical answer
!*******************************************************************************
subroutine integral_yukawa_all_atm_centers(atm_grid, alpha)

  type(atom_on_grid)     :: atm_grid(:)
  real(idp), allocatable :: yukawa(:,:)
  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic
  real(idp)              :: cent(3)
  real(idp)              :: alpha
  real(idp)              :: rmax
  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j, ycent

  write(iout,*)'*********************************'
  write(iout,*)' Yukawa test:',&
    ' do integral of Yukawa functions at all atomic centers on the grid'
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  write(iout,*)'*********************************'
  !*********************************
  allocate(yukawa(size_grid,3))
  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts

      do ycent = 2, num_cent

        cent = atm_grid(ycent)%center

        !! write(iout,*) "Yukawa's center:", cent
        intg(icent) = intg(icent) +     &
          yuk(atm_grid(icent)%xyz(ipt,:),&
          atm_grid(icent)%w_xyz(ipt)    ,&
          atm_grid(icent)%becke_norm(ipt),alpha,cent)
        !write(iout,*)ipt, atm_grid(icent)%xyz(ipt,:)
        !write(iout,*)   atm_grid(icent)%w_xyz(ipt)
        !write(iout,*)   atm_grid(icent)%becke_norm(ipt)

      end do
    end do
  end do

  analytic = real(num_cent-1)*4.d0 * pi* 1.d0/alpha**2 &
    -exp(-alpha*rmax)*(1.d0+rmax*alpha)/alpha**2
  write(iout,*) "contribution of each grid:"

  write(iout,*) "center grid: ", intg(1)
  do icent = 2, num_cent
    write(iout,'(A5,I0,A2,f20.10)') "atom_",icent, ": ", intg(icent)
  end do

  write(iout,*)'final numerical result:', sum(intg)
  write(iout,*)'analytical:',analytic
  write(iout,*) 'percent diff:', abs( analytic- sum(intg) )/analytic * 100.d0

end subroutine

!*******************************************************************************  
!Description : given parameters of a Yukawa function and a point and its weight
!              parameters on a numerical grid, will evalute the function and
!              multiply it by the weight at that point
!Input       : real, dimension(3) :: xyz: the Cartesian coordinates of one point
!                                         on the grid
!            : real :: w : weight assigned to the grid point 
!            : real :: wb: Becke weight or any modification of the weight, will be 
!                          multiplied into the grid weight, w.
!            : real :: alpha : coeff. of the Yukawa function
!            : real, dimension(3) :: cent : the center of the Yukawa function in 
!                                    Cartesian coordinates
!*********************************************************************************
function yuk(xyz,w,bw,alpha,cent) result(yk)

  real(idp) :: xyz(:),w, bw,alpha
  real(idp) :: yk, r
  real(idp) :: cent(3)

  r = sqrt(dot_product(xyz-cent,xyz-cent))
  if(r /= 0.d0)then
    yk = exp(-alpha * r)/r * w * bw
  else
    !   yk = -alpha * w * bw
    stop 'a grid point is exactly at one center!'
  end if
end function

!********************************************************************************
! Description: Given a Gaussian function, it calculates the numerical integral
!              of the function on a grid and prints the %diff from analytical 
!              answer
!Input       : defined type, dimension(number of grids) :: atm_grid, see 
!                                                 description in gridparams module
!            : real, dimension(3) ::cent, the Cartesian coordinate of the center
!                                         of the Gaussian function.
!            : real :: alpha: the coeff. of the exponential of the Gaussian func.
!output      : real :: err : %diff. of the numerical integration with the analytical
!                            answer.
!************************************************************************************
subroutine integral_gaussian(atm_grid, cent, alpha,err)
  !---------------------------------------------
  type(atom_on_grid)     :: atm_grid(:)

  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic
  real(idp)              :: cent(3)
  real(idp)              :: alpha
  real(idp)              :: rmax, err

  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j
  !--------------------------------------------
  write(iout,*)'*********************************'
  write(iout,*)' Gaussian test:',&
    ' do integral of a Gaussian at a certain center on the grid'
  write(iout,*) "Gaussian's center:", cent
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  write(iout,*) "alpha=", alpha
  write(iout,*)'*********************************'
  !*********************************
  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts

      intg(icent) = intg(icent)    +           &
        gaussian(atm_grid(icent)%xyz(ipt,:)   ,&
        atm_grid(icent)%w_xyz(ipt)            ,&
        atm_grid(icent)%becke_norm(ipt),alpha,cent)
    end do
  end do

  analytic = 1.d0
  err = abs( analytic- sum(intg) )/analytic * 100.d0
  write(iout,*)'result:', intg(:), sum(intg)
  write(iout,*)'Normed analytical:',analytic
  write(iout,*) 'percent diff:',err 

  print*, alpha, err

end subroutine

subroutine integral_bessel_at_center(atm_grid, cent, kappa)

  type(atom_on_grid)     :: atm_grid(:)

  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic
  real(idp)              :: cent(3), xyz(3)
  real(idp)              :: alpha
  real(idp)              :: rmax, err, r
  real(idp)              :: kappa, ans
  real(idp), parameter   :: eps = 1.d-13
  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j
  !--------------------------------------------
  write(iout,*)'*********************************'
  write(iout,*)' Bessel test:',&
    ' do integral of a Spherical Bessel on the grid'
  write(iout,*) "Bessel's center:", cent
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  write(iout,*) "alpha=", alpha
  write(iout,*)'*********************************'
  !*********************************
  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts
      !
      !        do ycent = loc, num_cent
      !
      !          cent = atm_grid(ycent)%center
      !           
      xyz(:) = atm_grid(icent)%xyz(ipt,:)
      !if(distance(xyz,[0.d0,0.d0,0.d0]) <= 50.d0+eps) then

      !           print*, xyz, cent(:,2), cent(:,3)
      !           print*,& 
      !           psi1(xyz, alpha_atm(1), kappa, cent(:,1)), &
      !           (psi2(xyz, alpha_atm(2), kappa, cent(:,2)) -  psi2(xyz, alpha_atm(3), kappa, cent(:,3))) , &
      !            atm_grid(icent)%w_xyz(ipt),atm_grid(icent)%becke_norm(ipt); stop

      r = sqrt(dot_product(xyz-cent,xyz-cent))
      !      if(r >= 1.d-16) then
      intg(icent) = intg(icent) +     &
        bessel_jn(1,kappa*r)/r   *     & 
        !            sin(kappa*r)             *     & 
      atm_grid(icent)%w_xyz(ipt)*atm_grid(icent)%becke_norm(ipt)
      !      else 
      !        intg(icent) = intg(icent) +     &
      !          0.5d0* kappa              *     & 
      !            sin(kappa*r)             *     & 
      !        atm_grid(icent)%w_xyz(ipt)*atm_grid(icent)%becke_norm(ipt)
      !      endif  
      !           atm_grid(icent)%
      !        !write(iout,*)ipt, atm_grid(icent)%xyz(ipt,:)
      !        !write(iout,*)   atm_grid(icent)%w_xyz(ipt)
      !        !write(iout,*)   atm_grid(icent)%becke_norm(ipt)
      !endif
      !       end do
    end do
  end do

  ans = sum(intg)

  !! This is mathematica's analytic answer: rmax*π^2 [ H_0(2*rmax) J_1(2*rmax) -  H_1(2*rmax) J_0(2*rmax) ]
  !! , where H_n(x) is the Struve function and J_n(x) is the bessel function. 
  analytic = -3.3801101384608d0!3.0724422249011d0    
  !    analytic =4.d0* pi*((2.d0-kappa**2 * rmax**2)*cos(kappa*rmax)+2.d0*kappa*rmax*sin(kappa*rmax)-2.d0)/kappa**3 
  err = abs( analytic- sum(intg) )/abs(analytic) * 100.d0
  write(iout,*)'result:', intg(:), ans
  !write(iout,*)'Normed analytical:',analytic
  !write(iout,*) 'percent diff:',err 
  print'("Num ans:",F17.12,"; Anlyt. ans:",F17.12,"; %DiffError:",Es7.0)', ans,analytic, err
  print'("abs. diff.:",F17.12)', abs(analytic - sum(intg))

end subroutine


subroutine integral_sin_at_center(atm_grid, cent, kappa)

  type(atom_on_grid)     :: atm_grid(:)

  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic
  real(idp)              :: cent(3), xyz(3)
  real(idp)              :: alpha
  real(idp)              :: rmax, err, r
  real(idp)              :: kappa, ans
  real(idp), parameter   :: eps = 1.d-13
  real(idp), parameter   :: rmx = 50.d0
  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j
  !--------------------------------------------

  write(iout,*)'*********************************'
  write(iout,*)' sin test:',&
    ' do integral of a sin on the grid'
  write(iout,*) "Sin func's center:", cent
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  write(iout,*)'*********************************'
  !*********************************

  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts

      xyz(:) = atm_grid(icent)%xyz(ipt,:)

      r = sqrt(dot_product(xyz,xyz))
      if(r <= rmx+eps) then

      intg(icent) = intg(icent)   +     &
        sin(kappa*r)              *     & 
        atm_grid(icent)%w_xyz(ipt)*atm_grid(icent)%becke_norm(ipt)

      endif

    end do
  end do

  ans = sum(intg)

  analytic =4.d0* pi*((2.d0-kappa**2 * rmx**2)*cos(kappa*rmx)+2.d0*kappa*rmx*sin(kappa*rmx)-2.d0)/kappa**3 
  err = abs( analytic- sum(intg) )/abs(analytic) * 100.d0

  write(iout,*)'result:', intg(:), ans
  print'("Num ans:",F20.12,";  Anlyt. ans:",F20.12,";  Error:",Es7.0)', ans,analytic, err
  print'("difference:",Es7.0)',  abs( analytic- sum(intg) )

end subroutine


!************************************************************************************
subroutine integral_gaussian_bessel(atm_grid, cent, alpha,kappa,err)
  !---------------------------------------------
  type(atom_on_grid)     :: atm_grid(:)

  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic
  real(idp)              :: cent(3)
  real(idp)              :: alpha
  real(idp)              :: rmax, err
  real(idp)              :: kappa, ans

  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j
  !--------------------------------------------
  write(iout,*)'*********************************'
  write(iout,*)' Gaussian+Bessel test:',&
    ' do integral of a Gaussian Central Spherical Bessel overlap at a certain center on the grid'
  write(iout,*) "Gaussian's center:", cent
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  write(iout,*) "alpha=", alpha
  write(iout,*)'*********************************'
  !*********************************
  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts

      intg(icent) = intg(icent)    +                  &
        gaussian_bessel(atm_grid(icent)%xyz(ipt,:)   ,&
        atm_grid(icent)%w_xyz(ipt)                   ,&
        atm_grid(icent)%becke_norm(ipt),alpha,kappa,cent)

    end do
  end do

  ans = sum(intg)
  !analytic = 1.d0
  !err = abs( analytic- sum(intg) )/analytic * 100.d0
  write(iout,*)'result:', intg(:), ans
  !write(iout,*)'Normed analytical:',analytic
  !write(iout,*) 'percent diff:',err 
  print*, alpha, ans
  !print*, alpha, err

end subroutine

!*****************************************************************
!
!
!*****************************************************************
subroutine integral_gaussian_all_atm_centers(atm_grid, alpha)

  type(atom_on_grid)     :: atm_grid(:)
  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic
  real(idp)              :: cent(3)
  real(idp)              :: alpha(:), alpha_c
  real(idp)              :: rmax
  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j, ycent, loc

  write(iout,*)'*********************************'
  write(iout,*)' Gaussian test:',&
    ' do integral of Gaussian functions at all atomic centers on the grid'
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  !check that the size of alpha matches num of atoms at least
  !it could match number of atoms + center grid as well
  if(size(alpha)/= num_cent)then
    if(size(alpha) /= num_cent - 1) then
      stop 'number of exponents alpha of Gaussians does'// &
        'not match num_atoms or num_centers'
    else
      loc = 2
    end if
  else
    loc = 1
  end if
  write(iout,*)'*********************************'
  !*********************************
  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts

      do ycent = loc, num_cent

        cent = atm_grid(ycent)%center


        !! write(iout,*) "Yukawa's center:", cent
        intg(icent) = intg(icent) +     &
          gaussian(atm_grid(icent)%xyz(ipt,:),&
          atm_grid(icent)%w_xyz(ipt)    ,&
          atm_grid(icent)%becke_norm(ipt),alpha(ycent),cent)
        !write(iout,*)ipt, atm_grid(icent)%xyz(ipt,:)
        !write(iout,*)   atm_grid(icent)%w_xyz(ipt)
        !write(iout,*)   atm_grid(icent)%becke_norm(ipt)

      end do
    end do
  end do

  analytic = real(num_cent-1)*1.d0
  write(iout,*) "contribution of each grid:"

  write(iout,*) "center grid: ", intg(1)
  do icent = 2, num_cent
    write(iout,'(A5,I0,A2,f20.10)') "atom_",icent, ": ", intg(icent)
  end do

  write(iout,*)'final numerical result:', sum(intg)
  write(iout,*)'analytical:',analytic
  write(iout,*) 'percent diff:', abs( analytic- sum(intg) )/analytic * 100.d0

end subroutine
!*****************************************************
! Computes a normalized Gaussian integral, numerically
! Beaware that this function uses becke-weight
!  Input : xyz, real(idp), dimension(3) : coordinates of
!                                  one point of the grid
!      w, real(idp) : weight of the grid point
!      bw, real(idp) : Becke weight
!      alpha, real(idp): exponent of the Gaussian
!      cent, real(idp), dimesnsion(3), Cartesian
!          coordinate of the center of the Gaussian func. 
!*****************************************************
function gaussian(xyz, w, bw, alpha, cent)

  real(idp) :: xyz(:),w, bw,alpha
  real(idp) :: gaussian, r2, norm
  real(idp) :: cent(3)

  r2 = dot_product(xyz-cent,xyz-cent)
  norm = sqrt(2.d0*pi)
  norm = norm ** 3
  norm = sqrt(alpha) ** 3 / norm
  gaussian = norm * exp(-0.5d0* alpha * r2) * w * bw

end function

!********************************************************
function gaussian_bessel(xyz, w, bw, alpha, kappa,cent)

  real(idp) :: xyz(:),w, bw,alpha
  real(idp) :: gaussian_bessel, r2, rc
  real(idp) :: cent(3)
  real(idp) :: kappa
  real(idp), parameter :: eps=1.d-16


  r2 = dot_product(xyz-cent,xyz-cent)
  rc = sqrt(dot_product(xyz,xyz))
  if(rc .gt. eps) then
    gaussian_bessel = exp(-0.5d0* alpha * r2) * sin(kappa*rc)/rc* w * bw
  else  
    gaussian_bessel = exp(-0.5d0* alpha * r2) * w * bw
  end if
end function

function psi1(xyz, alpha, kappa, cent)

  real(idp) :: xyz(:),alpha
  real(idp) :: psi1, r2, rc,r
  real(idp) :: cent(3)
  real(idp) :: kappa
  real(idp), parameter :: eps=1.d-16


  r2 = dot_product(xyz-cent,xyz-cent)
  r  = sqrt(r2)
  if(r .gt. eps) then
    psi1 = xyz(2)* ((bessel_jn(1,kappa*r)/r)+exp(-alpha*r))
  else  
    psi1 = xyz(2)* (0.5d0*kappa + exp(-alpha*r))
  end if

end function


function psi2(xyz, alpha, kappa, cent)

  real(idp) :: xyz(:),alpha
  real(idp) :: psi2, r2, rc,r
  real(idp) :: cent(3)
  real(idp) :: kappa
  real(idp), parameter :: eps=1.d-16

  !    print*, 'xyz-cent:',xyz-cent
  r2 = dot_product(xyz-cent,xyz-cent)
  !    print*, 'r2:',r2
  r  = sqrt(r2)
  !    print*, 'r:',r
  psi2 =  (bessel_jn(0,kappa*r)+exp(-alpha*r))
  !    print*, 'psi2:',psi2  

end function
!*****************************************************************
! Overlap integral test of two functions:
! psi1 = y(j_1(k |r- cent1|)/|r-cent1|+ exp(-lambda |r-cent1|)
! psi2 = j_0(k |r-cent2|) + exp(-lambda |r-cent2|) - j_0(k |r-cent3|)
!        - exp(-lambda |r-cent3|)
! 
!*****************************************************************
subroutine integral_psi1N_psi2O12(atm_grid,num_atms ,alpha,kappa)

  type(atom_on_grid)     :: atm_grid(:)
  integer                :: num_atms
  real(idp), allocatable :: intg(:)
  real(idp), parameter   :: eps = 1.d-13
  real(idp)              :: cent(3,3), xyz(3)
  real(idp)              :: alpha(:), kappa,alpha_atm(3)
  real(idp)              :: rmax
  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j, ycent, loc

  write(iout,*)'*********************************'
  write(iout,*)' Overlap psi1 and pasi2 test:',&
    ' do integral of mix of Bessel and Gaussin mimicking continum functions'
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  if(size(alpha) == 1)then
    alpha_atm(:) = alpha(1)
  elseif(size(alpha) == num_atms)then
    alpha_atm  = alpha
  else  
    stop 'size of alphas array should match atom numbers or be 1!'
  endif    
  print*, 'alphas:', alpha_atm
  !check that the number of grids matches num of atoms.
  !it could match number of atoms + center grid as well
  if(num_atms == num_cent) then

    loc  = 1
    do  j= loc, num_cent      
      cent(:,j) = atm_grid(j)%center
      print*, cent(:,j)
    end do

  elseif(num_atms == num_cent - 1) then
    if(num_atms /= 0 ) then
      loc  = 2   
      do  j= loc, num_cent      
        cent(:,j-1) = atm_grid(j)%center
        print*, cent(:,j-1)
      end do

    elseif(num_atms == 0) then

      loc = 1

      cent(:,1) = [0.0D+0,0.0D+0, 0.690883871163182D+0]
      cent(:,2) = [0.0D+0, 2.14030380875115D+0,-0.302167207327661D+0]
      cent(:,3) = [0.0D+0,-2.14030380875115D+0,-0.302167207327661D+0]

    else

      stop 'check num_atms!' 

    endif

  endif


  write(iout,*)'*********************************'
  !*********************************
  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts
      !
      !        do ycent = loc, num_cent
      !
      !          cent = atm_grid(ycent)%center
      !           
      xyz(:) = atm_grid(icent)%xyz(ipt,:)
      if(distance(xyz,[0.d0,0.d0,0.d0]) <= 50.d0+eps) then

        !           print*, xyz, cent(:,2), cent(:,3)
        !           print*,& 
        !           psi1(xyz, alpha_atm(1), kappa, cent(:,1)), &
        !           (psi2(xyz, alpha_atm(2), kappa, cent(:,2)) -  psi2(xyz, alpha_atm(3), kappa, cent(:,3))) , &
        !            atm_grid(icent)%w_xyz(ipt),atm_grid(icent)%becke_norm(ipt); stop

        intg(icent) = intg(icent) +     &
          psi1(xyz, alpha_atm(1), kappa, cent(:,1)) * &
          (psi2(xyz, alpha_atm(2), kappa, cent(:,2)) -  psi2(xyz, alpha_atm(3), kappa, cent(:,3))) * &
          atm_grid(icent)%w_xyz(ipt)*atm_grid(icent)%becke_norm(ipt)
        !           atm_grid(icent)%
        !        !write(iout,*)ipt, atm_grid(icent)%xyz(ipt,:)
        !        !write(iout,*)   atm_grid(icent)%w_xyz(ipt)
        !        !write(iout,*)   atm_grid(icent)%becke_norm(ipt)
      endif
      !       end do
    end do
  end do
  !
  !    analytic = real(num_cent-1)*1.d0
  write(iout,*) "contribution of each grid:"
  write(*,*) "contribution of each grid:"
  !
  if(loc == 2) then
    write(iout,*) "center grid: ", intg(1)  
    write(*,*) "center grid: ", intg(1)  
    do icent = loc, num_cent
      write(iout,'(A5,I0,A2,Es20.5)') "atom_",icent-1, ": ", intg(icent)
      write(*,'(A5,I0,A2,Es20.5)') "atom_",icent-1, ": ", intg(icent)
    end do
  else
    do icent = loc, num_cent
      write(iout,'(A5,I0,A2,Es20.5)') "atom_",icent, ": ", intg(icent)
      write(*,'(A5,I0,A2,Es20.5)') "atom_",icent, ": ", intg(icent)
    end do
  endif
  !
  write(iout,*)'final numerical result:', sum(intg)
  write(*,*)'final numerical result:', sum(intg)
  !    write(iout,*)'analytical:',analytic
  !    write(iout,*) 'percent diff:', abs( analytic- sum(intg) )/analytic * 100.d0

end subroutine
!*****************************************************
!Tests a set of default values of exponentials for the 
! Gaussian functions, compares the result with analytical
! results.
! Input : Type atom_on_grid, dimension(number of grids)
!*****************************************************
subroutine test_default_gbs(atm_grid)
  type(atom_on_grid)     :: atm_grid(:)
  integer :: num_atms
  integer :: i, j, iter
  real(idp) :: cent(3), err
  real(idp)::alpha_array(6,3)
  real(idp):: cent_array(3,3)
  real(idp) :: alpha
  alpha_array = reshape(&
    [0.9910616896D+02,0.1805231239D+02,0.4885660238D+01, &
    0.3780455879D+01,0.8784966449D+00,0.2857143744D+00, &
    !                     [0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
  !                     0.5033151319D+01,0.1169596125D+01,0.3803889600D+00, &
  0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
    0.5033151319D+01,0.1169596125D+01,0.3803889600D+00, &
    0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
    0.5033151319D+01,0.1169596125D+01,0.3803889600D+00],&
    shape(alpha_array))
  !    alpha_array = reshape([1.d+3,1.d+2,1.d+1,1.0d0,1.d-1, &!0.1805231239D+02,  &
  !                           1.d+3,1.d+2,1.d+1,1.0d0,1.d-1, &
  !                           1.d+3,1.d+2,1.d+1,1.0d0,1.d-1],&! [0.9910616896D+02,0.1805231239D+02,  &

  ! 0.9910616896D+03,0.1805231239D+02,  &! 0.1307093214D+03,0.2380886605D+02,  &
  !  0.1307093214D+03,0.2380886605D+02], &
  !                     shape(alpha_array))
  cent_array = reshape(&![0.0D+0, 0.0D+0,  2.0D+0 ,&
    ! 0.0D+0, 0.0D+0, -2.0D+0 ,&
  ! 0.0D+0, 0.0D+0,  5.0D+0],&
  [0.0D+0,0.0D+0, 0.690883871163182D+0, &     
    0.0D+0, 2.14030380875115D+0,-0.302167207327661D+0, &     
    !0.0D+0, 0.D+0, 0.0D0,&!0.690883871163182D+0,&     
  0.0D+0,-2.14030380875115D+0,-0.302167207327661D+0],&
    ! 0.0D+0,0.0D+0,5.0D+0],&
  shape(cent_array))
  open(unit=10, file='test_set.csv')
  !     write(10,*) 'alpha', ',', 'percent error'
  !    print*, size(alpha_array,1)
  iter = size(alpha_array,1)
  num_atms = size(atm_grid) - 1
  if (num_atms == 0)then

    do i = 1, size(alpha_array,2)
      cent = cent_array(:,i)
      do j = 1, iter
        alpha = alpha_array(j,i)
        !           print*, 'alpha:', alpha 
        call integral_gaussian(atm_grid, cent, alpha,err)
        write(10, *) alpha, ',', err
      end do   
    end do

  else
    if(num_atms > size(alpha_array,1)) stop 'this test is devised for number of atoms <=3'
    do i = 1 , num_atms
      cent = atm_grid(i+1)%center![0.d0, 0.d0, 2.d0]
      do j = 1, iter
        alpha = alpha_array(j,i)
        !         print*, 'alpha:', alpha 
        call integral_gaussian(atm_grid, cent, alpha,err)
        write(10, *) alpha, ',', err
      end do
    end do

  end if  
  close(10)
end subroutine

!******************************************************************
! For a range of default Gaussian functions the numerical integral
! of a overlaps of the Gaussian functions is computed and compared
! to analytical result. (S-waves are only tested)
! Input : atm_grid : Type(atom_on_grid), dimension(number of atomic grids)
!******************************************************************
subroutine overlap_integral_two_gaussians_at_atm_centers(atm_grid)
  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  type(atom_on_grid)     :: atm_grid(:)
  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic
  real(idp)              :: cent1(3),cent2(3)
  real(idp)              :: alpha_array(6,3)
  real(idp)              :: alpha1, alpha2
  real(idp)              :: rmax
  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, i,j, icent1, icent2, loc
  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  alpha_array = reshape([0.9910616896D+02,0.1805231239D+02,0.4885660238D+01, &
    0.3780455879D+01,0.8784966449D+00,0.2857143744D+00, &
    0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
    0.5033151319D+01,0.1169596125D+01,0.3803889600D+00, &
    0.1307093214D+03,0.2380886605D+02,0.6443608313D+01, &
    0.5033151319D+01,0.1169596125D+01,0.3803889600D+00],&
    shape(alpha_array))

  write(iout,*)'*********************************'
  write(iout,*)' Gaussian test:',&
    'integral of 2 Gaussian functions overlaps sitting at two atomic centers'
  size_grid = sum(atm_grid(:)%num_pts)
  write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  write(iout,*) 'Rmax=',rmax
  !check that the size of alpha matches num of atoms at least
  !it could match number of atoms + center grid as well

  loc = 2
  write(iout,*)'*********************************'
  !*********************************
  allocate(intg(num_cent))


  do icent1 = loc, num_cent - 1
    do icent2 = icent1 + 1, num_cent

      cent1 = atm_grid(icent1)%center
      cent2 = atm_grid(icent2)%center
      write(iout,*) 'centers:', icent1 - 1, icent2 - 1

      do i = 1, 6
        do j = 1, 6
          alpha1 = alpha_array(i,icent1-1)
          alpha2 = alpha_array(j,icent2-1)
          write(iout,*) 'alpha1:', alpha1
          write(iout,*) 'alpha2:', alpha2
          write(iout,*)'*********************************'

          analytic = analytic_overlap_2swave_Gaussians(&
            alpha1,alpha2,cent1,cent2)
          if(analytic > 1.d-16) then

            intg = 0.d0

            do icent = 1, num_cent
              do ipt = 1, atm_grid(icent)%num_pts

                intg(icent) = intg(icent) +                             &
                  gaussian_overlap_2swave(                  &
                  atm_grid(icent)%xyz(ipt,:)     ,&
                  atm_grid(icent)%w_xyz(ipt)     ,&
                  atm_grid(icent)%becke_norm(ipt),&
                  alpha1,alpha2,cent1,cent2       )

              end do
            end do

            !analytic = analytic_overlap_2swave_Gaussians(&
            !                     alpha1,alpha2,cent1,cent2)

            write(iout,*) "contribution of each grid:"

            write(iout,*) "center grid: ", intg(1)

            do icent = 2, num_cent
              write(iout,'(A5,I0,A2,f20.10)') "atom_",icent, ": ", &
                intg(icent)
            end do

            write(iout,*)'final numerical result:', sum(intg)
            write(iout,*)'analytical:',analytic
            write(iout,*) 'percent diff:', abs( analytic- sum(intg) )&
              /analytic * 100.d0
          else

            write(iout,*) 'no overlap, analytic:', analytic

          endif
        end do
      end do

    end do
  end do



end subroutine

!************************************************************************
! Given a point and its weight and Becke_weight computes the contribution
! of the point to the integral of the overlap of two S-wave Gaussian 
! functions
!************************************************************************
function gaussian_overlap_2swave(xyz, w, bw, alpha1, alpha2, cent1,cent2)

  real(idp) :: xyz(:),w, bw,alpha1, alpha2
  real(idp) :: gaussian_overlap_2swave, r2_1, r2_2
  real(idp) :: cent1(3), cent2(3)
  r2_1 = dot_product(xyz-cent1,xyz-cent1)
  r2_2 = dot_product(xyz-cent2,xyz-cent2)

  gaussian_overlap_2swave =   exp(- alpha1 * r2_1) *        &
    exp(- alpha2 * r2_2) * w * bw

end function

!************************************************************************
! Finds analytic answer to the overlap of two S-wave gaussian functions
!************************************************************************
function analytic_overlap_2swave_Gaussians(alpha1,alpha2,cent1,cent2)

  real(idp) :: alpha1, alpha2
  real(idp) :: analytic_overlap_2swave_Gaussians, r2, analytic
  real(idp) :: cent1(3), cent2(3)
  r2 = dot_product(cent2-cent1,cent2-cent1)
  !print*, r2
  analytic = -alpha1 * alpha2 * r2
  !print*, analytic
  analytic = analytic/ (alpha1 + alpha2)
  !print*, analytic
  !print*, 'pi/(alpha1+ alpha2):',pi/(alpha1+alpha2)
  analytic = exp(analytic) * sqrt((pi/(alpha1+alpha2)))
  !print*, 'after exp:',analytic

  analytic_overlap_2swave_Gaussians = analytic * (pi/(alpha1+alpha2))

end function


!*********************************************************
!
!*********************************************************
subroutine integral_gaussian2(atm_grid, cent, alpha,err)

  type(atom_on_grid)     :: atm_grid(:)
  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic
  real(idp)              :: cent(3)
  real(idp)              :: alpha
  real(idp)              :: rmax, err
  integer                :: size_grid
  integer                :: num_cent
  integer                :: icent, ipt, j

  !    write(iout,*)'*********************************'
  !    write(iout,*)' Gaussian test:',&
  !      ' do integral of a Gaussian at a certain center on the grid'
  !    write(iout,*) "Gaussian's center:", cent
  size_grid = sum(atm_grid(:)%num_pts)
  !    write(iout,*)'grid size:',size_grid
  num_cent  = size(atm_grid)
  !    write(iout,*)'number of centers:',num_cent
  rmax = maxval(atm_grid(:)%max_radius)
  !    write(iout,*) 'Rmax=',rmax
  !    write(iout,*) "alpha=", alpha
  !    write(iout,*)'*********************************'
  !*********************************
  allocate(intg(num_cent))
  intg = 0.d0

  do icent = 1, num_cent
    do ipt = 1, atm_grid(icent)%num_pts

      intg(icent) = intg(icent)    +           &
        gaussian(atm_grid(icent)%xyz(ipt,:)   ,&
        atm_grid(icent)%w_xyz(ipt)            ,&
        atm_grid(icent)%becke_norm(ipt),alpha,cent)
    end do
  end do

  analytic = 1.d0

  !    write(iout,*)'result:', intg(:), sum(intg)
  !    write(iout,*)'Normed analytical:',analytic
  !    write(iout,*) 'percent diff:', 
  err= abs( analytic- sum(intg) )/analytic * 100.d0

end subroutine


!*****************************************************
! Description: Given an lmax and grid parameters the
!              integral of Y_{lm} on the grid is computed
!              Result of all \int Y_{lm} d\Omega must 
!              be zero, except for Y_{00}, which is
!              2 sqrt{pi}  
!*****************************************************
subroutine test_ylm_on_grid(atm_grid,lmax)
  use sphericalharmonics
  use lmindx_map
  !++++++++++++++++++++++++++++++++++++++++++
  integer :: lmax, ang_num, iomg
  type(atom_on_grid)     :: atm_grid
  real(idp), allocatable :: ylm_on_grid(:,:)
  real(idp), allocatable :: theta(:), phi(:)
  !++++++++++++++++++++++++++++++++++++++++++

  print*,'lmax=', lmax
  print*, '------------------------------------------------------' 
  ang_num = atm_grid%ang_num
  allocate(theta(ang_num), phi(ang_num))

  do iomg = 1, ang_num
    theta(iomg) = atm_grid%rthetaphi(iomg, 2)
    phi  (iomg) = atm_grid%rthetaphi(iomg, 3)
  end do

  call real_spherical_harmonics_on_grid(ylm_on_grid,theta, phi, lmax)
  print*,  '          ','l     ' , '     m  ', '  int ylm d\omega'
  print*, '-------------------------------------------------------' 
  do iomg = 1, size(ylm_on_grid,2)
    print*, indx_to_lm(iomg,lmax),sum(ylm_on_grid(:,iomg)*atm_grid%w_omega(:))
  end do    

end subroutine


!********************************************************************
! Description: Given an lmax and grid parameters the
!              integral of Y_{lm}*Y_{l'm'} on the grid is computed
!              Result of all integrals must be delta(ll') delta(mm')
!********************************************************************
subroutine test_ylm2_on_grid(atm_grid,lmax)
  use sphericalharmonics
  use lmindx_map
  !++++++++++++++++++++++++++++++++++++++++++
  integer :: lmax, ang_num, iomg, iomg1
  type(atom_on_grid)     :: atm_grid
  real(idp), allocatable :: ylm_on_grid(:,:)
  real(idp), allocatable :: theta(:), phi(:)
  !++++++++++++++++++++++++++++++++++++++++++

  print*,'lmax=', lmax
  print*, '------------------------------------------------------' 
  ang_num = atm_grid%ang_num
  allocate(theta(ang_num), phi(ang_num))

  do iomg = 1, ang_num
    theta(iomg) = atm_grid%rthetaphi(iomg, 2)
    phi  (iomg) = atm_grid%rthetaphi(iomg, 3)
  end do

  call real_spherical_harmonics_on_grid(ylm_on_grid,theta, phi, lmax)

  print*,  '          ','l1     ' , '     m1  ', 'l2     ' , '     m2  ','  int Yl1m1*Yl2m2 d\omega'
  print*, '---------------------------------------------------------------------------------' 
  do iomg = 1, size(ylm_on_grid,2)
    do iomg1 = iomg, size(ylm_on_grid,2)

      print*, indx_to_lm(iomg,lmax),indx_to_lm(iomg1,lmax),&
        sum(ylm_on_grid(:,iomg)*ylm_on_grid(:,iomg1)*atm_grid%w_omega(:))

    end do
  end do

end subroutine

!**********************************************************************
! Need description here
!**********************************************************************
subroutine test_yukawa_range(atm_grid,num_atms,error,alpha_vals,single_cent_offcen_coord)

  type(atom_on_grid)  :: atm_grid(:)

  integer, intent(in) :: num_atms
  integer             :: i, j, iter
  integer, parameter  :: num_alpha = 7                

  real(idp)                         :: cent(3), err
  real(idp)                         :: cent_array(3,3)
  real(idp)                         :: alpha
  real(idp), optional               :: single_cent_offcen_coord(3)
  real(idp), optional               :: alpha_vals(:)
  real(idp), optional , allocatable :: error(:,:)
  real(idp),            allocatable :: alpha_array(:,:)
  real(idp), dimension(num_alpha)   :: stock_alpha = [1.d+3,1.d+2,1.d+1,1.0d0,1.d-1,1.d-2,1.d-3]

  logical   :: has_center


  has_center = check_center(atm_grid, num_atms)
  print*, ' '
  print*, '    alpha    ', '                %diff error    '
  print*, '-------------------------------------------------'

  cent_array = reshape(&
    [0.0D+0, 0.00000000000000D+0, 0.690883871163182D+0, &     
    0.0D+0, 2.14030380875115D+0,-0.302167207327661D+0, &     
    0.0D+0,-2.14030380875115D+0,-0.302167207327661D+0],&
    shape(cent_array))

  !     open(unit=10, file='test_set.csv')
  !     write(10,*) 'alpha', ',', 'percent error'
  !    print*, size(alpha_array,1)

  ! Initialize alpha_array
  if(present(alpha_vals) .and. num_atms /= 0) then
    allocate(alpha_array(size(alpha_vals),num_atms))   
    do i = 1, num_atms
      alpha_array(:,i)= alpha_vals
    end do      
  elseif(present(alpha_vals) .and. num_atms == 0) then

    if(present(single_cent_offcen_coord))then
      allocate(alpha_array(size(alpha_vals),1))
      alpha_array(:,1)= alpha_vals
    else         
      allocate(alpha_array(size(alpha_vals),size(cent_array,2)))
      do i = 1, size(cent_array,2)
        alpha_array(:,i)= alpha_vals
      end do
    endif

  elseif(.not. present(alpha_vals) .and. num_atms == 0) then
    if(present(single_cent_offcen_coord))then
      allocate(alpha_array(num_alpha,1))
      alpha_array(:,1)= stock_alpha
    else        
      allocate(alpha_array(num_alpha,size(cent_array,2)))
      do i = 1, size(cent_array,2)
        alpha_array(:,i)= stock_alpha
      end do
    endif       
  elseif(.not. present(alpha_vals) .and. num_atms /= 0) then        
    allocate(alpha_array(num_alpha,num_atms))
    do i = 1, num_atms
      alpha_array(:,i)= stock_alpha
    end do      
  end if   

  !this is single center case
  if (num_atms == 0)then

    if(present(error))then
      if(.not. allocated(error)) then
        allocate(error, source=alpha_array)
      end if
    end if  

    iter = size(alpha_array,1)
    if(present(single_cent_offcen_coord))then

      cent = single_cent_offcen_coord
      print'(A9,3(F10.5,2x))', '  center:', cent
      print*, '-------------------------------------------------'
      do j = 1,iter 
        alpha = alpha_array(j,1)
        !           print*, 'alpha:', alpha 
        call integral_yukawa(atm_grid, cent, alpha,err)
        if(present(error)) error(j,i) = err
        !            write(10, *) alpha, ',', err
      end do   


    else

      do i = 1, size(alpha_array,2)
        cent = cent_array(:,i)
        print'(A9,3(F10.5,2x))', '  center:', cent
        print*, '-------------------------------------------------'
        do j = 1, iter
          alpha = alpha_array(j,i)
          !           print*, 'alpha:', alpha 
          call integral_yukawa(atm_grid, cent, alpha,err)
          if(present(error)) error(j,i) = err
          !            write(10, *) alpha, ',', err
        end do   
      end do

    endif

  else


    if(present(error))then
      if(.not. allocated(error)) then
        allocate(error, source=alpha_array)
      end if
    end if

    iter = size(alpha_array,1)

    do i = 1 , num_atms

      if(has_center) then
        cent = atm_grid(i+1)%center![0.d0, 0.d0, 2.d0]
        print'(A9,3(F10.5,2x))', '  center:', cent
        print*, '-------------------------------------------------'
      else
        cent = atm_grid(i)%center
      end if

      do j = 1, iter
        alpha = alpha_array(j,i)
        !         print*, 'alpha:', alpha 
        call integral_yukawa(atm_grid, cent, alpha, err)
        if(present(error)) error(j,i) = err
        !            write(10, *) alpha, ',', err
      end do
    end do

  end if  
  !    close(10)
end subroutine

!**********************************************************************
! Need description here
!**********************************************************************
subroutine test_gbs_range(atm_grid,num_atms,error,alpha_vals, single_cent_offcen_coord)

  type(atom_on_grid)  :: atm_grid(:)

  integer, intent(in) :: num_atms
  integer             :: i, j, iter
  integer, parameter  :: num_alpha = 7                

  real(idp)                         :: cent(3), err
  real(idp)                         :: cent_array(3,3)
  real(idp)                         :: alpha
  real(idp), optional               :: single_cent_offcen_coord(3)
  real(idp), optional               :: alpha_vals(:)
  real(idp), optional , allocatable :: error(:,:)
  real(idp),            allocatable :: alpha_array(:,:)
  real(idp), dimension(num_alpha)   :: stock_alpha = [1.d+3,1.d+2,1.d+1,1.0d0,1.d-1,1.d-2,1.d-3]

  logical   :: has_center


  has_center = check_center(atm_grid, num_atms)
  print*, ' '
  print*, '    alpha    ', '                %diff error    '
  print*, '-------------------------------------------------'

  cent_array = reshape(&
    [0.0D+0, 0.00000000000000D+0, 0.690883871163182D+0, &     
    0.0D+0, 2.14030380875115D+0,-0.302167207327661D+0, &     
    0.0D+0,-2.14030380875115D+0,-0.302167207327661D+0],&
    shape(cent_array))

  !     open(unit=10, file='test_set.csv')
  !     write(10,*) 'alpha', ',', 'percent error'
  !    print*, size(alpha_array,1)

  ! Initialize alpha_array
  if(present(alpha_vals) .and. num_atms /= 0) then
    allocate(alpha_array(size(alpha_vals),num_atms))   
    do i = 1, num_atms
      alpha_array(:,i)= alpha_vals
    end do      
  elseif(present(alpha_vals) .and. num_atms == 0) then

    if(present(single_cent_offcen_coord))then
      allocate(alpha_array(size(alpha_vals),1))
      alpha_array(:,1)= alpha_vals
    else         
      allocate(alpha_array(size(alpha_vals),size(cent_array,2)))
      do i = 1, size(cent_array,2)
        alpha_array(:,i)= alpha_vals
      end do
    endif

  elseif(.not. present(alpha_vals) .and. num_atms == 0) then
    if(present(single_cent_offcen_coord))then
      allocate(alpha_array(num_alpha,1))
      alpha_array(:,1)= stock_alpha
    else        
      allocate(alpha_array(num_alpha,size(cent_array,2)))
      do i = 1, size(cent_array,2)
        alpha_array(:,i)= stock_alpha
      end do
    endif       
  elseif(.not. present(alpha_vals) .and. num_atms /= 0) then        
    allocate(alpha_array(num_alpha,num_atms))
    do i = 1, num_atms
      alpha_array(:,i)= stock_alpha
    end do      
  end if   

  !this is single center case
  if (num_atms == 0)then

    if(present(error))then
      if(.not. allocated(error)) then
        allocate(error, source=alpha_array)
      end if
    end if  

    iter = size(alpha_array,1)
    if(present(single_cent_offcen_coord))then

      cent = single_cent_offcen_coord
      print'(A9,3(F10.5,2x))', '  center:', cent
      print*, '-------------------------------------------------'
      do j = 1,iter 
        alpha = alpha_array(j,1)
        !           print*, 'alpha:', alpha 
        call integral_gaussian(atm_grid, cent, alpha,err)
        if(present(error)) error(j,i) = err
        !            write(10, *) alpha, ',', err
      end do   


    else

      do i = 1, size(alpha_array,2)
        cent = cent_array(:,i)
        print'(A9,3(F10.5,2x))', '  center:', cent
        print*, '-------------------------------------------------'
        do j = 1, iter
          alpha = alpha_array(j,i)
          !           print*, 'alpha:', alpha 
          call integral_gaussian(atm_grid, cent, alpha,err)
          if(present(error)) error(j,i) = err
          !            write(10, *) alpha, ',', err
        end do   
      end do

    endif

  else


    if(present(error))then
      if(.not. allocated(error)) then
        allocate(error, source=alpha_array)
      end if
    end if

    iter = size(alpha_array,1)

    do i = 1 , num_atms

      if(has_center) then
        cent = atm_grid(i+1)%center![0.d0, 0.d0, 2.d0]
        print'(A9,3(F10.5,2x))', '  center:', cent
        print*, '-------------------------------------------------'
      else
        cent = atm_grid(i)%center
      end if

      do j = 1, iter
        alpha = alpha_array(j,i)
        !         print*, 'alpha:', alpha 
        call integral_gaussian(atm_grid, cent, alpha, err)
        if(present(error)) error(j,i) = err
        !            write(10, *) alpha, ',', err
      end do
    end do

  end if  
  !    close(10)
end subroutine



!**********************************************************************
! Need description here
!**********************************************************************
subroutine test_gbsbess_range(atm_grid,num_atms,error,alpha_vals,kappa_val, &
    single_cent_offcen_coord)

  type(atom_on_grid)  :: atm_grid(:)

  integer, intent(in) :: num_atms
  integer             :: i, j, iter
  integer, parameter  :: num_alpha = 7                

  real(idp)                         :: cent(3), err
  real(idp)                         :: cent_array(3,3)
  real(idp)                         :: alpha, kappa
  real(idp), optional               :: single_cent_offcen_coord(3)
  real(idp), optional               :: kappa_val
  real(idp), optional               :: alpha_vals(:)
  real(idp), optional , allocatable :: error(:,:)
  real(idp),            allocatable :: alpha_array(:,:)
  real(idp), dimension(num_alpha)   :: stock_alpha = [1.d+3,1.d+2,1.d+1,1.0d0,1.d-1,1.d-2,1.d-3]

  logical   :: has_center


  has_center = check_center(atm_grid, num_atms)
  print*, ' '
  print*, '    alpha    ', '                final result    '
  print*, '-------------------------------------------------'

  cent_array = reshape(&
    [0.0D+0, 0.00000000000000D+0, 0.690883871163182D+0, &     
    0.0D+0, 2.14030380875115D+0,-0.302167207327661D+0, &     
    0.0D+0,-2.14030380875115D+0,-0.302167207327661D+0],&
    shape(cent_array))

  !     open(unit=10, file='test_set.csv')
  !     write(10,*) 'alpha', ',', 'percent error'
  !    print*, size(alpha_array,1)

  ! Initialize alpha_array
  if(present(alpha_vals) .and. num_atms /= 0) then
    allocate(alpha_array(size(alpha_vals),num_atms))   
    do i = 1, num_atms
      alpha_array(:,i)= alpha_vals
    end do      
  elseif(present(alpha_vals) .and. num_atms == 0) then

    if(present(single_cent_offcen_coord))then
      allocate(alpha_array(size(alpha_vals),1))
      alpha_array(:,1)= alpha_vals
    else         
      allocate(alpha_array(size(alpha_vals),size(cent_array,2)))
      do i = 1, size(cent_array,2)
        alpha_array(:,i)= alpha_vals
      end do
    endif

  elseif(.not. present(alpha_vals) .and. num_atms == 0) then
    if(present(single_cent_offcen_coord))then
      allocate(alpha_array(num_alpha,1))
      alpha_array(:,1)= stock_alpha
    else        
      allocate(alpha_array(num_alpha,size(cent_array,2)))
      do i = 1, size(cent_array,2)
        alpha_array(:,i)= stock_alpha
      end do
    endif       
  elseif(.not. present(alpha_vals) .and. num_atms /= 0) then        
    allocate(alpha_array(num_alpha,num_atms))
    do i = 1, num_atms
      alpha_array(:,i)= stock_alpha
    end do      
  end if   

  if(present(kappa_val))then
    kappa = kappa_val
  else
    kappa = 1.d0    
  end if  

  !this is single center case
  if (num_atms == 0)then

    if(present(error))then
      if(.not. allocated(error)) then
        allocate(error, source=alpha_array)
      end if
    end if  

    iter = size(alpha_array,1)
    if(present(single_cent_offcen_coord))then

      cent = single_cent_offcen_coord
      print'(A9,3(F10.5,2x))', '  center:', cent
      print*, '-------------------------------------------------'
      do j = 1,iter 
        alpha = alpha_array(j,1)
        !           print*, 'alpha:', alpha 
        call integral_gaussian_bessel(atm_grid, cent, alpha,kappa,err)
        if(present(error)) error(j,i) = err
        !            write(10, *) alpha, ',', err
      end do   


    else

      do i = 1, size(alpha_array,2)
        cent = cent_array(:,i)
        print'(A9,3(F10.5,2x))', '  center:', cent
        print*, '-------------------------------------------------'
        do j = 1, iter
          alpha = alpha_array(j,i)
          !           print*, 'alpha:', alpha 
          call integral_gaussian_bessel(atm_grid, cent, alpha,kappa,err)
          if(present(error)) error(j,i) = err
          !            write(10, *) alpha, ',', err
        end do   
      end do

    endif

  else


    if(present(error))then
      if(.not. allocated(error)) then
        allocate(error, source=alpha_array)
      end if
    end if

    iter = size(alpha_array,1)

    do i = 1 , num_atms

      if(has_center) then
        cent = atm_grid(i+1)%center![0.d0, 0.d0, 2.d0]
        print'(A9,3(F10.5,2x))', '  center:', cent
        print*, '-------------------------------------------------'
      else
        cent = atm_grid(i)%center
      end if

      do j = 1, iter
        alpha = alpha_array(j,i)
        !         print*, 'alpha:', alpha 
        call integral_gaussian_bessel(atm_grid, cent, alpha,kappa,err)
        if(present(error)) error(j,i) = err
        !            write(10, *) alpha, ',', err
      end do
    end do

  end if  
  !    close(10)
end subroutine


!********************************************************************
!> check if the central grid is present and number of atoms and 
!! center is consistent with either atoms and a center or atoms alone
!*******************************************************************
function check_center(atm_grid, num_atms)result(has_center)
  !------------------------------------------ 
  type(atom_on_grid) :: atm_grid(:)
  integer            :: num_atms
  logical            :: has_center 
  !------------------------------------------
  if(size(atm_grid) == num_atms + 1)then
    has_center = .true.
  elseif (size(atm_grid) == num_atms) then
    has_center = .false.
  else
    stop 'number of atoms is not the same as grids or grids + center '
  end if 

end function  
!********************************************************************
! establishes spherical harmonic matrix on the angular grid (omega)
! Input : mat_per_grid type: yk, allocatable 
!         atom_on_grid type: atm_grid, dimension(num_cent) 
!         integer : lmax
!         
!
!********************************************************************
subroutine initilize_sphericalH_onGrids(yk,atm_grid,lmax)
  use sphericalHarmonics,only: real_spherical_harmonics_on_atmgrid
  type(mat_per_grid), allocatable :: yk(:)
  type(atom_on_grid) :: atm_grid(:)
  integer :: lmax
  integer :: num_cent
  integer :: icent
  integer :: k

  num_cent = size(atm_grid)
  allocate(yk(num_cent))
  do icent = 1, num_cent
    !fill y(iomeg,k) matrix; the real spherical harmonic $Y_k(\Omega)$
    call real_spherical_harmonics_on_atmgrid(yk(icent)%mat,&
      atm_grid(icent),lmax)

    !one could fill other properties by also calling initialize without
    ! dimensions
    call yk(icent)%initialize()
    !multiply the omega_weight into this matrix
    do k = 1, yk(icent)%dim2
      yk(icent)%mat(:,k)=yk(icent)%mat(:,k)*atm_grid(icent)%w_omega(:)
    end do

  end do  
end subroutine    

!************************************************************************
!
!
!************************************************************************
subroutine initialize_1sa1sb_density_onGrids(rho_n, atm_grid)

  type(mat_per_grid), allocatable :: rho_n(:)
  type(atom_on_grid) :: atm_grid(:)
  integer :: num_cent
  integer :: icent
  integer :: ang_num, r_num
  integer :: k
  integer :: ir, iomeg
  real(idp) :: f 

  num_cent = size(atm_grid)

  allocate(rho_n(num_cent))

  do icent = 1, num_cent

    k = 0
    r_num =  atm_grid(icent)%r_num
    ang_num =  atm_grid(icent)%ang_num

    call rho_n(icent)%initialize(r_num, ang_num)


    do ir = 1, r_num
      do iomeg = 1, ang_num

        k   = k + 1

        !!becke-weighted function 1s, but not grid weighted
        f= one_s(xyz  = atm_grid(icent)%xyz(k,:), & 
          bw = atm_grid(icent)%becke_norm(k),   &
          cent = atm_grid(2)%center         ) + &
          one_s(xyz  = atm_grid(icent)%xyz(k,:),    &  
          bw   = atm_grid(icent)%becke_norm(k), &
          cent = atm_grid(3)%center      ) 
        !            
        rho_n(icent)%mat(ir,iomeg) = f * f
        !
      end do
    end do

  end do    

end subroutine
!********************************************************************

!*******************************************************************
subroutine initialize_expansion_coeff(rho, atm_grid, rho_n, yk, kmax)

  use matmult,only: mat_mat_mul
  type(mat_per_grid) :: yk(:), rho_n(:)
  type(mat_per_grid), allocatable :: rho(:)
  type(atom_on_grid) :: atm_grid(:)
  integer :: num_cent
  integer :: icent
  integer :: r_num
  integer :: kmax

  num_cent = size(atm_grid) 

  allocate(rho(num_cent))

  do icent = 1, num_cent

    r_num =  atm_grid(icent)%r_num
    !  call print_vector(rho_n(icent)%mat(:,1)); stop  
    !    ! initialize rho_k => rho(ir, kappa)           
    !    ! \rho is \rho_k; the coefficient of expansion of \rho_n in Ylm's          
    call rho(icent)%initialize(r_num,kmax)

    rho(icent)%mat = matmul(rho_n(icent)%mat, yk(icent)%mat)

  end do  

end subroutine
!********************************************************************
! Need description here
!********************************************************************
subroutine test_hydrogen(atm_grid,atms)
  !====================================================================   
  !====================================================================
  type(atom_on_grid)     :: atm_grid(:)
  type(atom)             :: atms(:)
  type(mat_per_grid), allocatable :: yk(:), rho_n(:),rho(:)
  type(mat_per_grid), allocatable :: rho_intp(:)
  type(vec_per_grid), allocatable :: r_array(:)
  real(idp), allocatable :: intg(:)
  real(idp)              :: analytic, f
  integer                :: num_atms, num_cent
  integer                :: lmax, lmaxp1
  integer                :: kmax, r_num, ang_num
  integer                :: ir, iomeg, l, m, k, icent, rad_reg, &
    j , cs_r
  !===================================================================
  !accounting for the central grid that is not an atom
  num_cent = size(atm_grid)
  num_atms = size(atm_grid) - 1
  ! We assume a 1s hydrogen function on each atomic center 
  ! Test with two atomic centers 1.4 Bohr apart and central 
  ! grid at the center of mass
  if (num_atms < 2 ) stop 'the test_hydrogen need two atoms at least'
  !print*, size(atms); stop
  ! optimum truncation order of agular momentum numbers,
  ! lmax, has to be lquad(grid) roughly divided by two
  ! so lmax= 5 when lqad = 11 or 50 angular points in
  ! lebedev scheme. See Becke,Dickson, JCP 89, 2993 (1988)
  lmax = atm_grid(2)%lmax / 2
  lmaxp1 = lmax + 1
  kmax = (lmaxp1 ) * (lmaxp1)
  !   !
  !   ! *************************************************************************
  !     Some definitions:
  !       nrad   = number of radial grid points for center n
  !       nomega = number of angular grid points for center n
  !       kmax   = number of ylm func. for a given lmax
  !       for n centers there are n matrices of type
  !
  !       yk(n)%mat(nomega,kmax) => Y_lm(nomega, kmax) * w(nomega)
  !
  !       rho_n(n)%mat(nrad, nomega) = s1(nrad) * s2(nrad) * const. * bw(nrad,nomega)
  !       const. = y00 **2 = 
  !       bw = becke weights (no grid weight at this point)
  !       
  !       rho(n)%mat(nrad, kmax) = rho_n(n)%mat(nrad, nomega)* yk(n)%mat(nomega,kmax)
  !   ! ***************************************************************************

  !   ! allocating various matrix-holding types for all centers

  !   !  fill matrices of yk(num_cent)

  call initilize_sphericalH_onGrids(yk, atm_grid, lmax)
  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !    !  fill matrices of rho_n

  call initialize_1sa1sb_density_onGrids(rho_n,atm_grid)  
  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
  ! fill matrices of rho:
  !    ! initialize rho_k => rho(ir, kappa)           
  !    ! \rho is \rho_k; the coefficient of expansion of \rho_n in Ylm's

  call initialize_expansion_coeff(rho, atm_grid,rho_n, yk, kmax)          
  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ! solve poisson equation for the rho matrices on the right-hand-side

  allocate(r_array(num_cent))

  call solve_poisson_rho_mat(rho, r_num, kmax, lmax, atms)

  !The radial solution is stored back in rho (**note rho is not resized)
  ! (** however, the extra elements in radial dimension are zeroed)

  ! V_n(nrad, nomega) = rho(nrad,kmax) * yk^T(nomega, kmax)
  ! to save space V_n is stored back in rho_n and again extra radial
  ! elements due to redundancy of the radial grid points (because of
  ! bridge functions) are zeroed.



  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  !
  !    do icent = 1, num_cent
  !!      
  !      !fill y(iomeg,k) matrix; the real spherical harmonic $Y_k(\Omega)$
  !       call real_spherical_harmonics_on_atmgrid(yk(icent)%mat,atm_grid(icent),lmax) 
  !       !one could fill other properties by also calling initialize without dimensions
  !      call yk(icent)%initialize()
  !!          print*, yk(icent)%dim1, yk(icent)%dim2, yk(icent)%flag 
  !!
  !      !initialize rho_n(ir,iomeg)
  !!      $\rho_n(r, \Omega) = \rho ( r, \Omega) w^{(n)}_{becke}(r, \Omega)        
  !!      $\rho_n(r,\Omega) = \sum_\kappa \rho_\kappa Y(\Omega, \kappa)$
  !  
  !      k = 0
  !      r_num =  atm_grid(icent)%r_num
  !      ang_num =  atm_grid(icent)%ang_num
  !      call rho_n(icent)%initialize(r_num, ang_num)
  !          !!print*, rho_n(icent)%dim1, rho_n(icent)%dim2, rho_n(icent)%flag 
  !
  !
  !          !!print*, rho(icent)%dim1, rho(icent)%dim2, rho(icent)%flag 
  !      do ir = 1, r_num
  !        do iomeg = 1, ang_num
  !            
  !            k   = k + 1
  !            
  !            !!becke-weighted function 1s, but not grid weighted
  !            f= one_s(xyz  = atm_grid(icent)%xyz(k,:), & 
  !                     bw = atm_grid(icent)%becke_norm(k), &
  !                     cent = atm_grid(2)%center         )+  &
  !               one_s(xyz  = atm_grid(icent)%xyz(k,:), &  
  !                     bw   = atm_grid(icent)%becke_norm(k),    &
  !                     cent = atm_grid(3)%center      ) 
  !!            
  !            rho_n(icent)%mat(ir,iomeg) = f * f
  !!
  !        end do
  !      end do
  !     ! allocate(rho(r_num,kmax))
  !     !  call print_vector(rho_n(icent)%mat(:,1)); stop  
  !!    ! initialize rho_k => rho(ir, kappa)           
  !!    ! \rho is \rho_k; the coefficient of expansion of \rho_n in Ylm's          
  !      call rho(icent)%initialize(r_num,kmax)
  !
  !      rho(icent)%mat = matmul(rho_n(icent)%mat, yk(icent)%mat)
  !
  !!     call dgemm ('n', 'n', r_num, kmax,ang_num, 1.d0, &
  !!                    rho_n(icent)%mat, r_num,             &
  !!                    yk(icent)%mat   , ang_num,           &
  !!                    0.d0, rho(icent)%mat, r_num)
  !!     call mat_mat_mul(rho_n(icent)%mat,yk(icent)%mat,rho(icent)%mat)
  !
  !!    print*, rho_n(icent)%mat(1,1), yk(icent)%mat(1,1), rho(icent)%mat(1,1)            
  !!    deallocate(rho)
  !       
  !       !test of dvr_types
  !       
  !       rad_reg = size(atms(icent)%r)
  !!      ! This radius is in the atom-center frame:       
  !       call make_contiguous_r(atms(icent)%r,r_array(icent)%vec, w,index)
  !       ! to fill other properties of the type:       
  !       call r_array(icent)%initialize()
  !       cs_r = r_array(icent)%num_pts
  !       print*, 'cs_r:',cs_r
  !       call print_vector(r_array(icent)%vec)
  !     ! solve linear Poisson equation for each center:
  !     ! [d^2/dr^2 - l(l+1)/r^2] u(r,k) = -4pi rho(r,k)
  !     ! here rho(r,k) is right-hand matrix, 
  !     ! linear solver replaces mat_rhs with u(r,k). We then store u(r,k) back into
  !     ! rho(r,k) that we no longer need.
  !       do l = 0, lmax
  !            !k12 contains the index of all kappa related to a fixed l number 
  !            ! kappa <=> {l,m=-l,l}
  !            k12=indx_range(l)
  !            !!print*, 'k12:',k12            
  !            call  make_ddvr_ddvr_matrix(atms(icent)%r,l, tot_mat_r, fix_and_drop=fix)
  !            !!print*, 'rho'
  !            !!call print_matrix(rho(icent)%mat(:,k12(1):k12(2)))
  !            call make_rhs_matrix(atms(icent)%r,rho(icent)%mat(:,k12(1):k12(2)), mat_rhs,fix)
  !         !
  !         !   print*, 'mat before'
  !         !   call print_matrix(mat_rhs)
  !            call linear_solver_matmat(tot_mat_r, mat_rhs)
  !
  !            print*, ' mat right after linear solve'
  !            call print_matrix(mat_rhs)
  !
  !            do j = 1, k12(2)-k12(1) + 1 
  !                mat_rhs(:,j) = -mat_rhs(:,j) / r_array(icent)%vec(:)/ 4.0_idp/ pi
  !            end do    
  !            print*, 'mat after linear solve'
  !            call print_matrix(mat_rhs)
  !
  !
  !            ! store back into the rho(icent)%mat, but remeber that dimension 1 
  !            ! of rho is different (in general) from mat_rhs, which is a contigouos
  !            ! matrix in r (i.e. first dimension), so we store the dimension of r
  !            ! in the type
  !            do j = 1, cs_r
  !               
  !               rho(icent)%mat(j,k12(1):k12(2)) =   mat_rhs(j,:)
  !               
  !            end do
  !
  !
  !            !make the extras zero
  !            rho(icent)%mat(cs_r + 1: rho(icent)%dim1 , k12(1):k12(2)) = 0.d0
  !
  !            rho(icent)%dim1 = cs_r
  !
  !         ! print*, size(tot_mat_r,1),size(tot_mat_r,2)  
  !         ! stop
  !         !   do m = -l, l
  !         !      k = make_lm_indx(l,m)
  !         !   end do !m
  !         !   stop
  !            deallocate(tot_mat_r,mat_rhs)
  !
  !       end do !l 




  !      ! multiply back into yk matrix, but the transpose of yk matrix
  !      ! The Poisson potential of each center is back in the 
  !      ! cs_r by ang_num portion of rho_n(icent)%matrix      
  !      ! call dgemm ('n', 't', cs_r,ang_num,kmax, 1.d0,  &
  !      !              rho(icent)%mat, cs_r,              &
  !      !              yk(icent)%mat , ang_num,           &
  !      !              0.d0, rho_n(icent)%mat, cs_r)


  !    end do

  ! we got to interpolate rho(r,k) in r and yk(omega, k) in omega
  ! for every grid point that is within an 
  ! atomic grid
  ! 
  ! After this call rho and r_array are destroyed and rho_intp has all
  ! the values of the particular solution to the Poisson stored on correct
  ! dimensions for all the grid points inside any atomic grid and the central grid.
  ! this value is zero outside the atomic grid, but is everywhere defined on the central
  ! grid. 
  !       call interpolate_radial_u(atm_grid,r_array ,rho, rho_intp)



  ! Add in homogenous solution for every grid point

  ! direct integral:
  ! poisson of s1*s1 on first atom

  !find right-hand-side matrix: rho


  !      do ir = 1, atm_grid(2)%r_num
  !        do iomeg = 1, atm_grid(2)%ang_num
  !           do k = 1, kmax
  !               
  !           end do
  !        end do
  !      end do   
  !
end subroutine


!*****************************************************
!
!*****************************************************
subroutine solve_poisson_rho_mat(rho, r_num, kmax, lmax, atms)
  use lmindx_map
  use dvrmatrixOperations
  !-----------------------------------------------------------------

  type(atom)                      :: atms(:)
  type(mat_per_grid)              :: rho(:)
  type(vec_per_grid), allocatable :: r_array(:)

  integer                :: l, j 
  integer                :: icent
  integer                :: num_cent
  integer                :: r_num
  integer                :: kmax
  integer                :: knum
  integer                :: lmax
  integer                :: radial_regions
  integer                :: num_cont_pts
  integer                :: k12(2)
  integer, allocatable   :: index(:)

  real(idp), allocatable :: w(:), qn(:)
  real(idp), allocatable :: tot_mat_r(:,:), mat_rhs(:,:)

  logical                :: fix(2) = [.false.,.true.]
  !-----------------------------------------------------------------

  num_cent = size(atms)



  allocate (r_array(num_cent))

  do icent = 1, num_cent


    !           radial_regions = size(atms(icent)%r)
    !      ! This radius is in the atom-center frame:       
    call make_contiguous_r(atms(icent)%r,r_array(icent)%vec, &
      w,index)
    ! to fill other properties of the type:       
    call r_array(icent)%initialize()
    num_cont_pts = r_array(icent)%num_pts
    !       print*, 'number of continuous radial points:',num_cont_pts
    !       call print_vector(r_array(icent)%vec)
    !     ! solve linear Poisson equation for each center:
    !     ! [d^2/dr^2 - l(l+1)/r^2] u(r,k) = -4pi*r*rho(r,k)
    !     ! here rho(r,k) is right-hand matrix, 
    !     ! linear solver replaces mat_rhs with u(r,k). We then store u(r,k) back into
    !     ! rho(r,k) that we no longer need.
    do l = 0, lmax
      !            !k12 contains the index of all kappa related to a fixed l number 
      !            ! kappa <=> {l,m=-l,l}
      k12=indx_range(l)
      !            !!print*, 'k12:',k12            
      call make_ddvr_ddvr_matrix(atms(icent)%r,l, tot_mat_r, &
        fix_and_drop=fix)
      !            !!print*, 'rho'
      !            !!call print_matrix(rho(icent)%mat(:,k12(1):k12(2)))
      call make_rhs_matrix(atms(icent)%r,&
        rho(icent)%mat(:,k12(1):k12(2)),mat_rhs,fix)
      !         !
      !         !   print*, 'mat before'
      !         !   call print_matrix(mat_rhs)
      call linear_solver_matmat(tot_mat_r, mat_rhs)
      !
      !            print*, ' mat right after linear solve'
      !            call print_matrix(mat_rhs)
      !                   
      knum = k12(2) - k12(1) + 1
      allocate(qn(knum))
      !needs fixing!!!
      !qn(1:knum)= sum(rho(icent)%mat(:,k12(1):k12(2))*atms(icent)%xyz(:)%w)

      do j = 1, knum  
        mat_rhs(:,j) = -mat_rhs(:,j) / r_array(icent)%vec(:)&
          / 4.0_idp/ pi
      end do    
      !            print*, 'mat after linear solve'
      !            this is now the particular solution
      !            call print_matrix(mat_rhs)
      !            The homogenous solution is -q_n/r^{l+1}
      !            q_n = \int_0^R \rho d^3r 

      do j = 1, knum
        mat_rhs(:,j) = mat_rhs(:,j) - qn(j) / r_array(icent)%vec(:) ** (l + 1)
      end do 
      !        
      !
      !            ! store back into the rho(icent)%mat, but remeber that dimension 1 
      !            ! of rho is different (in general) from mat_rhs, which is a contigouos
      !            ! matrix in r (i.e. first dimension), so we store the dimension of r
      !            ! in the type
      do j = 1, num_cont_pts
        !               
        rho(icent)%mat(j,k12(1):k12(2)) =   mat_rhs(j,:)
        !               
      end do
      !
      !
      !            !make the extras zero
      rho(icent)%mat(num_cont_pts + 1: rho(icent)%dim1 , k12(1):k12(2)) = 0.d0
      !
      rho(icent)%dim1 = num_cont_pts
      !
      !         ! print*, size(tot_mat_r,1),size(tot_mat_r,2)  
      !         ! stop
      !         !   do m = -l, l
      !         !      k = make_lm_indx(l,m)
      !         !   end do !m
      !         !   stop
      deallocate(tot_mat_r,mat_rhs,qn)
      !
    end do !l 

  end do
end subroutine

!******************************************************************
! interpolating a function of atomic grids in other atomic grids
!On input:
!atm_grid:is a type containig information about the grid coordinates
!          and weights as well as becke-weights. On input it is
!          atm_grid(icent) that is entered, i.e. all the information
!          about the grid.
! r       :is a type with variable vectors embeded in them
!          on input r(icent)%vec(:) contains the contiguous radial
!          coordinates of the icent grid. On exit it is destroyed.
!f_inp    :is a type with variable matrices embeded in them 
!          the value of a function at the r points of grid. 
!          on exit it is destroyed.
!Output:
!f_intrp  :is a an expanded set of f_inp that contains interpolated
!          points.
!******************************************************************
subroutine interpolate_radial_u(atm_grid, r,f_inp, f_intrp)
  use bspline_module, only: bspline_1d
  type(atom_on_grid)              :: atm_grid(:)
  type(vec_per_grid)              :: r(:)
  type(mat_per_grid)              :: f_inp(:)
  type(mat_per_grid), allocatable :: f_intrp(:)
  type(bspline_1d)                :: sp
  real(idp)         ,allocatable  :: r_array(:)
  real(idp)         ,allocatable  :: x(:), fnc(:)
  integer                         :: r_size, kappa_size, num_cent
  integer                         :: icent,ipt,i,na, k, kappa, iflag
  integer                         :: indx1, indx2
  !   idx is the derivative number of piecewise polynomial to interpolate 
  !   (i.e. idx=0 means interpolate the function itself)
  !   kx is the order of the polynomial 0<=kx<n, where n is the grid points
  !   because of adding a zero point to zero for interpolation, n => n+1
  integer, parameter              :: idx=0 , kx = 8

  !check sizes and allocate

  num_cent = size(atm_grid)

  if(size(f_inp) /= num_cent) stop 'discrepency in num_cent'

  allocate (f_intrp(num_cent))

  !======================================================================== 
  ! First the central grid, that encompasses all other grids, is dealt with
  !========================================================================
  kappa_size = size(f_inp(1)%mat, 2)
  r_size     = size(r(1)%vec,1)
  !print*, r_size, r(1)%num_pts, size(f_inp(1)%mat,1)
  !consistency check:
  if(r_size /= r(1)%num_pts) stop 'radial vector of central grid does not match f_inp size!!'

  ! want to add the boundary condition of fcn(0) = 0
  allocate(x(r_size+1),fnc(r_size+1))
  x(1) = 0.d0
  x(2:r_size+1) = r(1)%vec(1:r_size)
  fnc(1) = 0.d0 

  k = r_size

  do icent = 2, num_cent
    r_size = r_size + atm_grid(icent)%num_pts
  end do
  print*, 'n:',r_size    

  call f_intrp(1)%initialize(r_size, kappa_size)

  na  = r_size - k
  print*, 'added grid number:',na 

  allocate(r_array(na))

  r_size = k

  f_intrp(1)%mat(1:r_size,:) = f_inp(1)%mat(1:r_size,:)

  k = 0 ; indx1 = 0; indx2=0

  do icent = 2, num_cent

    indx2 = indx1 + atm_grid(icent)%num_pts
    indx1 = indx1 + 1

    r_array(indx1:indx2) =  radius_pt(atm_grid(icent)%xyz, atm_grid(icent)%num_pts)

    print*, indx1, indx2, r_array(indx1:indx2)


    do kappa = 1, kappa_size
      fnc(2:r_size+1) = f_inp(1)%mat(1:r_size,kappa)
      do i = 1, r_size + 1
        print*, x(i) , fnc(i)
      end do  

      !call sp%initialize(r(1)%vec,f_inp(1)%mat(1:r_size,kappa),kx, iflag)
      call sp%initialize(x,fnc,kx, iflag)

      do ipt = indx1, indx2 


        call sp%evaluate(r_array(ipt), idx, f_intrp(1)%mat(r_size+ipt,kappa), iflag)
        print*, r_array(ipt), f_intrp(1)%mat(r_size+ipt,kappa), iflag

      end do  
      stop


      call sp%destroy()

    end do

    indx1 = atm_grid(icent)%num_pts

  end do  

  !!====================================================================================================
  !! Test interpolation


  open(unit=10, file='cen_intrp_test.csv') 

  do i = 1, r(1)%num_pts
    write (10, *) r(1)%vec(i),(',', f_intrp(1)%mat(i,k),k=1,1)!kappa_size)
  end do

  do i = 1, size(r_array)
    write(10, *) r_array(i), (',', f_intrp(1)%mat(i,k), k= 1,1)! kappa_size)
  end do   

  !!
  !!====================================================================================================
  stop
  do icent = 2, num_cent

    r_size      = f_inp(icent)%dim1
    kappa_size  = f_inp(icent)%dim2

    !         call f_intrp(icent)%initialize(r_size, kappa_size)
    ! go over the x,y,z of each atomic_grid
    do i = 2, icent - 1



    end do  

    do i = icent + 1, num_cent


    end do   

  end do

  ! interpolate per kappa on the grid
  do icent = 1, num_cent

    do k = 1, kappa_size



    end do   

  end do  


end subroutine

!*****************************************************
!
!*****************************************************
function one_s (xyz,bw,cent) result(s1)
  real(idp) :: xyz(:), bw
  real(idp) :: s1, r
  real(idp) :: cent(3)

  r = sqrt(dot_product(xyz-cent,xyz-cent))

  s1 = exp(- r )
  s1 = 1.d0/sqrt(pi) * s1 * bw
  !!      print*,'r:',r,exp(-r),bw, 's1:',s1 
end function

!**********************************************************
!! test of bspline
!**********************************************************   
subroutine test_bspline()
  use bspline_module, only: bspline_1d

  type(bspline_1d) :: s1
  integer, parameter :: nx = 50, n2 = nx - 1
  real(idp) :: x(nx), fcn(nx), tru(n2), val(n2), x_half(n2)
  integer :: i, iflag, idx, kx

  idx = 0  ! derivative of piecewise polynomial to evaluate 
  kx  = 8  ! order (2≤kx<nx)
  iflag = 0

  do i = 1, nx

    x(i) = i * log(real(i)) * 1.d-3
    fcn(i) = x(i) - x(i)**3/6.d0 + x(i)**5/1.2d2-&
      x(i)**7/5.04d3   
    print*, sin(x(i)), x(i)-x(i)**3/6.d0 + x(i)**5/1.2d2-x(i)**7/5.0d4!fcn(i)

  end do  

  print*,'***********************'
  do i = 1, n2
    x_half(i) = x(i+1) + x(i)
    x_half(i) = 0.5d0 * x_half(i) 
    tru(i)    = x_half(i) - x_half(i)**3/6.d0 + x_half(i)**5/1.2d2-&
      x_half(i)**7/5.04d3

    print*, sin(x_half(i)), tru(i)          
  end do   
  stop
  call s1%initialize(x,fcn,kx,iflag)

  do i = 1, n2
    call s1%evaluate(x_half(i),idx,val(i),iflag)
    if(iflag /= 0 ) stop 'error in interpolation'
  end do

  do i = 1, n2


    print*,sin(x_half(i)), val(i),tru(i), (val(i) - tru(i))/tru(i) * 100.d0

  end do     




end subroutine
!***************************************************************
!This gives the radius of a point with Cartesian coordinates in
!the grid.
!***************************************************************
function radius_pt(atom_i_pt,n) result(rad)
  integer   :: n
  real(idp) :: atom_i_pt(n,3)
  real(idp) :: coord2(3)
  real(idp) :: rad(n)
  integer   :: i, j

  do j = 1, n
    do i = 1, 3
      coord2(i) = atom_i_pt(j,i) * atom_i_pt(j,i)
    end do
    rad(j) = sqrt(sum(coord2(:)))
  end do

end function
!***************************************************************
!This gives the radius of a point with Cartesian coordinates in
!the grid, given a center.
!***************************************************************
function distance(pt,cent) result(dist)
  real(idp) :: pt(3)
  real(idp) :: cent(3)
  real(idp) :: dist
  integer   :: i, j


  do i = 1, 3
    dist = dot_product(pt-cent,pt-cent)
  end do
  dist = sqrt(dist)


end function

end module
