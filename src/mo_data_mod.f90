module mo_data_mod
    use string_array_mod
    use commonparams, only: idp
    implicit none

    private

    public mo_data
    
    type mo_data
        integer :: num
        type(string_array_type)::sym, spin
        real(idp), allocatable :: coeff(:)
        real(idp) :: occupancy, energy
        contains
        procedure :: initialize_mo_data
        generic, public :: initialize=>initialize_mo_data
    end type mo_data
    
    contains

    !initialize mo_data type
    subroutine initialize_mo_data(self,n,sym,spin,occupancy,energy)
        class(mo_data):: self
        integer, intent(in) :: n
        character(len=*)::sym,spin
        real(idp)        ::occupancy, energy

        if (.not. allocated(self%coeff))then

            self%num = n

            allocate(self%coeff(n))

            self%sym%string = sym
            self%spin%string = spin
            self%energy  = energy
            self%occupancy = occupancy
            

        else
            
            stop 'attempt to reallocate already allocated gto '

        end if
       

    end subroutine

end module    