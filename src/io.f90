module io
use commonparams
use cfgio_mod

implicit none
private
public set_output, get_input_filename
public itoc
character(len=12), parameter :: default_inp_folder = '../../Input/'
character(len=20) :: default_inp = default_inp_folder // 'input.in'
contains
!///////////////////////////////////////////
! just opens the output file
!///////////////////////////////////////////
  subroutine set_output
      open(unit=iout,file='output.dat')
  end subroutine
!/////////////////////////////////////////////////////////////////////////////
! gets the inline input files if any,
! if no inline argument exists a default input is read
! 1st inline argument is assumed to be the grid parameter inputs
! 2nd inline argument is assumed to be a coordinate file 
! 3rd inline argument is assumed to be basis data file (following TURBOMOLE
! format)
!////////////////////////////////////////////////////////////////////////////
  subroutine get_input_filename(inp_file_name)!,coord_file_name,basis_file_name)
   integer :: ierr1, ierr2, ierr3
   integer :: num_atms
   character(len=3), allocatable :: symb(:)
   character(len=255) :: dummy1, dummy2, dummy3
   character(len=:), allocatable :: inp_file_name
   character(len=255) :: coord_file_name, basis_file_name
   
   logical :: inputfile_exists
   type(cfg_t)::conf

   select case(command_argument_count())

      case(0)

         print*, 'No command argument inputs, defaults are being used'
         inp_file_name = default_inp
         inquire(file=trim(inp_file_name),exist=inputfile_exists)
         if(.not. inputfile_exists) stop "input file" // inp_file_name //"is missing!"

       case(1)

         print*, 'Only input file is specified, default centers and basis'
         call get_command_argument(1, dummy1, status=ierr1)
         inp_file_name = trim(dummy1)
         if(ierr1 > 0)then
           stop 'no such input file name!'
         else if (ierr1 == -1)then
            stop 'command argument was truncated for inputfile'
         else
           inquire(file=trim(inp_file_name),exist=inputfile_exists)
           if(.not. inputfile_exists) stop 'no such input file name!'
           print*, 'input file name:', inp_file_name
         end if

       case(2)

         print*, 'Input file and coord is specified, coord will replace input centers'
         print*, 'Default basis is assumed'
         call get_command_argument(1, dummy1, status=ierr1)

         if(ierr1 > 0)then
           stop 'no such input file name!'
         else if (ierr1 == -1)then
            stop 'command argument was truncated for inputfile'
         else
           inp_file_name = trim(dummy1)
           print*, 'input file name:', inp_file_name
           inquire(file=trim(inp_file_name),exist=inputfile_exists)
           if(.not. inputfile_exists) stop 'no such input file name!'
         end if

         call get_command_argument(2, coord_file_name, status=ierr2)
         if(ierr2 > 0)then
           stop 'no such coordinate file name!'
         else if (ierr2 == -1)then
            stop 'command argument was truncated for coordinate'
         else
           print*, 'coordinates file name:', coord_file_name
         end if

         !test
          !call read_input_defaults(inp_file_name,conf)
          call read_coord_into_input(inp_file_name,coord_file_name,conf)

       case(3)

         call get_command_argument(1, dummy1, status=ierr1)
         if(ierr1 > 0)then
           stop 'no such input file name!'
         else if (ierr1 == -1)then
            stop 'command argument was truncated for inputfile'
         else
           inp_file_name = trim(dummy1)
           print*, 'input file name:', inp_file_name
           inquire(file=trim(inp_file_name),exist=inputfile_exists)
           if(.not. inputfile_exists) stop 'no such input file name!'
         end if

         call get_command_argument(2, coord_file_name, status=ierr2)
         if(ierr2 > 0)then
           stop 'no such coordinate file name!'
         else if (ierr2 == -1)then
            stop 'command argument was truncated for coordinate'
         else
           inquire(file=trim(coord_file_name),exist=inputfile_exists)
           if(.not. inputfile_exists) stop 'no such coord file name!'
           print*, 'coordinates file name:', coord_file_name
         end if

         call get_command_argument(3, basis_file_name, status = ierr3)
         if(ierr3 > 0)then
           stop 'no such basis file name!'
         else if (ierr3 == -1)then
           stop 'command argument was truncated for basis'
         else
           print*, 'basis file name:', basis_file_name
           inquire(file=trim(basis_file_name),exist=inputfile_exists)
           if(.not. inputfile_exists) stop 'no such basis file name!'
         end if

         call read_coord_into_input(inp_file_name,coord_file_name,conf,num_atms,symb)
         call read_basis_into_input(inp_file_name,basis_file_name,conf,num_atms,symb)

      case default

         stop 'too many arguments on the inline'

      end select

 end subroutine
!///////////////////////////////////////////////////////////////////////
!conversts integers to characters
!//////////////////////////////////////////////////////////////////////
function itoc(int_num) result(i_to_char)
  character (len=:), allocatable    :: i_to_char
  integer :: int_num
  if(int_num<10)then
    allocate(character(len=1)::i_to_char)
     write(i_to_char,'(I1)') int_num
  else if(int_num<100) then
    allocate(character(len=2)::i_to_char)
     write(i_to_char,'(I2)') int_num
   else if(int_num<1000) then
     allocate(character(len=3)::i_to_char)
     write(i_to_char,'(I3)') int_num
  end if

end function

!///////////////////////////////////////
! reads all the defaults of an inputfile
!///////////////////////////////////////
subroutine read_input_defaults(inp_file,cfg)
character(len=*)  :: inp_file
character(len=25) :: ang_grid_type
character(len=:), allocatable :: atm_sec
type(cfg_t)       :: cfg
integer           :: num_atoms
integer           :: cent_lmax
integer           :: region_num
integer           :: i, lmax, max_regions
real(idp), allocatable  :: r_intervals(:),atm_r_intv(:,:)
integer, allocatable    :: r_num_shell_pts(:),atm_r_num_shell_pts(:,:), &
                           atm_region_num(:), atm_lmax(:)

cfg=parse_cfg(inp_file)
if( .not. cfg%has_section("DEFAULTS") ) stop 'input file with no default header'
num_atoms = cfg%geti("DEFAULTS", "number_of_atoms")
ang_grid_type = cfg%gets("DEFAULTS","angular_quad_type")
call cfg%set("DEFAULTS","number_of_atoms",num_atoms)
call cfg%set("DEFAULTS","angular_quad_type",ang_grid_type)
if( cfg%has_section("center") )then
   cent_lmax=cfg%geti("center","cent_lmax")
   region_num=cfg%geti("center","region_num")

   call cfg%get("center","r_intervals",r_intervals)
   call cfg%get("center","r_num_shell_pts",r_num_shell_pts)

   !print*,num_atoms, trim(ang_grid_type), cent_lmax, region_num
   !print*, r_intervals
   !print*, r_num_shell_pts

end if

allocate(atm_lmax(num_atoms))
allocate(atm_region_num(num_atoms))

do i = 1, num_atoms
  atm_sec = "atom_"//itoc(i)
  if( cfg%has_section(atm_sec) )then
   atm_lmax(i)=cfg%geti(atm_sec,"lmax")
   atm_region_num(i)=cfg%geti(atm_sec,"region_num")
  end if
   deallocate(atm_sec)

end do

max_regions=maxval(atm_region_num(:))

allocate(atm_r_intv(max_regions+1,num_atoms))
allocate(atm_r_num_shell_pts(max_regions,num_atoms))

do i = 1, num_atoms
   atm_sec = "atom_"//itoc(i)
   call cfg%get(atm_sec,"r_intervals",r_intervals)
   atm_r_intv(1:atm_region_num(i)+1,i) = r_intervals(:)
   call cfg%get(atm_sec,"r_num_shell_pts",r_num_shell_pts)
   atm_r_num_shell_pts(1:atm_region_num(i),i)=r_num_shell_pts(:)

   deallocate(atm_sec)
end do

print*, atm_lmax, atm_region_num
print*, atm_r_intv
print*, atm_r_num_shell_pts
end subroutine
!/////////////////////////////////////////////////////////////////////////////
! Reads the coordinates of atoms in molecule as a second inline input argument
! and writes it (replaces existing parameters) to the input file
!/////////////////////////////////////////////////////////////////////////////
subroutine read_coord_into_input(inputfilename,coord_filename,conf,atm_num,ext_symb)

 logical :: coord_exists
 character(len=*) :: coord_filename
 character(len=*):: inputfilename
 character(len=20) :: dummy_string
 character(len=3),allocatable, optional  :: ext_symb(:)
 character(len=3),allocatable  :: symb(:)
 character(len=6)   :: sec_name
 character(len=7)   :: sec_name_2digit
 character(len=11)  :: key
 real(idp), dimension(3):: val
 real(idp), allocatable :: xyz_coord(:,:)
 integer  , optional    :: atm_num
 integer                :: i, io, num_atms, it1n
 type(cfg_t)            :: conf

  conf = parse_cfg(trim(inputfilename))
 !/////////////////////////////////////////////
 ! At this point coordinate exists, just open
 ! and read
 !////////////////////////////////////////////
      open(unit=1,file=coord_filename)
      read(1,*) dummy_string
      read(1,*,iostat=io) num_atms
      if(present(atm_num))then
        atm_num = conf%geti("DEFAULT","number_of_atoms")
        if(atm_num/=num_atms) stop 'number of atoms from basis and input do not match'
      end if
      if(io > 0 ) stop 'coord input is not right, second line must show integer num of atoms'
      allocate(xyz_coord(3,num_atms), symb(num_atms))
      do i = 1, num_atms
          read(1,*,iostat=io) symb(i), xyz_coord(:,i)
          if(io > 0 ) stop 'check input of coord file, something is wrong'
          print*, symb(i), xyz_coord(:,i)
      end do
      close(1)

      ! write the coord to input
      ! first convert from angstrom to a.u.
      xyz_coord = xyz_coord / bohr_angstrom

        !open(unit=1,file= inputfilename)

         key='atom_center'
        it1n = min(9, num_atms)
        do i = 1, it1n
            sec_name = 'atom_'//itoc(i); val= xyz_coord(:,i)
            print*, val
            call conf%set(sec_name,key,val)
        end do
        if (num_atms > 9)then
           do i = 10, num_atms
              sec_name_2digit = 'atom_'//itoc(i)
              val= xyz_coord(:,i)
               print*, val
               call conf%set(sec_name_2digit,key,val)
            end do
        end if  


        key='atom_symb'
        it1n = min(9, num_atms)
       do i = 1, it1n!num_atms
           sec_name = 'atom_'//itoc(i)
           print*, symb(i),sec_name
           call conf%set(sec_name,key,symb(i))
       end do
       if (num_atms > 9)then
       do i = 10, num_atms
           sec_name_2digit = 'atom_'//itoc(i)
           print*, symb(i),sec_name_2digit
           call conf%set(sec_name_2digit,key,symb(i))
       end do
           
       end if  
      call conf%write(trim(inputfilename))
        !close(1)
      if(present(ext_symb))then
        allocate(ext_symb(num_atms))
        ext_symb = symb
      end if
end subroutine
!/////////////////////////////////////////////////////////////////////
! TURBOMOL format of basis file is being read in
! 
!/////////////////////////////////////////////////////////////////////
subroutine read_basis_into_input(inputfilename,basis_filename,conf, num_atms,atm_symb)
!
! use list
use data_type_sl_list
 logical :: basis_exists
 character(len=*) :: basis_filename
 character(len=*):: inputfilename
 character(len=6) :: start_dummy_string
 character(len=4) :: end_dummy_string
 character(len=1)  :: dummy
 character(len=2)  :: symb
 character(len=3)  :: atm_symb(:)
 character(len=27) :: basis_name
 character(len=6)   :: sec_name
 character(len=11)  :: key
 character(len=1)   :: orb_symb
 real(idp), dimension(3):: val
 real(idp), allocatable :: xyz_coord(:,:)
 real(idp), allocatable :: expnt(:), coef(:)
 integer                :: num_atms, i, io
 integer                :: num_contracted
 integer                :: counter
 type(cfg_t)            :: conf
 type(Type_SL_List)     :: list_expnt_coef, list_symb
! type(cell_array), pointer :: exp_coef_cell, init_arry
! type(cell)      , pointer :: indx, init_indx  

 !/////////////////////////////////////////////
 ! At this point basis exists, just open
 ! and read
 !////////////////////////////////////////////
      conf     = parse_cfg(trim(inputfilename))
      

      ! num_atms = conf%gets("DEFAULT","number_of_atoms")
      ! key='atom_symb'
      ! do i = 1, num_atms
      !    sec_name = 'atom_'//itoc(i)
      !    !print*, symb(i)
      !    call conf%get(sec_name,key,symb(i))
      ! end do

      open(unit=1,file=basis_filename)
      read(1,'(A6)',iostat=io) start_dummy_string
      if(io > 0 .or. start_dummy_string /='$basis' ) then
        print*, start_dummy_string
        stop 'basis input is not right, follow TURBOMOLE format'
      end if

      read(1,'(A)',iostat=io)dummy
    do
      if(dummy == '*')then
        read(1,'(A2,A27)')symb, basis_name
        call list_symb%putt(symb)
        read(1,'(A)',iostat=io)dummy
      else
        stop 'basis input is not right, follow TURBOMOLE format'
      endif
      print*, start_dummy_string
      print*, symb, basis_name
      dummy = ' '
!      counter = 0
      do
!        counter = counter + 1
        read(1,'(A)') dummy
        if(dummy == '*')then
          exit
        else
          backspace(1)
        endif
        read(1,*,iostat=io) num_contracted, orb_symb
        print*, num_contracted, orb_symb
        allocate(expnt(num_contracted),coef(num_contracted))
!        if (counter == 1) then
         call list_expnt_coef%putt(num_contracted)          
!           call init_fill_list(init_indx,indx,)
!        else
!           call fill_list(indx,dreal(num_contracted))
!        end if
        do i = 1, num_contracted
          read(1,*) expnt(i), coef(i)
          print*,     expnt(i), coef(i)

          call list_expnt_coef%putt(expnt(i))
          call list_expnt_coef%putt(coef(i))

!          if(counter == 1)then
!             call init_fill_array_list(init_arry,exp_coef_cell,[expnt(i),coef(i)])
!          else 
!             call fill_array_list(exp_coef_cell,[expnt(i),coef(i)])
!          end if   
        end do
        deallocate(expnt,coef)
      end do
      print*, dummy
      read(1,'(A4)') end_dummy_string
      if(end_dummy_string == '$end') then
        exit
      else
        backspace(1)
      endif
    end do
!Test of lists
 do i = 1, list_symb%l
    call list_symb%get(i,symb)
    print*, symb
 end do

end subroutine

end module
