program main
  ! use input_output, only : Get_Environment
  use commonparams
  use get_inp_mod
  use read_molden_mod
  use io, only: set_output
  use gridparams
  !use becke_original
  use becke_luca
  use voroni
  use tests
  use lebedev_quadrature
  use sphericalHarmonics
  implicit none

  !*********************************************************
  integer                                 :: num_atoms
  integer                                 :: i, l
  integer, parameter                      :: max_rule = 47
  integer, allocatable                    :: lmax_array(:)
  type(atom_on_grid), allocatable         :: atm_grid(:)
  type(atom), allocatable                 :: atms(:)
  type(lebedev)                           :: lb, lbm
  real(idp)                               :: cent(3), expn
  real(idp)                               :: rad, rad_increment
  real(idp), allocatable                  :: rmax_array(:)
  real(idp), parameter                    :: max_R = 10.d0 
  character(len=:), allocatable           :: gridinp_file, basisinp_file
  character(len=:), allocatable           :: test_option

  ! Opening the default output
  call set_output

  ! Just getting the name of the input file or the default input file
   
  call get_input_filename(gridinp_file,basisinp_file)
  
  ! read molden file
  call read_molden_file(basisinp_file)



  ! Want to be totally independent of tests and just call 
  ! suite_of_tests with options that pick and chooses the
  ! real test

  call suite_of_tests(gridinp_file)

!  stop


  !*********************************************************
  ! test bspline
  ! 
  !  call test_bspline(); stop
  !**********************************************************
  !   call drum
  !   call Get_Environment
  !   write(iout,*) "************m6037 link******************"
!  call set_output
!  call lbm%get(max_rule)
  !   !**********************************************************
  !
!  call get_input_filename(inp_file)
 ! call read_write_gridparams(inp_file,num_atoms,atm_grid)
!  call read_write_gridparams(inp_file,num_atoms,atm_grid,atms,lmax_array)
! ************************************************************************
! hydrogen test
!
!  call form_becke_luca(atm_grid,num_atoms,exp_n=2.0_idp,Luca=.true.)
!  call test_hydrogen(atm_grid,atms)
!  stop
!  
!************************************************************************
!  call read_write_gridparams(inp_file,num_atoms,atm_grid,atms,lmax_array,rmax_array)
!  print*, rmax_array ; stop 
  !   write(iout,*) num_atoms

  !***************************
  ! Loop over exponent numbers
!  do i =5,5 
!    expn = real(i)
   ! print*, 'expn:', expn
  ! Adjust the lmax and with it the angular grid size 
  ! according to Lebedev rules  
!    l = 3
!    do while (l <= max_rule)
!       call lb%get(l)
!      print*, l, lb%precision
!  end do ; stop    
   
!   rad = 5.d0; rad_increment = 1.d0
!   do while (rad <= max_R ) 
       

!  do i = 1,1!5 
    
!    expn = real(i)
!    print*, 'expn:', expn
!    print*, '--------------------------------------------'
!    if(num_atoms > 0) then
        
      !lmax_array(2:num_atoms+1) = lb%precision   
      !lmax_array(1) = lb%precision
!      lmax_array(:) = lb%precision
!      call change_angular_grid_params(atm_grid,atms,lmax_array,num_atoms)
!      l = lb%rule + 1
      
      
      !rmax_array(2:num_atoms+1) = rad   
      !rmax_array(1) = rad
      !call change_radial_grid_params(atm_grid,atms,rmax_array)
      !rad = rad + rad_increment
!*******************************************************************
!      Here choose from a list of fitting schemes
!********************************************************************  
!      call form_becke_norms(atm_grid)
!      call form_becke_luca(atm_grid,num_atoms,expn,Luca)
!      call form_becke_luca_nico(atm_grid,num_atoms,expn,Luca=.true.)
!        call voroni_nico(atm_grid)
!       call voroni_lucchese_original(atm_grid)
!********************************************************************
!        print*, '      alpha     ','           %error'
    !
!    else
    !
!         atm_grid(1)%becke_norm(:) = 1.d0

!*********************************************************************
!         Tests for accuracy of the angular grid on a single-centered
!         grid (central grid)
!*********************************************************************
!         call test_ylm_on_grid(atm_grid(1),lmax=6) 
!         call test_plm_on_grid(atm_grid(1),lmax=1)

!
!    end if
!   !*********************************************************************
!   ! Integral of the overall sphere that envelops the problem
!        call integral_sphere(atm_grid, rad=atm_grid(1)%max_radius)!30.d0)
!   !*********************************************************************
!   ! Yukawa function integral tests one function on one atom only
  !   cent = atm_grid(2)%center![0.d0, 0.d0, 2.d0]
  !   call integral_yukawa(atm_grid, cent, alpha = 1.d0)
  !   cent = atm_grid(3)%center![0.d0, 0.d0, 2.d0]
  !   call integral_yukawa(atm_grid, cent, alpha = 1.d0)
  !   cent = atm_grid(4)%center![0.d0, 0.d0, 2.d0]
  !   call integral_yukawa(atm_grid, cent, alpha = 1.d0)
!   !*********************************************************************
!   ! Yukawa function test on all atomic grids
!        call integral_yukawa_all_atm_centers(atm_grid, alpha = 1.d0)
!   !*********************************************************************
!   ! Gaussian function integral on one center
  !     cent = atm_grid(2)%center![0.d0, 0.d0, 2.d0]
  !     call integral_gaussian(atm_grid, cent, alpha=1.d0)
!   !*********************************************************************  
  !     call test_default_gbs(atm_grid)
  !     call test_gbs_range(atm_grid)
!   !************************************************************************  
!   !   Larger tests involving changing lmax (angular grid size) v.s. alpha 
!   !   (i.e. the measure of diffuseness of Gaussian functions placed at the
!   !   atomic centers or rmax (i.e. the size of atomic grids) v.s. alpha
!   !************************************************************************
  !     call lmax_vs_alpha_test(atm_grid,num_atoms,lmax=lmax_array(2),&
  !                             upper_l=lbm%precision)
!        call lmax_vs_alpha_test(atm_grid,num_atoms,lmax=lmax_array(1),&
!                                iexp=i,upper_l=lbm%precision)
                              
  !     call rmax_vs_alpha_test(atm_grid,rmax_array(2),i,max_R)

  !     call overlap_integral_two_gaussians_at_atm_centers(atm_grid)
  !   !*********************************************************************
  !   ! Gaussian function on all atomic grids

  !    call integral_gaussian_all_atm_centers(atm_grid, alpha )
  !   !********************************************************************
  !   ! plm interpolation test : make grids (first equidistantce then gauss
  !   ! gaussian points) and test lagrange interpolation
  !   ! call plm_intp(-1.d0,1.d0,20)
  !   !*******************************************************************
  !   call chainx(0)
  !
!  end do 
!end do
  !
  !   subroutine plm_intp(a,b,num_pts)
  !     use quadrature
  !     use sphericalHarmonics
  !     use lagrange_intp
  !     real(idp)::h, a, b , p, diff
  !     real(idp)::x(num_pts),w(num_pts), x_halfpts(num_pts)
  !     real(idp) :: x0(0:num_pts-1)
  !     real(idp) :: pp(num_pts,num_pts),dp(num_pts,num_pts)
  !     real(idp) :: ddp(num_pts,num_pts), y(num_pts)
  !     real(idp)::scratch(num_pts)
  !     real(idp)::dummy1, dummy2, endpts(2)
  !     integer  ::num_pts,n, m, lmax, i, j, kpts
  !     real(idp), allocatable :: plm(:,:), plm_halfpt(:,:)
  !     real(idp), allocatable :: plm0(:,:)
  !     !**************************************************
  !     ! make equidistant points
  !
  !     h =  (b - a)/num_pts
  !     write(iout,*)'x points ******'
  !     do i = 1, num_pts
  !       x(i) =  a + i * h
  !       write(iout,*) x(i)
  !       x_halfpts(i) = x(i) - 0.75d0*h
  !     end do
  !
  !     !write(iout,*)'weights   ******'
  !     !call lag_weight(x, num_pts, w)
  !     !do i = 1, num_pts
  !     !  write(iout,*)w(i)
  !     !end do
  !     !call lgrng(pp,dp,ddp,x,y,num_pts,num_pts)
  !     write(iout,*) 'y:',y
  !
  !     m = 0
  !     lmax = 2
  !     allocate(plm(num_pts,m:lmax), plm_halfpt(num_pts,m:lmax))
  !     n = num_pts - 1
  !     allocate(plm0(0:n,m:lmax))
  !     call associated_legendre (plm,x,num_pts,lmax,m)
  !     call associated_legendre (plm_halfpt, x_halfpts, &
  !       num_pts, lmax, m)
  !     do i = m, lmax
  !       write(iout,*) '***',i,'***'
  !       write(iout,*) plm(:,i)
  !     end do
  !
  !     !now interpolate
  !     write(iout,*) 'equidistant points'
  !     do i = m, lmax
  !       write(iout, *) '***',i,'***'
  !       do j = 1, num_pts
  !         p = lag_intp(x,plm(:,i),num_pts,x_halfpts(j))
  !         diff = abs(p - plm_halfpt(j,i))/plm_halfpt(j,i) * 100
  !         write(iout,'(3F9.4,x,Es9.2)')            &
  !           x_halfpts(j),p,                        &
  !           plm_halfpt(j,i), diff
  !       end do
  !     end do
  !     !***************************************************************
  !     ! gauss quadrature points
  !     !***************************************************************
  !     write(iout,*) 'gauss quadrature points:'
  !     kpts = 0
  !     endpts(1) = a!-1.d0
  !     endpts(2) = b !1.d0
  !     call gauss_quadrature('legendre', num_pts, dummy1, dummy2,&
  !       kpts, endpts, scratch, x, w)
  !     do i = 1, num_pts
  !       write(iout,*) x(i)
  !       x0(i-1)=x(i)
  !     end do
  !
  !     call associated_legendre (plm,x,num_pts,lmax,m)
  !     call associated_legendre (plm0,x0,num_pts, lmax, m)
  !     write(iout,*) 'y:',y
  !     !now interpolate
  !     do i = m, lmax
  !       write(iout, *) '***',i,'***'
  !       do j = 1, num_pts
  !
  !         p =  lag_intp(x,plm(:,i),num_pts,x_halfpts(j))
  !         diff = abs(p - plm_halfpt(j,i))/plm_halfpt(j,i) * 100
  !         write(iout,'(3F9.4,x,Es9.2)')            &
  !           x_halfpts(j),                          &
  !           p,                                     &
  !           plm_halfpt(j,i), diff
  !       end do
  !     end do
  !
  !     !***************************************************************
  !     ! chebychev quadrature points
  !     !***************************************************************
  !     write(iout,*) 'chebyshev-1 quadrature points:'
  !     kpts = 0
  !     endpts(1) = a
  !     endpts(2) = b
  !     call gauss_quadrature('chebyshev-1', num_pts, dummy1, dummy2,&
  !       kpts, endpts, scratch, x, w)
  !
  !     do i = 1, num_pts
  !       write(iout,*) x(i)
  !     end do
  !
  !     call associated_legendre (plm,x,num_pts,lmax,m)
  !
  !     !now interpolate
  !     do i = m, lmax
  !       write(iout, *) '***',i,'***'
  !       do j = 1, num_pts
  !         p =  lag_intp(x,plm(:,i),num_pts,x_halfpts(j))
  !         diff = abs(p - plm_halfpt(j,i))/plm_halfpt(j,i) * 100
  !         write(iout,'(3F9.4,x,Es9.2)')            &
  !           x_halfpts(j),                          &
  !           p,                                     &
  !           plm_halfpt(j,i), diff
  !       end do
  !     end do
  !
  !     write(iout,*) 'chebyshev-2 quadrature points:'
  !     kpts = 0
  !     endpts(1) = a
  !     endpts(2) = b
  !     call gauss_quadrature('chebyshev-2', num_pts, dummy1, dummy2,&
  !       kpts, endpts, scratch, x, w)
  !
  !     do i = 1, num_pts
  !       write(iout,*) x(i)
  !     end do
  !
  !     call associated_legendre (plm,x,num_pts,lmax,m)
  !
  !     !now interpolate
  !     do i = m, lmax
  !       write(iout, *) '***',i,'***'
  !       do j = 1, num_pts
  !         p =  lag_intp(x,plm(:,i),num_pts,x_halfpts(j))
  !         diff = abs(p - plm_halfpt(j,i))/plm_halfpt(j,i) * 100
  !         write(iout,'(3F9.4,x,Es9.2)')            &
  !           x_halfpts(j),                          &
  !           p,                                     &
  !           plm_halfpt(j,i), diff
  !       end do
  !     end do
  !   end subroutine

end program
