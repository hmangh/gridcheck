module voroni 
  use commonparams
  use gridparams, only: atom_on_grid
  implicit none
  !!***************************************************!!
  !!  Based on Becke paper                              !
  !! of (A. D. Becke, J. Chem. Phys. vol 88, no 4, 1988)!
  !!***************************************************!!
  private

  public form_becke_norms, voroni_nico, voroni_lucchese_original, &
         novoronoi
contains

  !*******************************************************
  !makes Becke weights equal to 1. 
  !Used in single center cases as a convinient redundancy
  ! ideally routines have to realize that a single center
  ! is being used, but it might not always be the case
  !*******************************************************
   subroutine novoronoi(atm)

    type(atom_on_grid)::atm(:)
    integer :: size_atm, i

    size_atm = size(atm)
    do i = 1, size_atm
        if (.not. allocated(atm(i)%becke_norm))then
           allocate(atm(i)%becke_norm(atm(i)%num_pts))
        end if
        atm(i)%becke_norm = 1.d0
    end do

   end subroutine
  !*******************************************************!
  ! given atomic radius (and optionally the radius of the !
  ! center grid) this subroutine calculates the scaled    !
  ! atomic radii according to the perscription of Appendix!
  ! of (A. D. Becke, J. Chem. Phys. vol 88, no 4, 1988)   !
  !*******************************************************!
  subroutine scale_atomic_radii(a_ij,atm)

    type(atom_on_grid)::atm(:)

    real(idp), allocatable :: a_ij(:,:)
    real(idp)              :: u, chi
    integer                :: i, j, size_array


    size_array = size(atm)


    allocate(a_ij(size_array, size_array))

    do i= 1, size(atm)
      do j = 1, size(atm)

        chi = atm(i)%max_radius/atm(j)%max_radius
        !u = (chi - 1)/(chi + 1.d0)
        a_ij(i,j) = 0.25d0 * (1.d0/chi - chi) !u/( u * u - 1.d0)
        if( abs(a_ij(i,j)) > 0.5d0) a_ij(i,j) = sign(0.5d0, a_ij(i,j))

      end do
    end do


  end subroutine

  !***********************************************************!
  ! given a series of atom types and optionally a center      !
  ! the subroutine returns the matrix of inter nuclear        !
  ! distances. If the center is given the last row and column !
  ! of the matrix include the the distance of each atomic     !
  ! nuclues to the center                                     !
  !***********************************************************!
  subroutine inter_nuclear_distance(r_ab,atm)
    real(idp), allocatable :: r_ab(:,:)
    real(idp)              :: u(3)

    type(atom_on_grid) :: atm(:)

    integer    :: i, j 
    integer    :: size_array



    size_array = size(atm)


    allocate( r_ab(size_array, size_array))

    do i = 1, size(atm)
      do j = i, size(atm)

        u  =  atm(i)%center - atm(j)%center 
        r_ab(i,j) = sqrt( dot_product (u , u) )
        r_ab(j,i) = r_ab(i,j)

      end do
    end do



  end subroutine


  subroutine form_becke_norms(atm)

    type(atom_on_grid) :: atm(:)

    integer    :: num_atoms
    integer    :: iatm
    integer    :: i, j, k
    integer    :: ipt
    integer, parameter :: numiter = 3

    real(idp), allocatable :: r_jk(:,:)
    real(idp), allocatable :: a_jk(:,:)
    real(idp), allocatable :: dist(:)
    real(idp)              :: radius, norm, fac, fac1


    call inter_nuclear_distance(r_jk, atm)

    call scale_atomic_radii(a_jk, atm)

    num_atoms = size(atm)

    allocate(dist(num_atoms))

    !loop over atoms
    do iatm = 1, num_atoms

      !loop over iatm points 
      do ipt = 1, atm(iatm)%num_pts

        do j = 1, num_atoms

          dist(j) = distance_pt_from_center(atm(iatm)%xyz(ipt,:),atm(j)%center)

        end do !j loop

        ! radius = radius_pt(atm(iatm)%xyz(ipt,:))

        norm = 0.d0

        ! if(radius >= atm(iatm)%max_radius) norm = 1.d0


        do j = 1, num_atoms

          fac = 1.d0

          do k = 1, j - 1

            call find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter,fac)

          end do

          do k = j+1, num_atoms 

            call find_factor(dist(j), dist(k), r_jk(j,k), a_jk(j,k),numiter,fac)

          end do

          if(fac < 0.0) then 
            write(iout,*) iatm,ipt, fac
          end if  

          norm = norm + fac

          if (iatm == j)then

            atm(iatm)%becke_norm(ipt) = fac

          end if      

        end  do !j loop   

        atm(iatm)%becke_norm(ipt) = atm(iatm)%becke_norm(ipt)/norm

      end do !ipt loop

    end do !iatm loop   

  end subroutine

  !************************************************************************************
  ! finds distance of a cartesian point in the grid from a center point
  !************************************************************************************
  function distance_pt_from_center( atom_i_coord,center) result(dis)

    integer   :: i
    real(idp) :: atom_i_coord(:)
    real(idp) :: center(3), diff(3)
    real(idp) :: dis



    do i = 1, 3

      diff(i) = (atom_i_coord(i) - center(i))
      diff(i) = diff(i) * diff(i)

    end do

    dis = sqrt(sum(diff(:)))


  end function

  !***************************************************************
  ! 
  !***************************************************************
  function radius_pt(atom_i_pt) result(rad)

    real(idp) :: atom_i_pt(:)
    real(idp) :: coord2(3)
    real(idp) :: rad
    integer   :: i

    do i= 1, 3

      coord2(i) = atom_i_pt(i) * atom_i_pt(i)

    end do

    rad = sqrt(sum(coord2(:)))

  end function


  subroutine find_factor(dist1, dist2, r_12, a_12,numiter,fac)

    real(idp) :: dist1, dist2, r_12, a_12
    real(idp) :: diff, fac, fac1, s

    integer   :: numiter
    integer   :: iter

    diff = (dist1 - dist2)/ r_12
    fac1 = diff + a_12*(1.d0 - diff * diff)

    do iter = 1, numiter
      s = (1.5d0 - 0.5d0 * fac1 * fac1) * fac1
      fac1 = s
    end do

    fac = fac * 0.5d0 * (1.d0 - s)


  end subroutine 

  ! trying to call the voroni subroutine that nico gave me'
  subroutine voroni_nico(atm)

    type(atom_on_grid) :: atm(:)

    integer    :: ncent, npmx
    integer    :: iatm
    integer    :: i, j, k, irad
    integer    :: ipt
    integer, parameter :: numiter = 3
    integer,   allocatable :: ngrid(:)
    real(idp), allocatable :: wtt(:),arad(:,:), rmax(:)
    real(idp), allocatable :: a(:,:), grid(:,:,:)
    real(idp), allocatable :: dist(:), d(:), r(:,:) , p(:)
    real(idp)              :: radius, norm, fac, fac1, rad(1)

    irad = 0 !don't know what this is, it is made zero in input
    rad = 0.d0
    ncent = size(atm)

    npmx=maxval(atm(:)%num_pts)

    write(iout,*) 'maximum number of points on any grid:',npmx


    allocate(a(3,ncent))
    allocate(ngrid(ncent))
    allocate(grid(4,npmx, ncent))
    allocate(d(ncent),r(ncent,ncent),p(npmx),wtt(ncent))
    allocate (arad(ncent,ncent),rmax(ncent))

    do i = 1, ncent
      a(:,i)   = atm(i)%center
      ngrid(i) = atm(i)%num_pts
      rmax(i)  = atm(i)%max_radius
      do j = 1, ngrid(i)
        do k = 1, 3
          grid(k,j,i) = atm(i)%xyz(j,k)
        end do
        k = 4
        grid(4, j, i) = atm(i)%w_xyz(j)
      end do
    end do 

    wtt = 1.d0 !weight of atom (usually one)
    call voroni_douget(numiter,ncent,a,ngrid,grid,npmx,d,r,p,wtt &
      ,irad,rad,arad,rmax)

    !call lnkerr('stop')
    
    do i = 1, ncent
      do j = 1, ngrid(i)
        atm(i)%becke_norm(j) = grid(4, j, i) / atm(i)%w_xyz(j)
      end do
    end do

  end subroutine 


  ! trying to call the voroni subroutine that nico gave me before he changed it'
  subroutine voroni_lucchese_original(atm)

    type(atom_on_grid) :: atm(:)

    integer    :: ncent, npmx
    integer    :: iatm
    integer    :: i, j, k, irad
    integer    :: ipt
    integer, parameter :: numiter = 3
    integer,   allocatable :: ngrid(:)
    real(idp), allocatable :: wtt(:),arad(:,:), rmax(:)
    real(idp), allocatable :: a(:,:), grid(:,:,:)
    real(idp), allocatable :: dist(:), d(:), r(:,:) , p(:)
    real(idp)              :: radius, norm, fac, fac1, rad(1)

    irad = 0 !don't know what this is, it is made zero in input
    rad = 0.d0
    ncent = size(atm)

    npmx=maxval(atm(:)%num_pts)

    write(iout,*) 'maximum number of points on any grid:',npmx


    allocate(a(3,ncent))
    allocate(ngrid(ncent))
    allocate(grid(4,npmx, ncent))
    allocate(d(ncent),r(ncent,ncent),p(npmx),wtt(ncent))
    allocate (arad(ncent,ncent),rmax(ncent))

    do i = 1, ncent
      a(:,i)   = atm(i)%center
      ngrid(i) = atm(i)%num_pts
      rmax(i)  = atm(i)%max_radius
      do j = 1, ngrid(i)
        do k = 1, 3
          grid(k,j,i) = atm(i)%xyz(j,k)
        end do
        k = 4
        grid(4, j, i) = atm(i)%w_xyz(j)
      end do
    end do 

    wtt = 1.d0 !weight of atom (usually one)
    call voroni_lucchese(numiter,ncent,a,ngrid,grid,npmx,d,r,p,wtt &
      ,irad,rad,arad,rmax)

    !call lnkerr('stop')
    
    do i = 1, ncent
      do j = 1, ngrid(i)
        atm(i)%becke_norm(j) = grid(4, j, i) / atm(i)%w_xyz(j)
      end do
    end do

  end subroutine 

end module

