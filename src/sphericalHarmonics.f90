module sphericalHarmonics
  use commonparams
  use gridparams, only: atom_on_grid
  implicit none
  private
  public real_spherical_harmonics_on_grid, associated_legendre_on_grid
  public real_spherical_harmonics_on_atmgrid
  public spherical_harmonics_trans_shell
  integer,parameter                     :: maxfac= 200
  real(idp), dimension(0:maxfac)        :: dfct, ddfct

contains

  subroutine factorials
    integer:: i
    logical:: called=.false.
    !implicit integer (a-z)
    !real *8 dfct, ddfct
    !dimension dfct(0:maxfac), ddfct(0:maxfac)

    if(called)then
      !write(iout,*) "*****factorial is already called******"
      return
    else
      write(iout,*) "first time factorials is called"
      !----------------------------------------------------------------------c
      !               calculate factorials                                   c
      !----------------------------------------------------------------------c
      dfct(0)=1.d+00
      dfct(1)=1.d+00
      if (maxfac.gt.1) then
        do 10 i=2,maxfac
          dfct(i)=i*dfct(i-1)
          10     continue
        endif
        !----------------------------------------------------------------------c
        !           calculate (2*m-1) double factorial                         c
        !----------------------------------------------------------------------c
        ddfct(0)=1.d+00
        ddfct(1)=1.d+00
        ddfct(2)=3.d+00
        if (maxfac.gt.2) then
          do 20 i=3,maxfac
            ddfct(i)=(i+i-1)*ddfct(i-1)
            20    continue
          endif
          called = .true.
          return
        end if
      end subroutine factorials

!=========== combination ===============================================================!
      function comb(m, n) result(cmb)

        integer, intent(in) :: m,n
        integer :: i
        real(idp) :: dom, cmb
    
        cmb = 1.0
        if (m > 0 .and. n > 0) then
            dom = 1.0
            do i=n+1,m
                dom = dom * i 
            end do
            cmb = dom/dfct(m-n)
        end if
    
        end function comb 



      !************************************************************************************
      subroutine associated_legendre (plm,x,npt,lmax,m)
        !!for a fixed m finds plm for (l=m,lmax)
        integer                               :: i, l
        integer                               :: lmax, m, npt
        real(idp)                             :: x(:)
        real(idp),dimension(npt)              :: somx2
        real(idp)                             :: norm, fact
        real(idp),dimension(npt,m:lmax)       :: plm
        !----------------------------------------------------------------------
        !           initialize the arrays and do checks
        !----------------------------------------------------------------------

        plm = 0.d0

        if(m.lt.0.d0 .or. m .gt. lmax) then
          write(*,*) 'bad l and m arguments in plm'
          stop
        endif

        do i = 1, npt
          if(abs(x(i)).gt.1.d0)then
            write(*,*) 'bad grid (x) arguments in plm, x must be &
              cos_theta'
            stop
          end if
        end do

        !----------------------------------------------------------------------
        !          compute p^m_m = (-1)^m (1-x^2)^(m/2) (2m-1)!!
        !----------------------------------------------------------------------
        plm(:,m)=1.d0
        if (m > 0) then
          somx2(:) = sqrt((1.d0 - x(:)) * (1.d0 + x(:)))
          fact = 1.d0
          do i= 1, m
            plm(:,m) =  - plm(:,m) * fact * somx2(:)
            fact = fact + 2.d0
          end do
        end if
        !----------------------------------------------------------------------
        !          compute p^m_{m+1} = x (2 m + 1) p^m_m
        !----------------------------------------------------------------------
        if(m /= lmax)then
          plm(:,m + 1) = x(:) * (2 * m + 1) * plm(:,m)
          !----------------------------------------------------------------------
          !           start recursion with plm(m,m) and plm(m+1,m)
          !                      and recur upward
          !            p^m_l (l-m) = x (2l-1)p^m_{l-1} - (l+m-1) p^m_{l-2}
          !----------------------------------------------------------------------
          if(m+1 /= lmax)then
            do l = m + 2, lmax
              plm(:,l)=(x(:)*(2 * l - 1)*plm(:,l-1)-(l+m-1)*plm(:,l-2))/(l - m)
            end do
          endif
        endif
        !---------  normalized--------------------------------------------------

        !           do l = m, lmax
        !                norm= sqrt(0.5d0 * (2 *l + 1) * factorial(l - m )/factorial(l + m))
        !                plm(:,l) = norm * plm (:,l)
        !           end do

      end subroutine associated_legendre

      !***************************************************************************************
      ! Here negative is always associated with sin(m*phi) and positive with
      ! cos(m*phi).
      ! The spherical harmonics thus produced are known as real spherical harmonics
      ! or tesseral spherical harmonics.
      !*****************************************************************************************

      subroutine real_spherical_harmonics(ylm,theta,phi,npt,lmax)
        !!for a given lmax finds all real ylm (l=0,lmax) and (m=-lmax,lmax)
        integer                                        :: i
        integer                                        :: m,l
        integer                                        :: lmax, npt
        real(idp),dimension(npt)                       :: theta
        real(idp),dimension(npt)                       :: phi
        real(idp)                                      :: norm
        real(idp), allocatable                         :: x(:)
        real(idp)                                      :: fact
        real(idp)                                      :: condon_fac
        real(idp),dimension(:,:),allocatable           :: plm
        real(idp),dimension(:,:,:), allocatable        :: ylm
        if(allocated(ylm)) deallocate(ylm)
        allocate(ylm(npt,0:lmax,-lmax:lmax))
        !****it is only called once to make the factorials
        call factorials
        allocate(x(npt))
        x(:) = cos(theta(:))
        !****loop over positive and zero m values
        do m=0,lmax
          !***find all non-normalized plm's for a fixed value of m
          allocate(plm(npt,m:lmax))
          call associated_legendre (plm,x,npt,lmax,m)

          !call legend (plm,cos(theta),dfct,ddfct,npt,lmax,m,maxfac)
          !***loop over l values for a fixed m
          do l= m, lmax
            !**find condon-shortley phase factor of (-1)**m

            if(mod(m,2)==0)then
              condon_fac=1.d0
            else
              condon_fac=-1.d0
            endif
            !***normalization factor of ylm's
            if (m==0)then
              !fact=sqrt((2*l+1)*factorial(l)/(4.d0*pi)/factorial(l))
              fact=0.5d0*sqrt((2.d0*real(l)+1.d0)/pi)
              !write(iout,*)'fact=',fact, pi
              !write(iout,*)'l:',l
              !write(iout,*)'m:',m
              do i=1,npt
                ylm(i,l,0)=fact*plm(i,l)
                !write(iout,*)'ylm:',fact,plm(i,l),l,m,ylm(i,l,m)
              end do
            else
              !fact=condon_fac*sqrt((2*l+1)*factorial(l-m)/(2.d0*pi)/factorial(l+m))
              fact=condon_fac*sqrt((2.d0*l+1)*dfct(l-m)/(2.d0*pi)/dfct(l+m))
              ! write(iout,*)'fact=',fact
              ! write(iout,*)'l:',l
              ! write(iout,*)'m:',m
              ! write(iout,*)'(l-m)!:',dfct(l-m)!factorial(l-m)
              ! write(iout,*)'(l+m)!:',dfct(l+m)!factorial(l+m)
              ! write(iout,*)'fact:',fact
              !***evaluate ylm's on the quadrature points on the surface of a
              !sphere
              do i=1,npt
                !            write(iout,*)'plm:',plm(i,l)
                !            write(iout,*)'cos(phi):',cos(m*phi(i))
                !            write(iout,*)'sin(phi):',sin(m*phi(i))
                ylm(i,l, m) = fact * plm(i,l) * cos(m*phi(i)) !positive m part
                ylm(i,l,-m) = fact * plm(i,l) * sin(m*phi(i)) !negative m  part

                !write(iout,*)'ylm:',l,m,ylm(i,l,m)
                !write(iout,*)'ylm:',l,-m,ylm(i,l,-m)
              end do

            end if
          end do !l
          deallocate(plm)
        end do !m

      end subroutine real_spherical_harmonics


      subroutine real_spherical_harmonics_on_grid(ylm_on_grid,theta, phi, lmax)
        use lmindx_map
        real(idp) , allocatable :: ylm_on_grid(:,:)
        real(idp),dimension(:,:,:), allocatable :: ylm
        real(idp), intent(in) :: theta(:), phi(:)
        integer, intent(in) :: lmax
        integer   :: kappa, kappamax, l, m, npt

        npt = size(theta)
        if(npt /= size(phi)) stop "theta and phi are not the same size"

        kappamax = (lmax+1)*(lmax+1)

        allocate(ylm_on_grid(npt,kappamax))

        call real_spherical_harmonics(ylm,theta,phi,npt,lmax)

        ylm_on_grid(:,1) = ylm(:,0,0)
        if(lmax > 0) then
          do l = 1, lmax
            do m = -l, l
              kappa = make_lm_indx(l,m)
              ylm_on_grid(:,kappa) = ylm(:,l,m)
            end do
          end do
        endif 

      end subroutine real_spherical_harmonics_on_grid

      subroutine real_spherical_harmonics_on_atmgrid(ylm_on_grid,atm,lmax)
        type(atom_on_grid) :: atm
        real(idp) , allocatable :: ylm_on_grid(:,:)
        integer, intent(in)     :: lmax
        integer                 :: ang_num

        ang_num = atm%ang_num
        call real_spherical_harmonics_on_grid(ylm_on_grid, &
               atm%rthetaphi(1:ang_num,2), atm%rthetaphi(1:ang_num,3), lmax)

      end subroutine  

      subroutine associated_legendre_on_grid(plm_on_grid, theta, lmax)
        use lmindx_map
        real(idp) , allocatable :: plm_on_grid(:,:)
        real(idp),dimension(:,:,:), allocatable :: plm_2
        real(idp),dimension(:,:), allocatable :: plm
        real(idp),dimension(:), allocatable :: x
        real(idp), intent(in) :: theta(:)
        real(idp)           :: fact, condon_fac
        integer, intent(in) :: lmax
        integer   :: kappa, kappamax, l, m, npt
        integer   :: i

        !****it is only called once to make the factorials
        call factorials
        npt = size(theta)

        kappamax = (lmax+1)*(lmax+1)

        allocate(plm_on_grid(npt,kappamax), plm_2(npt,0:lmax,-lmax:lmax))
        allocate(x(npt))
        x = cos(theta)

        do m= 0, lmax
            allocate(plm(npt,m:lmax))
            call associated_legendre(plm,x,npt,lmax,m)
          do l= m, lmax
            !**find condon-shortley phase factor of (-1)**m

            if(mod(m,2)==0)then
              condon_fac=1.d0
            else
              condon_fac=-1.d0
            endif
            !***normalization factor of ylm's
            if (m==0)then
              !fact=sqrt((2*l+1)*factorial(l)/(4.d0*pi)/factorial(l))
              fact=0.5d0*sqrt((2.d0*real(l)+1.d0)/pi)
              !write(iout,*)'fact=',fact, pi
              !write(iout,*)'l:',l
              !write(iout,*)'m:',m
              do i=1,npt
                plm_2(i,l,0)=fact*plm(i,l)
                !write(iout,*)'ylm:',fact,plm(i,l),l,m,ylm(i,l,m)
              end do
            else
              !fact=condon_fac*sqrt((2*l+1)*factorial(l-m)/(2.d0*pi)/factorial(l+m))
              fact=condon_fac*sqrt((2.d0*l+1)*dfct(l-m)/(2.d0*pi)/dfct(l+m))
               write(*,*)'fact=',fact
               write(*,*)'l:',l
               write(*,*)'m:',m
               write(*,*)'(l-m)!:',dfct(l-m)!factorial(l-m)
               write(*,*)'(l+m)!:',dfct(l+m)!factorial(l+m)
               write(*,*)'fact:',fact
              !***evaluate ylm's on the quadrature points on the surface of a
              !sphere
              do i=1,npt
                            write(*,*)'plm:',plm(i,l)
                !            write(iout,*)'cos(phi):',cos(m*phi(i))
                !            write(iout,*)'sin(phi):',sin(m*phi(i))
                plm_2(i,l, m) = fact * plm(i,l)
                plm_2(i,l,-m) = (-1)**m * dfct(l-m)/dfct(l+m) * plm_2(i,l,m) 
              end do

            end if
          end do !l
          deallocate(plm)
        end do !m


        plm_on_grid(:,1) = plm_2(:,0,0)
        if(lmax > 0) then
          do l = 1, lmax
            do m = -l, l
              kappa = make_lm_indx(l,m)
              plm_on_grid(:,kappa) = plm_2(:,l,m)
            end do
          end do
        endif
        end subroutine   

    !=========== icart ===============================================================!
      ! cartesian descending ordering: idx = (L+1-i)*(j+k)/2 + k + 1
        function icart(i,j,k) result(indx)

          integer :: indx
          integer :: i,j,k
          integer :: L
      
          L = i + j + k
          indx = ((L+1-i)*(j+k)/2) + k + 1
      
          end function icart 
      
      
      !=========== Nlm ===============================================================!
          real(idp) function compute_Nlm(l,m)
            
          integer, intent(in) :: l, m
          integer :: m2
          real(idp) :: denom,fac
      
          m2 = abs(m);
      
          !compute N
          denom = 1.0;
          if (m == 0) then 
               denom = 2.0
          end if
          fac = 2.0 * dfct(l+m2) * dfct(l-m2) / denom
          compute_Nlm = sqrt(fac) / ( (2**m2)* dfct(l) )
      
          end function compute_Nlm 
      
      !=========== Ctuvlm ===============================================================!
          real(idp) function Ctuvlm(l,m,t,u,v,vm)
      
      
          integer, intent(in) :: l, m, t, u, v, vm
          integer :: m2, sgn
          real(idp) :: denom
      
          m2 = abs(m);
      
          ! compute C
          if (mod(t+v,2) == 0) then 
              sgn = 1;
          else 
              sgn = -1;
          end if
      
          denom = 4**t
          Ctuvlm = sgn * comb(l,t) * comb(l-t,m2+t) * comb(t,u) * comb(m2,vm) / denom
      
          end function Ctuvlm


!=========== spherical trans for shell: ===============================================================!
          !!subroutine strans_shell(l, Sc, Ss,pwr,x,y,z, coeff)
          subroutine spherical_harmonics_trans_shell (l, pwr, coeff)
  
            integer, intent(in) :: l
            !!real(idp), allocatable, dimension(:), intent(inout) :: Sc, Ss
            integer :: n_t, n_v, vv, m, mm, sidx, t, u, v, i, j, k, c1 
            real(idp) :: Nlm, Ctuvlm2, summ
            integer :: size_sc
            integer, allocatable :: pwr(:,:)
            real(idp) , allocatable:: coeff(:,:)
            !!real(idp), intent(in):: x, y, z
            real(idp) :: fac
            
            !initializations--------------------------------------------------     
            call factorials !****it is only called once to make the factorials
            size_sc = (l+1)*(l+2)/2
            !if(.not. allocated(Sc)) allocate(Sc(size_sc))
            !if(.not. allocated(Ss)) allocate(Ss(-l,l))
            if(.not. allocated(pwr)) then
              allocate(pwr(3, size_sc))
            endif
            if (.not. allocated(coeff)) then
              allocate( coeff(-l:l,size_sc) )
            endif  

            fac = sqrt((2*l+1)/pi)*.5d0


            n_v = 1
            vv = 0
            Nlm = 1.0
            do m=-l,l
                mm = abs(m)
      
               ! compute Nlm
               if (mm > 0) then 
                   Nlm = compute_Nlm(l, m)
               else 
                   Nlm = 1.0
               end if
      
               ! find n_t
               n_t = (l-mm)/2
      
               ! find n_v
               if (m >= 0) then 
                   n_v = mm/2 
               else 
                   n_v = (mm-1)/2
               end if
      
               ! spherical idx: for descending order
               !sidx = ipure2(l2, m); 
      
               ! spherical idx: for Psi4 order
               sidx = m 
      
               summ = 0.0
               ! loop over tuv
               do t=0,n_t
                   do v=0,n_v
                       vv = 2*v
                       if (m < 0) then 
                           vv = vv + 1
                       end if
                       do u=0,t
                           Ctuvlm2 = Ctuvlm(l, m, t, u, v, vv)
      
                           j = 2*(u+v)
                           if (m < 0) then
                               j = j + 1
                           end if
                           i = (2*t) + mm - j 
                           k = l - i - j
                           
    
                           ! Function index
                           c1 = icart(i,j,k)
                           ! Keep the powers
                           pwr(:,c1) = [i,j,k]
                           ! Keep the conversion coefficients
                           coeff(m,c1) = Ctuvlm2 * Nlm * fac
                           !!Sc(c1) = Nlm * (x**i * y**j * z**k)
                           !!summ = summ + (Ctuvlm2 * Sc(c1))
                       end do ! u
                   end do ! v
               end do ! t
               !!Ss(sidx) = summ
            end do ! m
       
            !close(1,status='keep')
      
            end subroutine spherical_harmonics_trans_shell 


    end module sphericalHarmonics
