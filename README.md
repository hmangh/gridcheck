cmake -DCMAKE_BUILD_TYPE=Debug ..
or
cmake -DCMAKE_BUILD_TYPE=RELEASE ..

to build with debugger or release

To build and run in the main directory:

1) mkdir build
2) cd build
3) cmake .. (for release) or cmake -DCMAKE_BUILD_TYPE=DEBUG ..
4) make
5) ./amo_grid (default input.in) or ./amo_grid inputfilename
6) view output.dat

***Note***: Fypp is needed to be installed through conda or pip for the CMake of UnitTests in test. This option can be easily dropped from the CMakeLists.txt (in the main directory) by disabling the last line:

        enable_testing()
        add_subdirectory(test)

Inputs are at /Input  

List of (bare essential) parameters that can be set in input:

1) num_atoms: an integer number. Number of atoms in a molecule.

2) angular_quadrature_type: only two options (a) mixed (b) lebedev
        choosing either type automatically sets that type globally.
 
3) [sec_key_name] : sec_key_names are either 'center' or 'atom_#' where # is an
                integer that could technically go up to 999. sec_key_names are
                distinguished with brackets [].
                The section below sec_key_name contains key_names relevant to 
                properties of each atomic or the central grid.

Each grid contains:  
4) lmax : maximum angular number, this number automatically sets a minimum 
          for the number of angular grid points. For details look into 
          gridparams.f90

5) region_num : Since the radial grid is based on dvr functions, the region_num
               indicates how many underlying radial support regions the radial 
               part is divided into. The dvrs are continuous over the boundaries 
               of these support regions, but their derivatives are not.
               Typically atoms should lie within one support region of the dvr functions.
               There is no automatic mechanism to enforce this, and is up to user to place
               atoms within one dvr region of the central grid. Again each small atom grid 
               typically has one region. So, in conclusion, the central grid grid could have 
               more than one dvr support region. The atomic grids have 1 support region.
  
6) r_intervals : every r_interval starts with point zero (so perhaps it is redundant how it is written now)
                number of r_intervals = region_num + 1 and indicate the boundary of the dvr support regions.
                The maximum boundary of each atom is typically a number between 1 to 10 atomic units.
                
7) r_num_shell_pts : number of dvr functions in each region of dvr support. Determines how dense the radial 
                     grid is in a region.

8) atom_center: xyz centre of the spherical atomic grid. The central grid is assumed to be at the origin.

** Note tha basis and coordinate options are not fully functional at this time
 in general the goal is to add input coordinate and basis in distinct files to 
 the inline input
                     
 
